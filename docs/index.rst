=================================
Welcome to cerbere documentation!
=================================

This is the home of ``cerbere`` package.  The objective of ``cerbere`` is to
provide different free and open source python modules for reading,
interpreting, and writing earth observation data complying to community
standards such as the Climate & Forecast (CF), Unidata's Common Data Model
(CDM) and ACDD metadata conventions, complemented by additional recommendations
from GHRSST, NCEI or authors of this package.

It is based on xarray_ which it extends through the use of accessors for
xarray_'s `Dataset` and `DataArray` classes and additional reader or backend
classes for non compliant datasets. It also adds a typology of observation
objects (called **features**) though a set of template classes implementing
common features defined in CF or CDM.

.. _NumPy: http://www.numpy.org
.. _xarray: http://xarray.pydata.org
.. _conda: https://docs.conda.io/en/latest/



Getting started
===============

.. toctree::
   :maxdepth: 1

   installation
   working_with_data

User guide
==========

.. toctree::
   :maxdepth: 2

   dataarray
   dataset
   features
   format_profile

Available dataset classes
=========================

.. toctree::
   :maxdepth: 2

   compatibility

Developer's corner
==================

.. toctree::
   :maxdepth: 2

   writing_reader
   unittesting


Reference
=========

.. toctree::
   :maxdepth: 1

   api


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

License
-------

``cerbere`` is available under the open source `GPL License`__.

__ https://www.gnu.org/licenses/gpl-3.0.en.html
