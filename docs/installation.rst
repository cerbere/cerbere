============
Installation
============
.. _conda: https://docs.conda.io/en/latest/

Requirements
============

``cerbere`` works with python >= 3.8.

Python dependencies include:

* netCDF4
* numpy
* pandas
* xarray>=0.15
* shapely
* python-dateutil


Cerbere
=======

Just use your package manager to perform installation.

``cerbere`` can be installed with conda_:

.. code-block:: bash

   conda install cerbere -c conda-forge

