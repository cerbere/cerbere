.. currentmodule:: cerbere

#############
API reference
#############

This page provides a summary of cerbere API. For more details
and examples, refer to the relevant chapters in the main part of the
documentation.

cerbere provides to accessors for xarray DataArray and Dataset classes. In
both cases, they are called `cerbere`.

DataArray accessor
==================

Creating a cerbere DataArray
----------------------------
.. currentmodule:: cerbere.accessor.cdataarray

.. autosummary::

    DataArrayAccessor

Attributes
----------
.. autosummary::

    DataArrayAccessor.cfarray

    DataArrayAccessor.long_name
    DataArrayAccessor.standard_name
    DataArrayAccessor.ancillary_variables
    DataArrayAccessor.units
    DataArrayAccessor.coverage_content_type
    DataArrayAccessor.science_dtype
    DataArrayAccessor.mask

    DataArrayAccessor.fill_value
    DataArrayAccessor.scale_factor
    DataArrayAccessor.add_offset
    DataArrayAccessor.precision
    DataArrayAccessor.valid_min
    DataArrayAccessor.valid_max

Methods
-------
.. autosummary::

    DataArrayAccessor.isel
    DataArrayAccessor.loc
    DataArrayAccessor.to_masked_array
    DataArrayAccessor.clip



Dataset cerbere accessor
========================

The base class for ``Dataset`` objects, than can be imported from
``cerbere.dataset.dataset`` module. All other classes in ``cerbere.dataset``
package are derived from this class.

Creating a dataset
------------------
.. currentmodule:: cerbere.accessor.cdataset

.. autosummary::

    DatasetAccessor

Attributes
----------
.. autosummary::

    DatasetAccessor.basename
    DatasetAccessor.url
    DatasetAccessor.filesize
    DatasetAccessor.file_creation_date
    #DatasetAccessor.geocoordnames
    #DatasetAccessor.geocoords
    #DatasetAccessor.geodimnames
    #DatasetAccessor.geodims
    #DatasetAccessor.geodimsizes
    DatasetAccessor.bbox
    DatasetAccessor.wkt_bbox
    DatasetAccessor.collection_id
    DatasetAccessor.naming_authority
    DatasetAccessor.product_version
    DatasetAccessor.time_coverage_start
    DatasetAccessor.time_coverage_end


Dataset contents
----------------
.. autosummary::

    DatasetAccessor.save


Dataset subsetting
------------------
.. autosummary::

    DatasetAccessor.isel
    DatasetAccessor.clip
    DatasetAccessor.get_location_by_value
    DatasetAccessor.closest_spatial_location


Feature
=======

A :class:`~cerbere.feature.cfeature.Feature` object inherits all the attributes
and methods of a :class:`~cerbere.dataset.dataset.Dataset` object. It provides
in addition the following methods and attributes.

Creating a feature
------------------

The generic method to create or open from a file a feature is
:func:`cerbere.open_as_feature`. It allows to instantiate any of the following
features.

.. currentmodule:: cerbere.feature

.. autosummary::

    cpoint.Point
    cprofile.Profile
    ctrajectory.Trajectory
    ctimeseries.TimeSeries
    ctimeseriesprofile.TimeSeriesProfile
    ctrajectoryprofile.TrajectoryProfile
    cswath.Swath
    cgrid.Grid
    cgridtimeseries.GridTimeSeries

Additional collection feature class, as defined by CF convention:

.. currentmodule:: cerbere.feature

.. autosummary::

    #ccracollection.CRACollection
    cimdcollection.IMDCollection
    comdcollection.OMDCollection


Attributes
----------
.. currentmodule:: cerbere.feature.feature

.. autosummary::

    Feature.feature_type
    #Feature.geodims
    #Feature.geodimnames
    #Feature.geodimsizes



Feature contents
----------------
.. autosummary::

    Feature.extract
    Feature.append
