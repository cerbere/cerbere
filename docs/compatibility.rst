.. |Grid| replace:: :class:`~cerbere.feature.grid.Grid`
.. |GridTimeSeries| replace:: :class:`~cerbere.feature.gridtimeseries.GridTimeSeries`
.. |Trajectory| replace:: :class:`~cerbere.feature.trajectory.Trajectory`
.. |Swath| replace:: :class:`~cerbere.feature.swath.Swath`
.. |PointCollection| replace:: :class:`~cerbere.feature.pointcollection.PointCollection`

.. |NCDataset| replace:: :class:`~cerbere.dataset.ncdataset.NCDataset`
.. |GHRSSTNCDataset| replace:: :class:`~cerbere.dataset.ghrsstncdataset.GHRSSTNCDataset`

Complementary readers
=====================

``cerbere`` ecosystem comes with a variety of contrib packages providing
``reader`` classes for different datasets that can not be natively
postprocessed and harmonized by the core ``cerbere`` accessors. They are to
be installed if you wish to read the corresponding datasets in ``cerbere``.

available contribs
------------------

Here is a list of existing contrib packages and their usage. Refer to these
packages for more details on the datasets they handle. These packages are
available with ``cerbere`` at: https://gitlab.ifremer.fr/cerbere. They are
also available as conda packages in ``conda-forge``.

`cerberecontrib-ghrsst <https://git.cersat.fr/cerbere/cerberecontrib-ghrsst>`_ :
GHRSST L2P, L3U, L3C, L3S and L4 datasets

`cerberecontrib-s3 <https://git.cersat.fr/cerbere/cerberecontrib-s3>`_: a
collection of readers for EUMETSAT Sentinel-3 SLSTR and OLCI L1 and L2 datasets

`cerberecontrib-cfosat`:

`cerberecontrib-scatterometer`:

`cerberecontrib-sar`:

`cerberecontrib-altimetry`:

`cerberecontrib-ers`:

`cerberecontrib-topex`:

`cerberecontrib-envisat`:

`cerberecontrib-bufr`:



Datasets natively handled in ``cerbere``
----------------------------------------

In addition to the above contribs, here is a non-comprehensive list of
datasets that are directly usable with ``cerbere`` accessor and feature
classes, without requiring any ``reader`` class:

.. csv-table:: **cerbere** package
   :header: "Dataset", "Matching ``feature`` class"

   "CATDS SMOS SSS CEC-OS products", |Grid|
   "AVISO Altimetry L2 (S)GDR", |Trajectory|
   "Ifremer (Boyer) Mixed Layer Depth climatology", |Grid|
