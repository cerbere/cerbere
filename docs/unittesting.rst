.. |reader| replace:: :mod:`~cerbere.dataset`
.. |feature| replace:: :mod:`~cerbere.feature`

=======================
Unit testing in cerbere
=======================


This section is for cerbere developers, in particular when implementing a new
reader class.


reader testing
==============

Cerbere unit testing is based on :mod:`pytest` python package. This section
describes the procedure to test new or existing reader classes.

test data
---------
When providing a new reader, test data files should be provided too. These
data files can not be stored on cerbere git server and must be made available
on some server.

As an example, the test data files for the reader classes implemented by
Ifremer are available at
ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/

There should be one sub-folder for each reader module. For instance for the
Sentinel-3 SLSTR reader classes (:mod:`cerberecontrib-s3` package,
:mod:`safesl` module), the test data files are to be found at:

ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/safesl/

There should be at least one test file per class in the reader module.

unit testing on reader classes
------------------------------

Generic test functions are provided in :mod:`cerbere.test.pytest_reader`
module.

To unit test a reader class, you basically need to create a new test module,
importing . This module should
define the required fixtures for the :mod:`cerbere.test.pytest_reader` test
functions to work, plus additional test functions you may want to
add for a specific reader. A test module must be created for each class in
your reader module.

This module file must be in the ``tests`` repository  of cerbere or your contrib
package.

For instance, for the GHRSST reader class available in
`cerberecontrib-sst <https://gitlab.ifremer.fr/cerbere/cerberecontrib-sst>`,
a simple test module would look as follow:

.. code-block:: python

    from .pytest_reader import *

    from cerbere.feature.cswath import Swath

    TEST_FILE = '/home/jfpiolle/git/naiad-index/tests/data/avhrr_metop' \
                '/20200330235803-OSISAF-L2P_GHRSST-SSTsubskin-AVHRR_SST_METOP_B' \
                '-sstmgr_metop01_20200330_235803-v02.0-fv01.0.nc'


    @pytest.fixture(scope='module')
    def test_file():
        return TEST_FILE


    @pytest.fixture(scope='module')
    def reader():
        return 'GHRSST'


    @pytest.fixture(scope='module')
    def feature_class():
        return Swath


    def test_guess_backend(input_file):
        """Open file and create dataset object"""
        dst = xr.open_dataset(input_file).cb.cfdataset
        assert 'time' in dst
        assert 'id' in dst.attrs


Your test class must import the :mod:`cerbere.test.pytest_reader` that
provides many predefine generic test functions to test a reader compliance to
``cerbere``. Required pytest fixtures for these tests to work must be
provided in the test class:

*  the reader class name and associated |feature| class name to be passed
   to ``cerbere.open_feature`` function, returned respectively by :func:`reader` and :func:`feature_class` fixtures.

.. code-block:: python

    @pytest.fixture(scope='module')
    def reader():
        return 'GHRSST'


    @pytest.fixture(scope='module')
    def feature_class():
        return Swath

*  which file to use for testing the class, returned by :func:`test_file`
   fixture.

.. code-block:: python

    @pytest.fixture(scope='module')
    def test_file():
        return '/20200330235803-OSISAF-L2P_GHRSST-SSTsubskin-AVHRR_SST_METOP_B' \
                '-sstmgr_metop01_20200330_235803-v02.0-fv01.0.nc'


*  where to get this file, return by :func:`download_url` class method

.. code-block:: python

    @pytest.fixture(scope='module')
    def download_url(cls):
        """Return the URL of the data test repository where to get the test
        files
        """
       return "ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/"



testing example
---------------

In order for the test to run, the location of your test data must be known
to the testing procedures. To ensure this, set the environment variable
``CERBERE_TESTDATA_REPOSITORY`` to your root repository for your test data.

Set the environment variable to locate your test data:

.. code-block:: bash

   export CERBERE_TESTDATA_REPOSITORY=/repo/where/you/put/your/test/data/

To run the test suite, go into the root folder of the tested contrib package:

.. code-block:: bash

   pytest your_class_test.py

