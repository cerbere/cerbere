========
Features
========
.. ipython:: python
    :suppress:

    import numpy as np
    import pandas as pd
    import xarray as xr
    from datetime import datetime

.. _Xarray: http://xarray.pydata.org

.. |BaseFeature| replace:: :class:`~cerbere.feature.cbasefeature.CBaseFeature`

.. |Grid| replace:: :class:`~cerbere.feature.cgrid.Grid`
.. |Swath| replace:: :class:`~cerbere.feature.cswath.Swath`
.. |Image| replace:: :class:`~cerbere.feature.cimage.Image`
.. |GridTimeSeries| replace:: :class:`~cerbere.feature.cgridtimeseries.GridTimeSeries`
.. |Trajectory| replace:: :class:`~cerbere.feature.ctrajectory.Trajectory`
.. |Profile| replace:: :class:`~cerbere.feature.cprofile.Profile`
.. |Point| replace:: :class:`~cerbere.feature.cpoint.Point`
.. |TimeSeries| replace:: :class:`~cerbere.feature.ctimeseries.TimeSeries`
.. |TimeSeriesProfile| replace:: :class:`~cerbere.feature.ctimeseriesprofile.TimeSeriesProfile`
.. |TrajectoryProfile| replace:: :class:`~cerbere.feature.ctrajectoryprofile.TrajectoryProfile`
.. |UniZTrajectoryProfile| replace:: :class:`~cerbere.feature.ctrajectoryprofile.UniZTrajectoryProfile`
.. |OMDCollection| replace:: :class:`~cerbere.feature.comdcollection.OMDCollection`
.. |IMDCollection| replace:: :class:`~cerbere.feature.cimdcollection.IMDCollection`
.. |CRACollection| replace:: :class:`~cerbere.feature.ccracollection.CRACollection`


Features in ``cerbere`` implement specific data structures tied to observation
patterns shared by many Earth observation data, following the `Climate and
Forecast` conventions (complemented by other conventions or rules). They check
and harmonize their dimensions and coordinates so that observations acquired in
the same or sharing common spatial and temporal properties are structured and
named in the same way.

**Feature** classes inherit from the main |BaseFeature| base class which
itself is a wrapper for a Xarray_ ``Dataset`` (and derives from it by
composition). They can be created from any ``Dataset`` object meeting
the expected properties of a given feature. |BaseFeature| is an abstract base
class and only objects from other classes in ``feature`` package can be
instantiated. Examples of feature classes include |Point|, |Profile|,
|TimeSeries| and many more.

A feature object can therefore be created from any Xarray_ ``Dataset`` object,
but it has to match the expected requirements on dimensions and coordinates
of the instantiated feature class. ``cerbere`` may do some guesses to try
to cast the ``Dataset`` object into the expected feature class properties,
therefore possibly renaming or reorganizing its dimensions and variables.


Reading a feature from a file
-----------------------------

Knowing the feature type of an object stored in a file, and the reader
class (if not an Xarray_ engine) corresponding to the file format, one can
directly load the content of this file into a fully formed feature object.

In the following example, let's open a GHRSST L2P file, which
is a |Swath| feature, for which there is a special ``GHRSST`` reader class in
:mod:`cerberecontrib-sst` contrib package.

.. ipython:: python
    :okexcept:
    :okwarning:

    import cerbere

    # detects it is a GHRSST file, fetches the correct reader and returns a
    # CF/Cerbere compliant Swath feature object.
    swath = cerbere.open_feature(
        './samples/20190719000110-MAR-L2P_GHRSST-SSTskin-SLSTRA-20190719021531-v02.0-fv01.0.nc',
        'Swath'
    )
    print(swath)


Creating a feature from or like a xarray_ :class:`~xarray.Dataset` object
-------------------------------------------------------------------------

Features can be created from a xarray :class:`xarray.Dataset` object. The
definition of the :class:`xarray.Dataset` object must match the
requirements of the feature to be instantiated, in terms of dimensions and
coordinate variables.

For instance to create a |Grid| feature from a
:class:`xarray.Dataset` object, the later must have, for a cylindrical grid:

  * ``lat``, ``lon``, ``time`` dimensions. ``time`` dimension must have a length
    equal to 1.
  * a ``lat`` coordinate variable with dimension ``lat``.
  * a ``lon`` coordinate variable with dimension ``lon``.
  * a ``time`` coordinate variable with dimension ``time``.
  * data variables with spatial dimensions (``lat``, ``lon``,). Extra dimensions
    are allowed (except ``time``).


.. code-block:: python

    from cerbere.feature.cgrid import Grid

    # create an xarray Dataset with the structure of a cylindrical grid
    lat = xr.DataArray(data=np.arange(-80, 80, 1), dims=['lat'])
    lon = xr.DataArray(data=np.arange(-180, 180, 1), dims=['lon'])
    time = xr.DataArray([datetime(2018, 1, 1)], dims='time')
    var = xr.DataArray(
        data=np.ones(shape=(160, 360)),
        dims=['lat', 'lon'],
        attrs={'myattr': 'test_attr_val'}
    )
    xrdataset = xr.Dataset(
        coords={'lat': lat, 'lon': lon, 'time': time},
        data_vars={'myvar': var},
        attrs={'gattr1': 'gattr1_val', 'gattr2': 'gattr2_val'}
    )

    # create the cylindrical grid feature from the xarray dataset object
    grid = Grid(xrdataset)
    grid


Harmonization
-------------
When creating a feature from a :class:`xarray.Dataset` object or file
content, ``cerbere`` performs some checks to verify the object meets the
expected feature properties and rename its coordinates and dimensions if
necessary.


Available features
------------------

The main features currently managed include :

 * :class:`~cerbere.feature.cgrid.Grid`
 * :class:`~cerbere.feature.cswath.Swath`
 * :class:`~cerbere.feature.cimage.Image`
 * :class:`~cerbere.feature.cgridtimeseries.GridTimeSeries`
 * :class:`~cerbere.feature.ctrajectory.Trajectory`
 * :class:`~cerbere.feature.cpointtimeseries.PointTimeSeries`
 * :class:`~cerbere.feature.cpoint.Point`
 * :class:`~cerbere.feature.cprofile.Profile`
 * :class:`~cerbere.feature.ctimeseries.TimeSeries`
 * :class:`~cerbere.feature.ctimeseriesprofile.TimeSeriesProfile`
 * :class:`~cerbere.feature.ctrajectoryprofile.TrajectoryProfile`


The classes provided in :mod:`~cerbere.feature` modules and listed above
correspond to the main sampling patterns usually used for Earth Observation
data. Whenever possible, they follow the recommendations of `Climate and
Forecast (CF) convention <https://cfconventions.org/cf-conventions>`_.

The following table describes the dimensions and spatio-temporal
coordinate (geolocation) fields associated with each feature in
:mod:`~cerbere.feature`:

.. csv-table:: **cerbere** main features
   :delim: ;
   :file: features.csv
   :header-rows: 1


.. rubric:: CF references

.. [#f1] `CF Point featureType <https://cfconventions.org/cf-conventions/cf-conventions.html#point-data>`_
.. [#f2] `CF Trajectory featureType <https://cfconventions.org/cf-conventions/cf-conventions.html#trajectory-data>`_
.. [#f3] `CF Profile featureType <https://cfconventions.org/cf-conventions/cf-conventions.html#profile-data>`_
.. [#f4] `CF TimeSeries featureType <https://cfconventions.org/cf-conventions/cf-conventions.html#time-series-data>`_
.. [#f5] `CF TrajectoryProfile featureType <https://cfconventions.org/cf-conventions/cf-conventions.html#trajectory-profiles>`_
.. [#f6] `CF TimeSeriesProfile featureType <https://cfconventions.org/cf-conventions/cf-conventions.html#time-series-profiles>`_


Special features
++++++++++++++++

Additional types of features, representing particular cases of the main features
above, are also available. They follow the CF rules defined for these particular
cases, when applicable.

.. csv-table:: Special features
   :delim: ;
   :file: derived_features.csv
   :header-rows: 1

.. rubric:: CF references

.. [#s1] `CF TrajectoryProfile featureType where  all the profiles have the same
 set of vertical coordinates <https://cfconventions.org/cf-conventions/cf-conventions.html#trajectory-profiles>`_


Collection features
+++++++++++++++++++

Additional types of features include ``collection`` : these are special features
that consist in grouping features of the same type into one single dataset along
an extra axis, the name of which depends on the grouped feature's type.

They are described in CF convention and include:

* orthogonal multidimensional collection of features (|OMDCollection|)
* incomplete multidimensional collection of features (|IMDCollection|)
* contiguous ragged array collection of features (|CRACollection|)
