# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# http://www.sphinx-doc.org/en/master/config

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import pathlib
import sys


# PYTHONPATH = docs/source
DOC_SOURCES_DIR = os.path.dirname(os.path.abspath(__file__))
PROJECT_ROOT_DIR = os.path.dirname(os.path.abspath(DOC_SOURCES_DIR))
sys.path.insert(0, DOC_SOURCES_DIR)
print('PROJECT_ROOT_DIR', PROJECT_ROOT_DIR)

# If runs on ReadTheDocs environment
on_rtd = os.environ.get('READTHEDOCS', None) == 'True'
#on_rtd = 1 == 1

# Hack for lacking git-lfs support ReadTheDocs
if on_rtd:
    print('Fetching files with git_lfs')
    from git_lfs import fetch
    fetch(PROJECT_ROOT_DIR)

#sys.path.insert(0, os.path.abspath('..'))

# make sure the source version is preferred (#3567)
root = pathlib.Path(__file__).absolute().parent.parent
os.environ['PYTHONPATH'] = str(root)
sys.path.insert(0, str(root))


allowed_failures = set()

print('python exec:', sys.executable)
print('sys.path:', sys.path)
print('PYTHONPATH:', os.environ['PYTHONPATH'])

# -- Project information -----------------------------------------------------

project = 'cerbere'
copyright = '2019, Jeff Piolle'
author = 'Jeff Piolle'

# The full version, including alpha/beta/rc tags
release = '2'


# -- General configuration ---------------------------------------------------
autoclass_content = 'both'  # include both class docstring and __init__
autodoc_default_options = {
    # Make sure that any autodoc declarations show the right members
    'members': True,
    'inherited-members': True,
    'show-inheritance': True,
}
autodoc_typehints = 'signature'

autosummary_generate = True  # Make _autosummary files and include them
napoleon_numpy_docstring = False  # Force consistency, leave only Google
napoleon_use_rtype = False  # More legible

numpydoc_class_members_toctree = True
numpydoc_show_class_members = False

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx.ext.doctest',
    'sphinx.ext.todo',
    'sphinx.ext.coverage',
    'sphinx.ext.ifconfig',
    'sphinx.ext.viewcode',
    'sphinx.ext.graphviz',
    'sphinx.ext.napoleon',
    'sphinx_autodoc_typehints',
    'sphinx.ext.napoleon',
    'IPython.sphinxext.ipython_console_highlighting',
    'IPython.sphinxext.ipython_directive'
]




# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']


# Example configuration for intersphinx: refer to the Python standard library.
intersphinx_mapping = {
    'python': ('https://docs.python.org/3/', None),
    'pandas': ('https://pandas.pydata.org/pandas-docs/stable', None),
    'numpy': ('https://docs.scipy.org/doc/numpy', None),
    'scipy': ('https://docs.scipy.org/doc/scipy/reference', None),
    'numba': ('https://numba.pydata.org/numba-doc/latest', None),
    'matplotlib': ('https://matplotlib.org', None),
    'dask': ('https://docs.dask.org/en/latest', None),
    'cftime': ('https://unidata.github.io/cftime', None),
    'xarray': ('http://xarray.pydata.org/en/stable', None),
}
