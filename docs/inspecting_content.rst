===================================
Inspect Dataset and Feature objects
===================================

.. |dataset| replace:: :mod:`~cerbere.dataset`
.. |Field| replace:: :mod:`~cerbere.dataset.field.Field`



The following section describes the basic operations you can perform with
:mod:`cerbere` to handle Earth Observation data files with
|dataset| package. This package can be seen as a unified API to
access any data file content, whatever the format and content. There is one
|dataset| class per data format, convention, product type...

Using objects from `dataset` subpackage
=======================================

A |dataset| class provides a set of methods to inspect the content
of the read data. They allow to retrieve information from a file in the same way
whatever its format. There is one class per type of format or writing convention
. Let's open for instance a GHRSST sea surface temperature product, using the
:class:`~cerbere.dataset.ghrsstncdataset.GHRSSTNCDataset` class.

First download the sample product at: ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/GHRSSTNCDataset/20190719000110-MAR-L2P_GHRSST-SSTskin-SLSTRA-20190719021531-v02.0-fv01.0.nc

Open it:

.. code-block:: python

    from cerbere.dataset.ghrsstncdataset import GHRSSTNCDataset
    prod = GHRSSTNCDataset('20190719000110-MAR-L2P_GHRSST-SSTskin-SLSTRA-20190719021531-v02.0-fv01.0.nc')

Get the list of fields in a file (all but the geolocation fields) with
:attr:`~cerbere.dataset.dataset.Dataset.fieldnames`:

>>> prod.fieldnames
['adi_dtime_from_sst', 'aerosol_dynamic_indicator', 'brightness_temperature', 'dt_analysis', 'dual_nadir_sst_difference', 'l2p_flags', 'nadir_sst_theoretical_uncertainty', 'nedt', 'probability_cloud_single_in', 'probability_cloud_single_io', 'quality_level', 'satellite_zenith_angle', 'sea_ice_fraction', 'sea_ice_fraction_dtime_from_sst', 'sea_surface_temperature', 'sses_bias', 'sses_standard_deviation', 'sst_algorithm_type', 'sst_theoretical_uncertainty', 'wind_speed', 'wind_speed_dtime_from_sst']

Get the list of geolocation fields with :attr:`~cerbere.dataset.dataset.Dataset.geocoordnames`:

>>> prod.geocoordnames
['lat', 'lon', 'time']

Get the dimensions (like in netCDF) of a file (Note that the geolocation dimension
names returned are standardized - here ``cell`` and ``row`` are the standard
dimensions names for a satellite swath observation feature):

>>> prod.dims
OrderedDict([('cell', 1500), ('channel', 3), ('row', 1202)])

Get the size of a dimension (standard names can be used for geolocation
dimensions):

>>> prod.get_dimsize('row')    # standard dimension name for Swath feature
1202

Get a field and print it:

>>> field = prod.get_field('sea_surface_temperature')
>>> print(field)
Field : 'sea_surface_temperature'
    sea surface skin temperature
    dimensions :
      # row : 1202
      # cell : 1500
    standard CF attributes :
      # standard_name : sea_surface_skin_temperature
      # units : kelvin
      # valid_max : 4500
      # valid_min : -300
      # fill_value : -32768
    other attributes :
      # comment : Marine skin surface temperature
      # depth : 10 micrometres
      # quality_vars : None


Get the field attributes, as a dictionary:

>>> prod.get_field_attrs('sea_surface_temperature')
{'comment': 'Marine skin surface temperature', 'depth': '10 micrometres', 'long_name': 'sea surface skin temperature', 'standard_name': 'sea_surface_skin_temperature', 'units': 'kelvin', 'valid_max': 4500, 'valid_min': -300, 'quality_vars': None}

or:

>>> prod.get_field('sea_surface_temperature').attrs
{'comment': 'Marine skin surface temperature', 'depth': '10 micrometres', 'long_name': 'sea surface skin temperature', 'standard_name': 'sea_surface_skin_temperature', 'units': 'kelvin', 'valid_max': 4500, 'valid_min': -300, 'quality_vars': None}

Get the dimensions (like in netCDF) of a particular field:

>>> prod.get_field_sizes('sea_surface_temperature')
OrderedDict([('row', 1202), ('cell', 1500)])

or:

>>> prod.get_field('sea_surface_temperature').dims
OrderedDict([('row', 1202), ('cell', 1500)])


Get the dataset global attributes, as a dictionary:

>>> prod.attrs
{'Conventions': 'CF-1.6, Unidata Observation Dataset v1.0', 'Metadata_Conventions': 'Unidata Dataset Discovery v1.0', 'acknowledgment': 'European Commission Copernicus Programme', 'cdm_data_type': 'swath', 'comment': 'GHRSST SST L2P', 'creator_email': 'ops@eumetsat.int', 'creator_name': 'EUMETSAT', 'creator_url': 'http://navigator.eumetsat.int/', 'date_created': '20190719T021531Z', 'easternmost_longitude': 179.80088806152344, 'file_quality_level': 3, 'gds_version_id': '2.0r5', 'geospatial_lat_resolution': 0.009, 'geospatial_lat_units': 'degrees_north', 'geospatial_lon_resolution': 0.009, 'geospatial_lon_units': 'degrees_east', 'history': 'Sentinel-3 Optical Instrument Processing Facility  2019-07-19T02:15:31Z: PUGCoreProcessor /data/ipf-s3/workdir3/612042951/JobOrder.612042951.xml', 'id': 'SLSTRA-MAR-L2P-v1.0', 'institution': 'MAR', 'keywords': 'Oceans > Ocean Temperature > Sea Surface Temperature', 'keywords_vocabulary': 'NASA Global Change Master Directory (GCMD) Science Keywords', 'license': 'Copernicus Programme free, full and open data policy', 'metadata_link': 'N/A', 'naming_authority': 'org.ghrsst', 'netcdf_version_id': '4.2 of Jul  5 2012 17:07:43 $', 'northernmost_latitude': 73.35906219482422, 'platform': 'Sentinel3A', 'processing_level': 'L2P', 'product_version': '1.0', 'project': 'Group for High Resolution Sea Surface Temperature', 'publisher_email': 'ghrsst-po@nceo.ac.uk', 'publisher_name': 'The GHRSST Project Office', 'publisher_url': 'http://www.ghrsst.org', 'references': 'S3IPF PDS 005.3 - i2r7 - Product Data Format Specification - SLSTR Level 2 Marine, S3IPF PDS 002 - i1r7 - Product Data Format Specification - Product Structures, S3IPF DPM 007 - i1r8 - Detailed Processing Model - SLSTR Level 2', 'sensor': 'SLSTR', 'source': 'IPF-SL-2 06.16', 'southernmost_latitude': 59.30952453613281, 'spatial_resolution': '1 km at nadir', 'standard_name_vocabulary': 'NetCDF Climate and Forecast (CF) Metadata Convention', 'start_time': '20190719T000109Z', 'stop_time': '20190719T000409Z', 'summary': 'Sentinel-3A SLSTR skin sea surface temperature', 'time_coverage_end': datetime.datetime(2019, 7, 19, 0, 4, 9, tzinfo=tzutc()), 'time_coverage_start': datetime.datetime(2019, 7, 19, 0, 1, 9, tzinfo=tzutc()), 'title': 'Sentinel-3A SLSTR L2P SST dataset', 'uuid': 'TBC', 'westernmost_longitude': -179.07666015625}

Then instantiate a feature class object from the above dataset object. This
product corresponds to a Swath feature, which can be created from the content of
the above dataset:

>>> from cerbere.feature.swath import Swath
>>> swath = Swath(prod)
>>> print(swath)
Feature : Swath
Dataset: GHRSSTNCDataset
Feature Dims :
   .	row : 1202
   .	cell : 1500
Other Dims :
   .	channel : 3
Feature Coordinates :
   .	lat  (row: 1202, cell: 1500)
   .	lon  (row: 1202, cell: 1500)
   .	time  (row: 1202, cell: 1500)
Other Coordinates :
Fields :
   .	adi_dtime_from_sst  (row: 1202, cell: 1500)
   .	aerosol_dynamic_indicator  (row: 1202, cell: 1500)
   .	brightness_temperature  (channel: 3, row: 1202, cell: 1500)
   .	dt_analysis  (row: 1202, cell: 1500)
   .	dual_nadir_sst_difference  (row: 1202, cell: 1500)
   .	l2p_flags  (row: 1202, cell: 1500)
   .	nadir_sst_theoretical_uncertainty  (row: 1202, cell: 1500)
   .	nedt  (channel: 3, row: 1202, cell: 1500)
   .	probability_cloud_single_in  (row: 1202, cell: 1500)
   .	probability_cloud_single_io  (row: 1202, cell: 1500)
   .	quality_level  (row: 1202, cell: 1500)
   .	satellite_zenith_angle  (row: 1202, cell: 1500)
   .	sea_ice_fraction  (row: 1202, cell: 1500)
   .	sea_ice_fraction_dtime_from_sst  (row: 1202, cell: 1500)
   .	sea_surface_temperature  (row: 1202, cell: 1500)
   .	sses_bias  (row: 1202, cell: 1500)
   .	sses_standard_deviation  (row: 1202, cell: 1500)
   .	sst_algorithm_type  (row: 1202, cell: 1500)
   .	sst_theoretical_uncertainty  (row: 1202, cell: 1500)
   .	wind_speed  (row: 1202, cell: 1500)
   .	wind_speed_dtime_from_sst  (row: 1202, cell: 1500)
Global Attributes :
   .	Conventions	CF-1.6, Unidata Observation Dataset v1.0
   .	Metadata_Conventions	Unidata Dataset Discovery v1.0
   .	acknowledgment	European Commission Copernicus Programme
   .	cdm_data_type	swath
   .	comment	GHRSST SST L2P
   .	creator_email	ops@eumetsat.int
   .	creator_name	EUMETSAT
   .	creator_url	http://navigator.eumetsat.int/
   .	date_created	20190719T021531Z
   .	easternmost_longitude	179.80088806152344
   .	file_quality_level	3
   .	gds_version_id	2.0r5
   .	geospatial_lat_resolution	0.009
   .	geospatial_lat_units	degrees_north
   .	geospatial_lon_resolution	0.009
   .	geospatial_lon_units	degrees_east
   .	history	Sentinel-3 Optical Instrument Processing Facility  2019-07-19T02:15:31Z: PUGCoreProcessor /data/ipf-s3/workdir3/612042951/JobOrder.612042951.xml
   .	id	SLSTRA-MAR-L2P-v1.0
   .	institution	MAR
   .	keywords	Oceans > Ocean Temperature > Sea Surface Temperature
   .	keywords_vocabulary	NASA Global Change Master Directory (GCMD) Science Keywords
   .	license	Copernicus Programme free, full and open data policy
   .	metadata_link	N/A
   .	naming_authority	org.ghrsst
   .	netcdf_version_id	4.2 of Jul  5 2012 17:07:43 $
   .	northernmost_latitude	73.35906219482422
   .	platform	Sentinel3A
   .	processing_level	L2P
   .	product_version	1.0
   .	project	Group for High Resolution Sea Surface Temperature
   .	publisher_email	ghrsst-po@nceo.ac.uk
   .	publisher_name	The GHRSST Project Office
   .	publisher_url	http://www.ghrsst.org
   .	references	S3IPF PDS 005.3 - i2r7 - Product Data Format Specification - SLSTR Level 2 Marine, S3IPF PDS 002 - i1r7 - Product Data Format Specification - Product Structures, S3IPF DPM 007 - i1r8 - Detailed Processing Model - SLSTR Level 2
   .	sensor	SLSTR
   .	source	IPF-SL-2 06.16
   .	southernmost_latitude	59.30952453613281
   .	spatial_resolution	1 km at nadir
   .	standard_name_vocabulary	NetCDF Climate and Forecast (CF) Metadata Convention
   .	start_time	20190719T000109Z
   .	stop_time	20190719T000409Z
   .	summary	Sentinel-3A SLSTR skin sea surface temperature
   .	time_coverage_end	2019-07-19 00:04:09+00:00
   .	time_coverage_start	2019-07-19 00:01:09+00:00
   .	title	Sentinel-3A SLSTR L2P SST dataset
   .	uuid	TBC
   .	westernmost_longitude	-179.07666015625
dimensions names for a satellite swath observation feature):

>>> prod.dims
OrderedDict([('cell', 1500), ('channel', 3), ('row', 1202)])

Get the size of a dimension (standard names can be used for geolocation
dimensions):

>>> prod.get_dimsize('row')    # standard dimension name for Swath feature
1202

Get a field and print it:

>>> field = prod.get_field('sea_surface_temperature')
>>> print(field)
Field : 'sea_surface_temperature'
    sea surface skin temperature
    dimensions :
      # row : 1202
      # cell : 1500
    standard CF attributes :
      # standard_name : sea_surface_skin_temperature
      # units : kelvin
      # valid_max : 4500
      # valid_min : -300
      # fill_value : -32768
    other attributes :
      # comment : Marine skin surface temperature
      # depth : 10 micrometres
      # quality_vars : None


Get the field attributes, as a dictionary:

>>> prod.get_field_attrs('sea_surface_temperature')
{'comment': 'Marine skin surface temperature', 'depth': '10 micrometres', 'long_name': 'sea surface skin temperature', 'standard_name': 'sea_surface_skin_temperature', 'units': 'kelvin', 'valid_max': 4500, 'valid_min': -300, 'quality_vars': None}

or:

>>> prod.get_field('sea_surface_temperature').attrs
{'comment': 'Marine skin surface temperature', 'depth': '10 micrometres', 'long_name': 'sea surface skin temperature', 'standard_name': 'sea_surface_skin_temperature', 'units': 'kelvin', 'valid_max': 4500, 'valid_min': -300, 'quality_vars': None}

Get the dimensions (like in netCDF) of a particular field:

>>> prod.get_field_sizes('sea_surface_temperature')
OrderedDict([('row', 1202), ('cell', 1500)])

or:

>>> prod.get_field('sea_surface_temperature').dims
OrderedDict([('row', 1202), ('cell', 1500)])


Get the dataset global attributes, as a dictionary:

>>> prod.attrs
{'Conventions': 'CF-1.6, Unidata Observation Dataset v1.0', 'Metadata_Conventions': 'Unidata Dataset Discovery v1.0', 'acknowledgment': 'European Commission Copernicus Programme', 'cdm_data_type': 'swath', 'comment': 'GHRSST SST L2P', 'creator_email': 'ops@eumetsat.int', 'creator_name': 'EUMETSAT', 'creator_url': 'http://navigator.eumetsat.int/', 'date_created': '20190719T021531Z', 'easternmost_longitude': 179.80088806152344, 'file_quality_level': 3, 'gds_version_id': '2.0r5', 'geospatial_lat_resolution': 0.009, 'geospatial_lat_units': 'degrees_north', 'geospatial_lon_resolution': 0.009, 'geospatial_lon_units': 'degrees_east', 'history': 'Sentinel-3 Optical Instrument Processing Facility  2019-07-19T02:15:31Z: PUGCoreProcessor /data/ipf-s3/workdir3/612042951/JobOrder.612042951.xml', 'id': 'SLSTRA-MAR-L2P-v1.0', 'institution': 'MAR', 'keywords': 'Oceans > Ocean Temperature > Sea Surface Temperature', 'keywords_vocabulary': 'NASA Global Change Master Directory (GCMD) Science Keywords', 'license': 'Copernicus Programme free, full and open data policy', 'metadata_link': 'N/A', 'naming_authority': 'org.ghrsst', 'netcdf_version_id': '4.2 of Jul  5 2012 17:07:43 $', 'northernmost_latitude': 73.35906219482422, 'platform': 'Sentinel3A', 'processing_level': 'L2P', 'product_version': '1.0', 'project': 'Group for High Resolution Sea Surface Temperature', 'publisher_email': 'ghrsst-po@nceo.ac.uk', 'publisher_name': 'The GHRSST Project Office', 'publisher_url': 'http://www.ghrsst.org', 'references': 'S3IPF PDS 005.3 - i2r7 - Product Data Format Specification - SLSTR Level 2 Marine, S3IPF PDS 002 - i1r7 - Product Data Format Specification - Product Structures, S3IPF DPM 007 - i1r8 - Detailed Processing Model - SLSTR Level 2', 'sensor': 'SLSTR', 'source': 'IPF-SL-2 06.16', 'southernmost_latitude': 59.30952453613281, 'spatial_resolution': '1 km at nadir', 'standard_name_vocabulary': 'NetCDF Climate and Forecast (CF) Metadata Convention', 'start_time': '20190719T000109Z', 'stop_time': '20190719T000409Z', 'summary': 'Sentinel-3A SLSTR skin sea surface temperature', 'time_coverage_end': datetime.datetime(2019, 7, 19, 0, 4, 9, tzinfo=tzutc()), 'time_coverage_start': datetime.datetime(2019, 7, 19, 0, 1, 9, tzinfo=tzutc()), 'title': 'Sentinel-3A SLSTR L2P SST dataset', 'uuid': 'TBC', 'westernmost_longitude': -179.07666015625}

Then instantiate a feature class object from the above dataset object. This
product corresponds to a Swath feature, which can be created from the content of
the above dataset:

>>> from cerbere.feature.swath import Swath
>>> swath = Swath(prod)
>>> print(swath)
Feature : Swath
Dataset: GHRSSTNCDataset
Feature Dims :
   .	row : 1202
   .	cell : 1500
Other Dims :
   .	channel : 3
Feature Coordinates :
   .	lat  (row: 1202, cell: 1500)
   .	lon  (row: 1202, cell: 1500)
   .	time  (row: 1202, cell: 1500)
Other Coordinates :
Fields :
   .	adi_dtime_from_sst  (row: 1202, cell: 1500)
   .	aerosol_dynamic_indicator  (row: 1202, cell: 1500)
   .	brightness_temperature  (channel: 3, row: 1202, cell: 1500)
   .	dt_analysis  (row: 1202, cell: 1500)
   .	dual_nadir_sst_difference  (row: 1202, cell: 1500)
   .	l2p_flags  (row: 1202, cell: 1500)
   .	nadir_sst_theoretical_uncertainty  (row: 1202, cell: 1500)
   .	nedt  (channel: 3, row: 1202, cell: 1500)
   .	probability_cloud_single_in  (row: 1202, cell: 1500)
   .	probability_cloud_single_io  (row: 1202, cell: 1500)
   .	quality_level  (row: 1202, cell: 1500)
   .	satellite_zenith_angle  (row: 1202, cell: 1500)
   .	sea_ice_fraction  (row: 1202, cell: 1500)
   .	sea_ice_fraction_dtime_from_sst  (row: 1202, cell: 1500)
   .	sea_surface_temperature  (row: 1202, cell: 1500)
   .	sses_bias  (row: 1202, cell: 1500)
   .	sses_standard_deviation  (row: 1202, cell: 1500)
   .	sst_algorithm_type  (row: 1202, cell: 1500)
   .	sst_theoretical_uncertainty  (row: 1202, cell: 1500)
   .	wind_speed  (row: 1202, cell: 1500)
   .	wind_speed_dtime_from_sst  (row: 1202, cell: 1500)
Global Attributes :
   .	Conventions	CF-1.6, Unidata Observation Dataset v1.0
   .	Metadata_Conventions	Unidata Dataset Discovery v1.0
   .	acknowledgment	European Commission Copernicus Programme
   .	cdm_data_type	swath
   .	comment	GHRSST SST L2P
   .	creator_email	ops@eumetsat.int
   .	creator_name	EUMETSAT
   .	creator_url	http://navigator.eumetsat.int/
   .	date_created	20190719T021531Z
   .	easternmost_longitude	179.80088806152344
   .	file_quality_level	3
   .	gds_version_id	2.0r5
   .	geospatial_lat_resolution	0.009
   .	geospatial_lat_units	degrees_north
   .	geospatial_lon_resolution	0.009
   .	geospatial_lon_units	degrees_east
   .	history	Sentinel-3 Optical Instrument Processing Facility  2019-07-19T02:15:31Z: PUGCoreProcessor /data/ipf-s3/workdir3/612042951/JobOrder.612042951.xml
   .	id	SLSTRA-MAR-L2P-v1.0
   .	institution	MAR
   .	keywords	Oceans > Ocean Temperature > Sea Surface Temperature
   .	keywords_vocabulary	NASA Global Change Master Directory (GCMD) Science Keywords
   .	license	Copernicus Programme free, full and open data policy
   .	metadata_link	N/A
   .	naming_authority	org.ghrsst
   .	netcdf_version_id	4.2 of Jul  5 2012 17:07:43 $
   .	northernmost_latitude	73.35906219482422
   .	platform	Sentinel3A
   .	processing_level	L2P
   .	product_version	1.0
   .	project	Group for High Resolution Sea Surface Temperature
   .	publisher_email	ghrsst-po@nceo.ac.uk
   .	publisher_name	The GHRSST Project Office
   .	publisher_url	http://www.ghrsst.org
   .	references	S3IPF PDS 005.3 - i2r7 - Product Data Format Specification - SLSTR Level 2 Marine, S3IPF PDS 002 - i1r7 - Product Data Format Specification - Product Structures, S3IPF DPM 007 - i1r8 - Detailed Processing Model - SLSTR Level 2
   .	sensor	SLSTR
   .	source	IPF-SL-2 06.16
   .	southernmost_latitude	59.30952453613281
   .	spatial_resolution	1 km at nadir
   .	standard_name_vocabulary	NetCDF Climate and Forecast (CF) Metadata Convention
   .	start_time	20190719T000109Z
   .	stop_time	20190719T000409Z
   .	summary	Sentinel-3A SLSTR skin sea surface temperature
   .	time_coverage_end	2019-07-19 00:04:09+00:00
   .	time_coverage_start	2019-07-19 00:01:09+00:00
   .	title	Sentinel-3A SLSTR L2P SST dataset
   .	uuid	TBC
   .	westernmost_longitude	-179.07666015625
dimensions names for a satellite swath observation feature):

>>> prod.dims
OrderedDict([('cell', 1500), ('channel', 3), ('row', 1202)])

Get the size of a dimension (standard names can be used for geolocation
dimensions):

>>> prod.get_dimsize('row')    # standard dimension name for Swath feature
1202

Get a field and print it:

>>> field = prod.get_field('sea_surface_temperature')
>>> print(field)
Field : 'sea_surface_temperature'
    sea surface skin temperature
    dimensions :
      # row : 1202
      # cell : 1500
    standard CF attributes :
      # standard_name : sea_surface_skin_temperature
      # units : kelvin
      # valid_max : 4500
      # valid_min : -300
      # fill_value : -32768
    other attributes :
      # comment : Marine skin surface temperature
      # depth : 10 micrometres
      # quality_vars : None


Get the field attributes, as a dictionary:

>>> prod.get_field_attrs('sea_surface_temperature')
{'comment': 'Marine skin surface temperature', 'depth': '10 micrometres', 'long_name': 'sea surface skin temperature', 'standard_name': 'sea_surface_skin_temperature', 'units': 'kelvin', 'valid_max': 4500, 'valid_min': -300, 'quality_vars': None}

or:

>>> prod.get_field('sea_surface_temperature').attrs
{'comment': 'Marine skin surface temperature', 'depth': '10 micrometres', 'long_name': 'sea surface skin temperature', 'standard_name': 'sea_surface_skin_temperature', 'units': 'kelvin', 'valid_max': 4500, 'valid_min': -300, 'quality_vars': None}

Get the dimensions (like in netCDF) of a particular field:

>>> prod.get_field_dims('sea_surface_temperature')
OrderedDict([('row', 1202), ('cell', 1500)])

or:

>>> prod.get_field('sea_surface_temperature').dims
OrderedDict([('row', 1202), ('cell', 1500)])


Get the dataset global attributes, as a dictionary:

>>> prod.attrs
{'Conventions': 'CF-1.6, Unidata Observation Dataset v1.0', 'Metadata_Conventions': 'Unidata Dataset Discovery v1.0', 'acknowledgment': 'European Commission Copernicus Programme', 'cdm_data_type': 'swath', 'comment': 'GHRSST SST L2P', 'creator_email': 'ops@eumetsat.int', 'creator_name': 'EUMETSAT', 'creator_url': 'http://navigator.eumetsat.int/', 'date_created': '20190719T021531Z', 'easternmost_longitude': 179.80088806152344, 'file_quality_level': 3, 'gds_version_id': '2.0r5', 'geospatial_lat_resolution': 0.009, 'geospatial_lat_units': 'degrees_north', 'geospatial_lon_resolution': 0.009, 'geospatial_lon_units': 'degrees_east', 'history': 'Sentinel-3 Optical Instrument Processing Facility  2019-07-19T02:15:31Z: PUGCoreProcessor /data/ipf-s3/workdir3/612042951/JobOrder.612042951.xml', 'id': 'SLSTRA-MAR-L2P-v1.0', 'institution': 'MAR', 'keywords': 'Oceans > Ocean Temperature > Sea Surface Temperature', 'keywords_vocabulary': 'NASA Global Change Master Directory (GCMD) Science Keywords', 'license': 'Copernicus Programme free, full and open data policy', 'metadata_link': 'N/A', 'naming_authority': 'org.ghrsst', 'netcdf_version_id': '4.2 of Jul  5 2012 17:07:43 $', 'northernmost_latitude': 73.35906219482422, 'platform': 'Sentinel3A', 'processing_level': 'L2P', 'product_version': '1.0', 'project': 'Group for High Resolution Sea Surface Temperature', 'publisher_email': 'ghrsst-po@nceo.ac.uk', 'publisher_name': 'The GHRSST Project Office', 'publisher_url': 'http://www.ghrsst.org', 'references': 'S3IPF PDS 005.3 - i2r7 - Product Data Format Specification - SLSTR Level 2 Marine, S3IPF PDS 002 - i1r7 - Product Data Format Specification - Product Structures, S3IPF DPM 007 - i1r8 - Detailed Processing Model - SLSTR Level 2', 'sensor': 'SLSTR', 'source': 'IPF-SL-2 06.16', 'southernmost_latitude': 59.30952453613281, 'spatial_resolution': '1 km at nadir', 'standard_name_vocabulary': 'NetCDF Climate and Forecast (CF) Metadata Convention', 'start_time': '20190719T000109Z', 'stop_time': '20190719T000409Z', 'summary': 'Sentinel-3A SLSTR skin sea surface temperature', 'time_coverage_end': datetime.datetime(2019, 7, 19, 0, 4, 9, tzinfo=tzutc()), 'time_coverage_start': datetime.datetime(2019, 7, 19, 0, 1, 9, tzinfo=tzutc()), 'title': 'Sentinel-3A SLSTR L2P SST dataset', 'uuid': 'TBC', 'westernmost_longitude': -179.07666015625}

Then instantiate a feature class object from the above dataset object. This
product corresponds to a Swath feature, which can be created from the content of
the above dataset:

>>> from cerbere.feature.swath import Swath
>>> swath = Swath(prod)
>>> print(swath)
Feature : Swath
Dataset: GHRSSTNCDataset
Feature Dims :
   .	row : 1202
   .	cell : 1500
Other Dims :
   .	channel : 3
Feature Coordinates :
   .	lat  (row: 1202, cell: 1500)
   .	lon  (row: 1202, cell: 1500)
   .	time  (row: 1202, cell: 1500)
Other Coordinates :
Fields :
   .	adi_dtime_from_sst  (row: 1202, cell: 1500)
   .	aerosol_dynamic_indicator  (row: 1202, cell: 1500)
   .	brightness_temperature  (channel: 3, row: 1202, cell: 1500)
   .	dt_analysis  (row: 1202, cell: 1500)
   .	dual_nadir_sst_difference  (row: 1202, cell: 1500)
   .	l2p_flags  (row: 1202, cell: 1500)
   .	nadir_sst_theoretical_uncertainty  (row: 1202, cell: 1500)
   .	nedt  (channel: 3, row: 1202, cell: 1500)
   .	probability_cloud_single_in  (row: 1202, cell: 1500)
   .	probability_cloud_single_io  (row: 1202, cell: 1500)
   .	quality_level  (row: 1202, cell: 1500)
   .	satellite_zenith_angle  (row: 1202, cell: 1500)
   .	sea_ice_fraction  (row: 1202, cell: 1500)
   .	sea_ice_fraction_dtime_from_sst  (row: 1202, cell: 1500)
   .	sea_surface_temperature  (row: 1202, cell: 1500)
   .	sses_bias  (row: 1202, cell: 1500)
   .	sses_standard_deviation  (row: 1202, cell: 1500)
   .	sst_algorithm_type  (row: 1202, cell: 1500)
   .	sst_theoretical_uncertainty  (row: 1202, cell: 1500)
   .	wind_speed  (row: 1202, cell: 1500)
   .	wind_speed_dtime_from_sst  (row: 1202, cell: 1500)
Global Attributes :
   .	Conventions	CF-1.6, Unidata Observation Dataset v1.0
   .	Metadata_Conventions	Unidata Dataset Discovery v1.0
   .	acknowledgment	European Commission Copernicus Programme
   .	cdm_data_type	swath
   .	comment	GHRSST SST L2P
   .	creator_email	ops@eumetsat.int
   .	creator_name	EUMETSAT
   .	creator_url	http://navigator.eumetsat.int/
   .	date_created	20190719T021531Z
   .	easternmost_longitude	179.80088806152344
   .	file_quality_level	3
   .	gds_version_id	2.0r5
   .	geospatial_lat_resolution	0.009
   .	geospatial_lat_units	degrees_north
   .	geospatial_lon_resolution	0.009
   .	geospatial_lon_units	degrees_east
   .	history	Sentinel-3 Optical Instrument Processing Facility  2019-07-19T02:15:31Z: PUGCoreProcessor /data/ipf-s3/workdir3/612042951/JobOrder.612042951.xml
   .	id	SLSTRA-MAR-L2P-v1.0
   .	institution	MAR
   .	keywords	Oceans > Ocean Temperature > Sea Surface Temperature
   .	keywords_vocabulary	NASA Global Change Master Directory (GCMD) Science Keywords
   .	license	Copernicus Programme free, full and open data policy
   .	metadata_link	N/A
   .	naming_authority	org.ghrsst
   .	netcdf_version_id	4.2 of Jul  5 2012 17:07:43 $
   .	northernmost_latitude	73.35906219482422
   .	platform	Sentinel3A
   .	processing_level	L2P
   .	product_version	1.0
   .	project	Group for High Resolution Sea Surface Temperature
   .	publisher_email	ghrsst-po@nceo.ac.uk
   .	publisher_name	The GHRSST Project Office
   .	publisher_url	http://www.ghrsst.org
   .	references	S3IPF PDS 005.3 - i2r7 - Product Data Format Specification - SLSTR Level 2 Marine, S3IPF PDS 002 - i1r7 - Product Data Format Specification - Product Structures, S3IPF DPM 007 - i1r8 - Detailed Processing Model - SLSTR Level 2
   .	sensor	SLSTR
   .	source	IPF-SL-2 06.16
   .	southernmost_latitude	59.30952453613281
   .	spatial_resolution	1 km at nadir
   .	standard_name_vocabulary	NetCDF Climate and Forecast (CF) Metadata Convention
   .	start_time	20190719T000109Z
   .	stop_time	20190719T000409Z
   .	summary	Sentinel-3A SLSTR skin sea surface temperature
   .	time_coverage_end	2019-07-19 00:04:09+00:00
   .	time_coverage_start	2019-07-19 00:01:09+00:00
   .	title	Sentinel-3A SLSTR L2P SST dataset
   .	uuid	TBC
   .	westernmost_longitude	-179.07666015625



Saving data
-----------

Extracting sub-features
-----------------------
