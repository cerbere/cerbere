"""
Reader class for KNMI / OSI SAF scatterometer Level 2 NETCDF files

Example of file pattern:
ascat_20230210_165700_metopc_22116_eps_o_coa_3301_ovw.l2.nc
"""
from pathlib import Path
import re
import typing as T

from .basereader import BaseReader


class OSISAFSCATL2(BaseReader):

    pattern = re.compile(r"[a-z]*_[0-9]{8}_[0-9]{6}.*_ovw.l2.nc$")
    engine: str = "netcdf4"
    description: str = "Use OSI SAF NetCDF scatterometer files in Xarray and " \
                   "cerbere"
    url: str = "https://link_to/your_backend/documentation"

    @classmethod
    def postprocess(cls, ds, **kwargs):
        ds.cb.cerberize(
            dim_matching={
                'numrows': 'row',
                'NUMROWS': 'row',
                'numcells': 'cell',
                'NUMCELLS': 'cell',
                'NUMAMBIGS': 'solutions',
                'NUMVIEWS': 'views'
            },
            var_matching={
                'row_time': 'time',
                'wvc_lat': 'lat',
                'wvc_lon': 'lon'
            }
        )

        # normalize longitude to -180/180
        ds.cb.cfdataset['lon'] = ds.cb.cfdataset['lon'].where(
            ds.cb.cfdataset['lon'] < 180,
            ds.cb.cfdataset['lon'] - 360
        )

        return ds.cb.cfdataset
