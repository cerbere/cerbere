# ---------------------------------------------------------------
# Global
# ---------------------------------------------------------------

# default image
image: continuumio/miniconda3:latest

# stages (main steps of pipeline)
stages:
#  - Quality
#  - Tests
#  - Sonarqube
  - Documentation
#  - Deploy
  - Publish

# ---------------------------------------------------------------
# Jobs templates
# ---------------------------------------------------------------

.code-changes-template: &code-changes
  only:
    changes:
      - ${CI_PROJECT_NAME}/**/*.py
      - tests/**/*.py
      - .gitlab-ci.yml
      - pyproject.toml

.install-deps-conda-template-37: &install-conda-deps-37
  before_script:
    - conda create -n cerbere python=3.7
    - source activate cerbere
    - conda install -c conda-forge pygrib
    - pip install poetry poetry-dynamic-versioning poetry2conda
    - poetry config virtualenvs.in-project false
    - poetry install -vv

.install-deps-conda-template-38: &install-conda-deps-38
  before_script:
    - conda create -n cerbere python=3.8
    - source activate cerbere
    - conda install -c conda-forge pygrib
    - pip install poetry poetry-dynamic-versioning poetry2conda
    - poetry config virtualenvs.in-project false
    - poetry install -vv

.install-deps-conda-template-39: &install-conda-deps-39
  before_script:
    - conda create -n cerbere python=3.9
    - source activate cerbere
    - conda install -c conda-forge pygrib
    - pip install poetry poetry-dynamic-versioning poetry2conda
    - poetry config virtualenvs.in-project false
    - poetry install -vv


.quality-template: &quality
  <<: *install-conda-deps-38
  <<: *code-changes
  stage: Quality
  allow_failure: true
  except:
    - tags

.test-template-37: &test_37
  <<: *install-conda-deps-37
  <<: *code-changes
  stage: Tests
  script:
    - poetry run pytest tests/
  except:
    - tags

.test-template-38: &test_38
  <<: *install-conda-deps-38
  <<: *code-changes
  stage: Tests
  script:
    - poetry run pytest tests/
  except:
    - tags

# ---------------------------------------------------------------
# Quality jobs
# ---------------------------------------------------------------
#flake8:
#  <<: *quality
#  script:
#    - poetry run flake8 --max-line-length=120 --docstring-convention google ${CI_PROJECT_NAME}
#
#pylint:
#  <<: *quality
#  script:
#    #- poetry run pylint -j8 -E ${CI_PROJECT_NAME} tests
#    - "poetry run pylint --exit-zero ${CI_PROJECT_NAME} tests -r n --max-line-length=120 --msg-template='{path}:{line}: [{msg_id}({symbol}), {obj}] {msg}' | tee pylint.txt"
#  artifacts:
#    expire_in: 1 week
#    paths:
#      - pylint.txt

# ---------------------------------------------------------------
# Test jobs
# ---------------------------------------------------------------
#python3.7:
#  <<: *test_37
#  image: continuumio/miniconda3
#  allow_failure: true
#  script:
#    - poetry run pytest tests/
#
#python3.8:
#  <<: *test_38
#  image: continuumio/miniconda3
#  allow_failure: true
#  script:
#    - poetry run pytest tests/
#
#python3.9:
#  <<: *install-conda-deps-39
#  <<: *code-changes
#  stage: Tests
#  image: continuumio/miniconda3
#  coverage: '/^TOTAL\s+\d+\s+\d+\s+\d+\s+\d+\s+(\d+\%)/'
#  allow_failure: true
#  script:
#    - poetry run pytest --cov=${CI_PROJECT_NAME} --cov-branch --cov-report=term tests/
#    - poetry run pytest -ra -q --cov=${CI_PROJECT_NAME} --cov-branch --cov-report xml:coverage.xml tests/
#  artifacts:
#    expire_in: 1 week
#    paths:
#      - coverage.xml
#  except:
#    - tags

# ---------------------------------------------------------------
# SonarQube
# ---------------------------------------------------------------
#sonarqube:
#  stage: Sonarqube
#  tags: [cerbere-runner]
#  image:
#    name: sonarsource/sonar-scanner-cli:latest
#    entrypoint: [""]
#  allow_failure: true
#  script:
#    - sonar-scanner
#      -Dsonar.projectKey=${CI_PROJECT_NAME}
#      -Dsonar.language=py
#      -Dsonar.host.url=http://visi-common-sonar:9000
#      -Dsonar.login=e6f816eee72d3d5c03319ec74b468157b9164d12
#      -Dsonar.sourceEncoding=UTF-8
#      -Dsonar.python.coverage.reportPaths=coverage.xml
#      -Dsonar.coverage.exclusions=**__init__**,tests/**
#      -Dsonar.python.pylint.reportPath=pylint.txt
#  <<: *code-changes
#  except:
#    - tags

# ---------------------------------------------------------------
# Documentation job
# ---------------------------------------------------------------
pages:
  <<: *install-conda-deps-38
  image: continuumio/miniconda3 # does not work with python 3.9
  stage: Documentation
  #allow_failure: true
  script:
    - poetry run sphinx-build -b html docs public
  only:
    refs:
      - master
    changes:
      - docs/**/*
      - .gitlab-ci.yml
  artifacts:
    paths:
      - public
  except:
    - tags

## ---------------------------------------------------------------
## Release job
## ---------------------------------------------------------------
#nexus:
#  <<: *install-conda-deps-38
#  stage: Deploy
#  script:
#    - poetry build --format wheel
#    - poetry config repositories.nexus-public-release https://nexus-test.ifremer.fr/repository/hosted-pypi-public-release/
#    - poetry publish -r nexus-public-release -u nexus-ci -p w2bH2NjgFmQnzVk3
#  only:
#    - tags

# ---------------------------------------------------------------
# Release job
# ---------------------------------------------------------------
publish_pypi_on_gitlab:
  stage: Publish
  tags: [cerbere-runner]
  rules:
    # run only if valid tag and step enable
    # - if: '$PIPELINE_ENABLE_PUBLISH_PYPI == "true" && $CI_COMMIT_BRANCH =~ "^v?(?P<base>\\d+\\.\\d+\\.\\d+)(-?((?P<stage>[a-zA-Z]+)\\.?(?P<revision>\\d+)?))?$"'
    - if: '$CI_COMMIT_BRANCH =~ "^v?(?P<base>\\d+\\.\\d+\\.\\d+)(-?((?P<stage>[a-zA-Z]+)\\.?(?P<revision>\\d+)?))?$"'
      when: always
    - when: never
  image:
    name: python:3.9
  script:
    - pip install poetry poetry-dynamic-versioning
    - poetry build --format wheel
    - poetry config repositories.release $PYPI_URL
    - poetry publish -r release -u $PYPI_USER -p $PYPI_PWD
