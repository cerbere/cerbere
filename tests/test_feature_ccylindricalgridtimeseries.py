from cerbere.feature.cgridtimeseries import CylindricalGridTimeSeries
from cerbere.feature.ctrajectory import Trajectory

from cerbere.feature.pytest_cfeature import *


@pytest.fixture(scope='module')
def feature_class():
    return CylindricalGridTimeSeries


@pytest.fixture(scope='module')
def feature_instance():
    # creates a test xarray object
    lon = xr.DataArray(data=np.arange(-180, 180, 1), dims=['lon'])
    lat = xr.DataArray(data=np.arange(-80, 80, 1), dims=['lat'])
    time = xr.DataArray([
        np.datetime64('2018-01-01'),
        np.datetime64('2018-01-02'),
        np.datetime64('2018-01-03'),
        np.datetime64('2018-01-04'),
        np.datetime64('2018-01-05')
    ], dims='time')
    var = xr.DataArray(
        data=np.ones(shape=(5, 160, 360)),
        dims=['time', 'lat', 'lon'],
        attrs={'myattr': 'test_attr_val'}
    )
    xrdataset = xr.Dataset(
        coords={'lat': lat, 'lon': lon, 'time': time},
        data_vars={'my_var': var},
        attrs={'gattr1': 'gattr1_val', 'gattr2': 'gattr2_val'}
    )

    return CylindricalGridTimeSeries(xrdataset)


@pytest.fixture(scope='module')
def feature_dimnames():
    return 'lat', 'lon', 'time',


@pytest.fixture(scope='module')
def feature_dimsizes():
    return 160, 360, 5,


@pytest.fixture(scope='module')
def content_class():
    return


def test_create_feature_with_incorrect_geodim_order():
        basefeat = self.define_base_feature()
        feat = self.get_feature_class()(
            {
                'lat': {'data': basefeat.get_lat(), 'dims': ('lat',)},
                'lon': {'data': basefeat.get_lon(), 'dims': ('lon',)},
                'time': {
                    'data': basefeat.get_times(), 'dims': ('time',)
                },
                'my_var': {
                    'data': np.ones(shape=(5, 360, 160)),
                    'dims': ('time', 'lon', 'lat')
                }
            }
        )
        self.assertEqual(
            feat.get_field_dims('myvar'), ('time', 'lat', 'lon')
        )
        print('Feature from: test_create_feature_with_incorrect_geodim_order')
        print(feat)


def test_create_feature_from_dict_datetime_1d(feature_instance, feature_class):
        feat = feature_class(xr.Dataset(xr.Dataset.from_dict({
            'coords': {
                'lat': {'data': feature_instance.dataset.lat.values,
                        'dims': ('lat',)},
                'lon': {'data': feature_instance.dataset.lon.values,
                        'dims': ('lon',)},
                'time': {
                    'data': [feature_instance.dataset.time.values[0]],
                    'dims': ('time',)
                }},
            'data_vars': {
                'my_var': {
                    'data': feature_instance.dataset.my_var.values[0:1, :, :],
                    'dims': ('time', 'lat', 'lon')
                }}
            }
        )))
        assert isinstance(feat, feature_class)
        assert feat.dataset.time.sizes == {'time': 1}
        print('Feature from: test_create_feature_from_dict_datetime_1d')
        print(feat)


def test_expanded_latlon(feature_instance):
        res = feature_instance.dataset.lat.cb.isel(
            {'lat': slice(10, 15), 'lon': slice(50, 55)})

        assert res.shape == (5,)
        assert np.equal(res, np.arange(-70, -65)).all()

        res = feature_instance.dataset.lat.cb.isel(
            {'lat': 10, 'lon': 50},
            broadcast_coords=[feature_instance.dataset['lat'],
                              feature_instance.dataset['lon']])

        assert res == -70

        res = feature_instance.dataset.lon.cb.isel(
            {'lat': 10, 'lon': 50},
            broadcast_coords=[feature_instance.dataset['lat'],
                              feature_instance.dataset['lon']])
        assert res == -130

        res = feature_instance.dataset['lat'].cb.isel(
            {'lat': slice(10, 15), 'lon': slice(50, 53)},
            broadcast_coords=[feature_instance.dataset['lat'],
                              feature_instance.dataset['lon']]
        )
        assert res.shape == (5, 3,)
        assert np.equal(res[:, 0], np.arange(-70, -65)).all()
        assert np.equal(res[0, :], np.ones((3,)) * -70).all()

        res = feature_instance.dataset['lon'].cb.isel(
            {'lat': slice(10, 15), 'lon': slice(50, 53)},
            broadcast_coords=[feature_instance.dataset['lat'],
                              feature_instance.dataset['lon']]
        )
        assert res.shape == (5, 3,)
        assert np.equal(res[0, :], np.arange(-130, -127)).all()
        assert np.equal(res[:, 0], np.ones((5,)) * -130).all()

        feature_instance.dataset['lat'].cb.isel(
            broadcast_coords=[feature_instance.dataset['lat'],
                              feature_instance.dataset['lon']]
        )
        print('result: test_expanded_latlon ', res)


def test_expanded_latlon_2(feature_instance):

    res = feature_instance.dataset['time'].cb.isel(
        {'lat': slice(10, 15), 'lon': slice(50, 53)},
        broadcast_coords=[feature_instance.dataset['lat'],
                          feature_instance.dataset['lon']]
    )
    assert res.sizes == {'lat': 5, 'lon': 3, 'time': 5}

    res = feature_instance.dataset['time'].cb.isel(
        broadcast_coords=[feature_instance.dataset['lat'],
                          feature_instance.dataset['lon']])

    print('result: test_expanded_time ', res)


def test_trajectory(feature_instance):

    # create a trajectory
    lon = xr.DataArray(data=np.arange(7), dims=['time'])
    lat = xr.DataArray(data=np.arange(7), dims=['time'])
    time = xr.DataArray([
        datetime(2018, 1, 1),
        datetime(2018, 1, 2),
        datetime(2018, 1, 3),
        datetime(2018, 1, 3, 12),
        datetime(2018, 1, 4),
        datetime(2018, 1, 5),
        datetime(2018, 1, 6)
    ], dims='time')
    xrdataset = xr.Dataset(
        coords={'lat': lat, 'lon': lon, 'time': time},
    )
    traj = Trajectory(xrdataset)

    # extract a collocated trajectory from the feature
    extracted_traj = feature_instance.trajectory(traj)
    print(extracted_traj)
