import unittest

import numpy as np
import xarray as xr

from cerbere.dataset.field import Field

N_VALUES = 10

class TestMaskedArray(unittest.TestCase):
    """Test class for checking fill values"""

    def test_field_ma_float32_auto_fillvalue(self):
        data = np.ma.ones((N_VALUES,), dtype=np.float32)
        field = Field(
            data=data,
            dims=['time'],
            name='myvar')
        self.assertEqual(field.get_values().count(), N_VALUES)

        data = np.ma.masked_where(
            (np.arange(N_VALUES) >= N_VALUES/2), data, copy=True)
        field.set_values(data)
        self.assertEqual(field.get_values().count(), N_VALUES/2)
        self.assertEqual(field.get_values().dtype.name, 'float32')

    def test_field_ma_float32(self):
        data = np.ma.ones((N_VALUES,), dtype=np.float32)
        field = Field(
            data=data,
            dims=['time'],
            fill_value=1e5,
            name='myvar')
        self.assertEqual(field.get_values().count(), N_VALUES)

        data = np.ma.masked_where(
            (np.arange(N_VALUES) >= N_VALUES/2), data, copy=True)
        field.set_values(data)
        self.assertEqual(field.get_values().count(), N_VALUES/2)
        self.assertEqual(field.get_values().fill_value, 1e5)
        self.assertEqual(field.fill_value, 1e5)
        self.assertEqual(field.get_values().dtype.name, 'float32')


    def test_field_ma_int32_auto_fillvalue(self):
        data = np.ma.ones((N_VALUES,), dtype=np.int32)
        field = Field(
            data=data,
            dims=['time'],
            name='myvar')
        self.assertEqual(field.get_values().count(), N_VALUES)

        data = np.ma.masked_where(
            (np.arange(N_VALUES) >= N_VALUES / 2), data, copy=True)
        field.set_values(data)
        self.assertEqual(field.get_values().count(), N_VALUES / 2)
        self.assertEqual(field.get_values().dtype.name, 'int32')

    def test_field_ma_int32(self):
        data = np.ma.ones((N_VALUES,), dtype=np.int32)
        field = Field(
            data=data,
            fill_value=-1,
            dims=['time'],
            name='myvar')
        self.assertEqual(field.get_values().count(), N_VALUES)

        data = np.ma.masked_where(
            (np.arange(N_VALUES) >= N_VALUES / 2), data, copy=True)
        field.set_values(data)
        self.assertEqual(field.get_values().count(), N_VALUES / 2)
        self.assertEqual(field.get_values().fill_value, -1)
        self.assertEqual(field.fill_value, -1)
        self.assertEqual(field.get_values().dtype.name, 'int32')

    def test_field_ma_datetime64_auto_fillvalue(self):
        data = np.ma.array(
            [np.datetime64('2010-01-01')] * N_VALUES, dtype=np.datetime64)

        field = Field(
            data=data,
            dims=['time'],
            name='myvar')
        self.assertEqual(field.get_values().count(), N_VALUES)

        data = np.ma.masked_where(
            (np.arange(N_VALUES) >= N_VALUES / 2), data, copy=True)
        field.set_values(data)
        self.assertEqual(field.get_values().count(), N_VALUES / 2)
        self.assertEqual(field.get_values().dtype.name, 'datetime64[ns]')

    def test_field_da_float32_auto_fillvalue(self):
        data = np.ma.ones((N_VALUES,), dtype=np.float32)
        da = xr.DataArray(
            data=data,
            dims=['time'],
            name='myvar'
        )
        field = Field(
            data=da,
            dims=['time'],
            name='myvar')
        self.assertEqual(field.get_values().count(), N_VALUES)

        data = np.ma.masked_where(
            (np.arange(N_VALUES) >= N_VALUES/2), data, copy=True)
        field.set_values(data)
        print(field.get_values())
        self.assertEqual(field.get_values().count(), N_VALUES/2)
        self.assertEqual(field.get_values().dtype.name, 'float32')
        self.assertEqual(field.get_values().fill_value, np.float32(1e20))


    def test_field_da_float32(self):
        data = np.ma.ones((N_VALUES,), dtype=np.float32)
        da = xr.DataArray(
            data=data,
            dims=['time'],
            name='myvar'
        )
        field = Field(
            data=da,
            dims=['time'],
            fill_value=1e5,
            name='myvar')
        self.assertEqual(field.get_values().count(), N_VALUES)

        data = np.ma.masked_where(
            (np.arange(N_VALUES) >= N_VALUES/2), data, copy=True)
        field.set_values(data)
        print(field.get_values())
        self.assertEqual(field.get_values().count(), N_VALUES/2)
        self.assertEqual(field.get_values().fill_value, 1e5)
        self.assertEqual(field.fill_value, 1e5)
        self.assertEqual(field.get_values().dtype.name, 'float32')

    def test_field_da_int32_auto_fillvalue(self):
        data = np.ma.ones((N_VALUES,), dtype=np.int32)
        da = xr.DataArray(
            data=data,
            dims=['time'],
            name='myvar'
        )
        field = Field(
            data=da,
            dims=['time'],
            name='myvar')
        self.assertEqual(field.get_values().count(), N_VALUES)

        data = np.ma.masked_where(
            (np.arange(N_VALUES) >= N_VALUES/2), data, copy=True)
        field.set_values(data)
        print(field.get_values())
        self.assertEqual(field.get_values().count(), N_VALUES/2)
        self.assertEqual(field.get_values().dtype.name, 'int32')
