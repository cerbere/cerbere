
import numpy as np
import xarray as xr
from shapely.geometry import Polygon

from cerbere.feature.trajectory import Trajectory

from .test_feature import TestFeature


class TestTrajectoryFeature(TestFeature):
    """Test class for Trajectory feature"""

    def __init__(self, methodName='runTest'):
        super().__init__(methodName)

    def get_feature_class(self):
        return Trajectory

    def define_base_feature(self):
        # creates a test xarray object
        lon = xr.DataArray(data=np.arange(100, 180, 1), dims=['time'])
        lat = xr.DataArray(data=np.arange(0, 80, 1), dims=['time'])
        times = np.full((80,), np.datetime64('2018-01-01 00:00:00'))
        times += np.array([np.timedelta64(_) for _ in np.arange(0, 80)])
        time = xr.DataArray(times, dims='time')
        var = xr.DataArray(
            data=np.ones(shape=(80,)),
            dims=['time'],
            attrs={'myattr': 'test_attr_val'}
        )
        xrdataset = xr.Dataset(
            coords={'lat': lat, 'lon': lon, 'time': time},
            data_vars={'myvar': var},
            attrs={'gattr1': 'gattr1_val', 'gattr2': 'gattr2_val'}
        )
        return Trajectory(xrdataset)

    def get_feature_dimnames(self):
        return ('time',)

    def get_feature_dimsizes(self):
        return (80,)

    def test_clip(self):
        dst = self.define_base_feature()
        subset = dst.clip(
            Polygon([(120, 20), (140, 20), (140, 70), (120, 70)])
        )
        self.assertEqual(len(subset), 1)
        print(subset[0])

    def test_extract_subset(self):
        dst = self.define_base_feature()
        subset = dst.extract(
            index={'time': slice(10, 20)}, deep=True
        )
        self.assertTrue('myvar' in subset.fieldnames)
        self.assertSequenceEqual(
            subset.get_values('myvar').shape, (10,)
        )
        self.assertIsNot(
            subset.get_field('myvar').to_xarray(),
            dst.get_field('myvar').to_xarray()
        )

    def test_extract_subset_padding(self):
        dst = self.define_base_feature()
        subset = dst.extract(
            index={'time': slice(70, 90)}, padding=True, deep=True
        )
        self.assertTrue('myvar' in subset.fieldnames)
        self.assertSequenceEqual(
            subset.get_values('myvar').shape, (20,)
        )
        self.assertIsNot(
            subset.get_field('myvar').to_xarray(),
            dst.get_field('myvar').to_xarray()
        )
        self.assertEqual(subset.get_values('myvar').count(), 10)
        self.assertEqual(subset.get_values('myvar').size, 20)

    def test_extract_subset_padding_offlimit(self):
        dst = self.define_base_feature()
        subset = dst.extract(
            index={'time': slice(-10, 10)}, padding=True, deep=True
        )
        self.assertTrue('myvar' in subset.fieldnames)
        self.assertSequenceEqual(
            subset.get_values('myvar').shape, (20,)
        )
        self.assertIsNot(
            subset.get_field('myvar').to_xarray(),
            dst.get_field('myvar').to_xarray()
        )
        self.assertEqual(subset.get_values('myvar').count(), 10)
        self.assertEqual(subset.get_values('myvar').size, 20)
