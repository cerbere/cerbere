import unittest

import xarray as xr


TEST_FILE = '/home/jfpiolle/git/cerbere/tests/data' \
            '/FilledKNMIL2NCDataset' \
            '/hscat_20131016_084128_hy_2a__10289_o_250_ovw_l2.bufr.nc'


class TestOSISAFSCATL2Backend(unittest.TestCase):
    """Test class for OSISAF SCAT L2 backend"""

    def test_from_file(self):
        ds = xr.open_dataset(TEST_FILE, engine='osisafscatl2')
        assert 'row' in ds.dims
        assert 'cell' in ds.dims
        print(ds)

