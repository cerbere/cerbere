
import numpy as np
import xarray as xr

from cerbere.feature.section import Section

from .test_feature import TestFeature


class TestSectionFeature(TestFeature):
    """Test class for Section feature"""

    def __init__(self, methodName='runTest'):
        super().__init__(methodName)

    def get_feature_class(self):
        return Section

    def define_base_feature(self):
        # creates a test xarray object
        lon = xr.DataArray(data=np.arange(10, 11.5, 0.1), dims=['time'])
        lat = xr.DataArray(data=np.arange(9, 10.5, 0.1), dims=['time'])
        z = xr.DataArray(data=np.arange(0, 1000, 10), dims=['z'])
        times = np.full((15,), np.datetime64('2018-01-01 00:00:00'))
        times += np.array([np.timedelta64(_) for _ in np.arange(0, 15)])
        times = xr.DataArray(times, dims='time')
        var = xr.DataArray(
            data=np.ones(shape=(15, 100)),
            dims=['time', 'z'],
            attrs={'myattr': 'test_attr_val'}
        )
        xrdataset = xr.Dataset(
            coords={'lat': lat, 'lon': lon, 'time': times, 'depth': z},
            data_vars={'myvar': var},
            attrs={'gattr1': 'gattr1_val', 'gattr2': 'gattr2_val'}
        )
        return Section(xrdataset)

    def get_feature_dimnames(self):
        return 'time', 'z',

    def get_feature_dimsizes(self):
        return 15, 100,
