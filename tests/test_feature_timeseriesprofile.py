import numpy as np
import xarray as xr

from cerbere.feature.timeseriesprofile import TimeSeriesProfile

from .test_feature import TestFeature


class TestTimeSeriesProfileFeature(TestFeature):
    """Test class for TimeSeriesProfile feature"""

    def __init__(self, methodName='runTest'):
        super().__init__(methodName)

    def get_feature_class(self):
        return TimeSeriesProfile

    def define_base_feature(self):
        # creates a test xarray object
        lon = xr.DataArray(data=np.arange(10, 11.5, 0.1), dims=['profile'])
        lat = xr.DataArray(data=np.arange(9, 10.5, 0.1), dims=['profile'])
        z_data = np.repeat(np.arange(0, 1000, 10), 15).reshape(100, 15).T
        z = xr.DataArray(data=z_data, dims=['profile', 'z'])
        times = np.full((15,), np.datetime64('2018-01-01 00:00:00'))
        times += np.array([np.timedelta64(_) for _ in np.arange(0, 15)])
        times = xr.DataArray(times, dims='profile')
        var = xr.DataArray(
            data=np.ones(shape=(15, 100)),
            dims=['profile', 'z'],
            attrs={'myattr': 'test_attr_val'}
        )
        xrdataset = xr.Dataset(
            coords={'lat': lat, 'lon': lon, 'time': times, 'depth': z},
            data_vars={'myvar': var},
            attrs={'gattr1': 'gattr1_val', 'gattr2': 'gattr2_val'}
        )
        return TimeSeriesProfile(xrdataset)

    def get_feature_dimnames(self):
        return 'profile', 'z',

    def get_feature_dimsizes(self):
        return 15, 100,
