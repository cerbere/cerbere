import numpy as np
import xarray as xr

from cerbere.feature.omdcollection import OMDCollection
from cerbere.feature.profile import Profile

from cerbere.feature.pytest_cfeature import *

@pytest.fixture(scope='module')
def feature_class():
    return OMDCollection


@pytest.fixture(scope='module')
def feature_instance():
    # creates a test xarray object
    lon = xr.DataArray(data=[100., 100], dims=['profile'])
    lat = xr.DataArray(data=[50., 50], dims=['profile'])
    z = xr.DataArray(data=np.arange(0, 1000, 10), dims=['z'])
    time = xr.DataArray(
        [np.datetime64('2018-01-01 00:00:00'),
         np.datetime64('2018-01-01 00:00:00')],
        dims=['profile']
    )
    var = xr.DataArray(
        data=np.ones(shape=(2, 100,)),
        dims=['profile', 'z'],
        attrs={'myattr': 'test_attr_val'}
    )
    xrdataset = xr.Dataset(
        coords={'lat': lat, 'lon': lon, 'time': time, 'z': z},
        data_vars={'myvar': var},
        attrs={'gattr1': 'gattr1_val', 'gattr2': 'gattr2_val'}
    )

    return OMDCollection(xrdataset, feature_class=Profile)


@pytest.fixture(scope='module')
def feature_dimnames():
    return 'profile', 'z',


@pytest.fixture(scope='module')
def feature_dimsizes():
    return 2, 100,


@pytest.fixture(scope='module')
def content_class():
    return Profile
