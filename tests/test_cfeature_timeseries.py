"""
Example H.4. A single timeseries from CF convention

   dimensions:
      time = 100233 ;

   variables:
      float lon ;
          lon:standard_name = "longitude";
          lon:long_name = "station longitude";
          lon:units = "degrees_east";
      float lat ;
          lat:standard_name = "latitude";
          lat:long_name = "station latitude" ;
          lat:units = "degrees_north" ;
      float alt ;
          alt:long_name = "vertical distance above the surface" ;
          alt:standard_name = "height" ;
          alt:units = "m";
          alt:positive = "up";
          alt:axis = "Z";
      string station_name ;
          station_name:long_name = "station name" ;
          station_name:cf_role = "timeseries_id";

      double time(time) ;
          time:standard_name = "time";
          time:long_name = "time of measurement" ;
          time:units = "days since 1970-01-01 00:00:00" ;
      float humidity(time) ;
          humidity:standard_name = “specific_humidity” ;
          humidity:coordinates = "time lat lon alt station_name" ;
          humidity:_FillValue = -999.9f;
      float temp(time) ;
          temp:standard_name = “air_temperature” ;
          temp:units = "Celsius" ;
          temp:coordinates = "time lat lon alt station_name" ;
          temp:_FillValue = -999.9f;

   attributes:
          :featureType = "timeSeries";
"""

from cerbere.feature.ctimeseries import TimeSeries

from cerbere.feature.pytest_cfeature import *


@pytest.fixture(scope='module')
def feature_class():
    return TimeSeries


@pytest.fixture(scope='module')
def feature_instance():
    # creates a test xarray object
    lon = xr.DataArray(data=10., dims=[])
    lat = xr.DataArray(data=9., dims=[])
    times = np.full((15,), np.datetime64('2018-01-01 00:00:00', 'ns'))
    times += np.array([np.timedelta64(_) for _ in np.arange(0, 15)])
    times = xr.DataArray(times, dims='time')
    alt = xr.DataArray(
        data=10., dims=[], attrs=dict(
            standard_name='height',
            units="m",
            positive="up",
            axis="Z"
        ))
    station = xr.DataArray(data='station #1', dims=[], attrs=dict(
        long_name="station name",
        cf_role="timeseries_id"))
    var = xr.DataArray(
        data=np.ones(shape=(15,)),
        dims=['time'],
        attrs={'myattr': 'test_attr_val'}
    )
    xrdataset = xr.Dataset(
        coords={'lat': lat, 'lon': lon, 'time': times, 'alt': alt},
        data_vars={'my_var': var, 'station': station},
        attrs={'gattr1': 'gattr1_val', 'gattr2': 'gattr2_val'}
    )
    return TimeSeries(xrdataset)


@pytest.fixture(scope='module')
def feature_dimnames():
    return 'time',


@pytest.fixture(scope='module')
def feature_dimsizes():
    return 15,


@pytest.fixture(scope='module')
def content_class():
    return


def test_axis_coords(feature_instance):
    assert feature_instance.ds.cb.cf_axis_coords == {'T': 'time'}


def test_axis_dims(feature_instance):
    assert feature_instance.ds.cb.cf_axis_dims == {'T': 'time'}


def test_instance_dims(feature_instance):
    assert feature_instance.ds.cb.cf_instance_dims == []
