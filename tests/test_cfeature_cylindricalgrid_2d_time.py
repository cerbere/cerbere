from cerbere.feature.cgrid import Grid

from cerbere.feature.pytest_cfeature import *


@pytest.fixture(scope='module')
def feature_class():
    return Grid


@pytest.fixture(scope='module')
def feature_instance():
    # creates a test xarray object
    lon = xr.DataArray(data=np.arange(-180, 180, 1), dims=['lon'])
    lat = xr.DataArray(data=np.arange(-80, 80, 1), dims=['lat'])
    time = xr.DataArray(
        data=np.full((160, 360), np.datetime64("2018-01-01", 'ns')),
        dims=['lat', 'lon'])
    var = xr.DataArray(data=np.ones(shape=(160, 360)), dims=['lat', 'lon'])
    var.attrs['my_var'] = 'test_attr_val'
    xrdataset = xr.Dataset(
        coords={'lat': lat, 'lon': lon, 'time': time},
        data_vars={'my_var': var},
        attrs={'gattr1': 'gattr1_val', 'gattr2': 'gattr2_val'}
    )

    return Grid(xrdataset)


@pytest.fixture(scope='module')
def feature_dimnames():
    return 'lat', 'lon', 'time',


@pytest.fixture(scope='module')
def feature_dimsizes():
    return 160, 360, 1,


@pytest.fixture(scope='module')
def content_class():
    return
