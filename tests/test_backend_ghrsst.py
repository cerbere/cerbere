from cerbere.backend.pytest_backend import *

from cerbere.feature.cswath import Swath

TEST_FILE = '/home/jfpiolle/git/naiad-index/tests/data/avhrr_metop' \
            '/20200330235803-OSISAF-L2P_GHRSST-SSTsubskin-AVHRR_SST_METOP_B' \
            '-sstmgr_metop01_20200330_235803-v02.0-fv01.0.nc'

@pytest.fixture(scope='module')
def test_file():
    return TEST_FILE


@pytest.fixture(scope='module')
def backend():
    return 'ghrsst'


@pytest.fixture(scope='module')
def feature_class():
    return Swath


def test_guess_backend(input_file):
    """Open file and create dataset object"""
    dst = xr.open_dataset(input_file).cb.cfdataset
    assert 'time' in dst
    assert 'id' in dst.attrs
