import numpy as np
import xarray as xr

from cerbere.feature.imdcollection import IMDCollection
from cerbere.feature.point import Point

from .test_feature import TestFeature


class TestIMDCollectionProfileFeature(TestFeature):
    """Test class for incomplete multidimensional collection of profile
    features"""

    def __init__(self, methodName='runTest'):
        super().__init__(methodName)

    def get_feature_class(self):
        return IMDCollection

    def get_feature(self, *args, **kwargs):
        return self.get_feature_class()(
            *args, feature_class=Point, **kwargs
        )

    def define_base_feature(self):
        # creates a test xarray object
        lon = xr.DataArray(data=[100., 100], dims=['obs'])
        lat = xr.DataArray(data=[50., 50], dims=['obs'])

        time = xr.DataArray(
            [np.datetime64('2018-01-01 00:00:00'),
             np.datetime64('2018-01-01 00:00:00')],
            dims=['obs']
        )
        var = xr.DataArray(
            data=np.ones(shape=(2,)),
            dims=['obs',],
            attrs={'myattr': 'test_attr_val'}
        )
        xrdataset = xr.Dataset(
            coords={'lat': lat, 'lon': lon, 'time': time},
            data_vars={'myvar': var},
            attrs={'gattr1': 'gattr1_val', 'gattr2': 'gattr2_val'}
        )

        return IMDCollection(xrdataset, feature_class=Point)

    def get_feature_dimnames(self):
        return 'obs',

    def get_feature_dimsizes(self):
        return 2,
