import numpy as np
import xarray as xr

from cerbere.feature.profile import Profile

from .test_feature import TestFeature


class TestProfileFeature(TestFeature):
    """Test class for Profile feature"""

    def __init__(self, methodName='runTest'):
        super().__init__(methodName)

    def get_feature_class(self):
        return Profile

    def define_base_feature(self):
        # creates a test xarray object
        lon = xr.DataArray(data=100., dims=[])
        lat = xr.DataArray(data=50., dims=[])
        z = xr.DataArray(data=np.arange(0, 1000, 10), dims=['z'])
        time = xr.DataArray(np.datetime64('2018-01-01 00:00:00'), dims=())
        profile = xr.DataArray(
            data=np.int32(1),
            dims=(),
            attrs={'cf_role': "profile_id"}
        )

        var = xr.DataArray(
            data=np.ones(shape=(100,)),
            dims=['z'],
            attrs={'myattr': 'test_attr_val'}
        )
        xrdataset = xr.Dataset(
            coords={'lat': lat, 'lon': lon, 'time': time, 'z': z,
                    'profile': profile},
            data_vars={'myvar': var},
            attrs={'gattr1': 'gattr1_val', 'gattr2': 'gattr2_val'}
        )
        return Profile(xrdataset)

    def get_feature_dimnames(self):
        return ('z',)

    def get_feature_dimsizes(self):
        return (100,)
