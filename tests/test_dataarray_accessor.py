'''
Created on Aug 15, 2018

@author: jfpiolle
'''
from datetime import datetime
import unittest

import dask
import numpy as np
import xarray as xr

import cerbere.accessor.cdataarray


TEST_FILE = '/home/jfpiolle/git/naiad-index/tests/data/avhrr_metop' \
            '/20200330235803-OSISAF-L2P_GHRSST-SSTsubskin-AVHRR_SST_METOP_B' \
            '-sstmgr_metop01_20200330_235803-v02.0-fv01.0.nc'

DEF_FILL_VALUE = {
    np.int32: np.int32(-2147483648),
    np.float32: np.float32(1e20),
    np.float64: np.float64(1e20),
}
AUTO_FILL_VALUE = {
    np.int32: np.int32(-2147483648),
    np.float32: np.nan,
    np.float64: np.nan,
}


class TestField(unittest.TestCase):
    """Test class for AbstractFeature"""

    @classmethod
    def create_field_from_numpy(cls):
        data = np.ma.zeros((100, 200))
        return xr.DataArray(data, name='test', dims=('x', 'y'))

    def test_from_file(self):
        ds = xr.open_dataset(TEST_FILE, engine='netcdf4')

        toto = np.ma.MaskedArray([0,0,0])
        print("TOTO", toto.count())
        toto.set_fill_value(0)
        print("TOTO2", toto.count())

        # check science dtype of a few variables
        self.assertTrue(np.issubdtype(
            ds.aerosol_dynamic_indicator.cerbere.science_dtype,
            np.floating))
        self.assertTrue(np.issubdtype(
            ds.sea_surface_temperature.cerbere.science_dtype,
            np.floating))
        self.assertTrue(np.issubdtype(
            ds.l2p_flags.cerbere.science_dtype,
            np.int16))
        self.assertTrue(np.issubdtype(
            ds.time.cerbere.science_dtype,
            np.datetime64))
        self.assertTrue(np.issubdtype(
            ds.sst_dtime.cerbere.science_dtype,
            np.float64))

        # test mask
        self.assertEqual(
            ds.sea_surface_temperature.cerbere.mask.sum(),
            ds.sea_surface_temperature.size - ds.sea_surface_temperature.count()
        )
        self.assertEqual(
            ds.l2p_flags.cerbere.mask.sum(),
            ds.l2p_flags.size - ds.l2p_flags.count()
        )
        self.assertEqual(ds.lon.cerbere.mask, np.ma.nomask)
        self.assertIsInstance(ds.lon.cerbere.mask, type(np.ma.nomask))
        self.assertIsInstance(ds.sst_dtime.cerbere.mask, type(np.ma.nomask))


        # verify xarray does not copy the cerbere attrs in a subset, except
        # the xarray attrs and encoding
        subds = ds.sea_surface_temperature.isel(
            nj=slice(10, 20), ni=slice(10, 20))
        self.assertEqual(subds.cerbere.standard_name,
                         'sea_surface_subskin_temperature')

        # verify cerbere copies both xarray and cerbere accessor attrs
        subs2 = ds.sea_surface_temperature.cerbere.isel(
            nj=slice(10, 20), ni=slice(10, 20), drop=True)
        self.assertEqual(subs2.cerbere.standard_name,
                         'sea_surface_subskin_temperature')

        subs2 = ds.sea_surface_temperature.cerbere.isel(
            nj=slice(10, 20), ni=slice(10, 20))
        self.assertEqual(subs2.cerbere.standard_name,
                         'sea_surface_subskin_temperature')

        # test mask subset
        self.assertEqual(subs2.cerbere.mask.size, 100)
        self.assertEqual(
            subs2.cerbere.mask.sum(),
            subs2.size -
            subs2.count()
        )
        self.assertEqual(subs2.lon.cerbere.mask, np.ma.nomask)
        self.assertIsInstance(subs2.lon.cerbere.mask, type(np.ma.nomask))

        # test subsetting
        print("subsetting")
        subout = ds.sea_surface_temperature.isel(nj=slice(-10, 10),
                                                 ni=slice(-10, 10))
        assert tuple(subout.sizes.values()) == (1, 0, 0)

        csubout = ds.sea_surface_temperature.cerbere.isel(
            nj=slice(-10, 10), ni=slice(-10, 10), padding=True)
        # test mask subset
        self.assertEqual(csubout.cerbere.mask.size, 400)
        self.assertEqual(
            csubout.cerbere.mask.sum(),
            csubout.size - csubout.to_masked_array().count()
        )
        self.assertEqual(
            csubout.cerbere.mask.sum(),
            csubout.size - csubout.count()
        )
        self.assertEqual(csubout.lon.cerbere.mask.sum(), 300)

        # transform subset to MaskedArray
        csubout = csubout.cerbere.to_masked_array()
        self.assertEqual(csubout.mask.size, 400)
        self.assertEqual(
            csubout.mask.sum(),
            csubout.size - csubout.count()
        )
        self.assertEqual(
            csubout.mask.sum(),
            csubout.size - csubout.count()
        )
        self.assertTrue(np.ma.is_masked(csubout[0, 0, 0]))

        # test xarray type transformation
        print("DTYPE TRANSFORMATION")
        test_arr = np.ma.MaskedArray([1, 1, 1, 2])
        print(xr.DataArray(test_arr))
        test_arr = np.ma.masked_equal(test_arr, 2)
        print(xr.DataArray(test_arr), xr.DataArray(test_arr).dtype,
              xr.DataArray(test_arr).encoding)

        csubout = ds.l2p_flags.cerbere.isel(
            nj=slice(-10, 10), ni=slice(-10, 10), padding=True)
        # test mask subset
        self.assertEqual(csubout.cerbere.mask.size, 400)
        self.assertEqual(
            csubout.cerbere.mask.sum(),
            csubout.size - csubout.to_masked_array().count()
        )
        self.assertEqual(
            csubout.cerbere.mask.sum(),
            csubout.size - csubout.count()
        )
        self.assertEqual(csubout.lon.cerbere.mask.sum(), 300)

        # transform subset to MaskedArray
        csubout = csubout.cerbere.to_masked_array()
        self.assertEqual(csubout.dtype, np.dtype(np.int16))
        self.assertEqual(csubout.mask.size, 400)
        self.assertEqual(
            csubout.mask.sum(),
            csubout.size - csubout.count()
        )
        self.assertEqual(
            csubout.mask.sum(),
            csubout.size - csubout.count()
        )
        self.assertTrue(np.ma.is_masked(csubout[0, 0, 0]))

    def test_create_int_field_from_numpy_no_dims(self):
        data = np.ma.zeros((100, 200), dtype=np.int32)
        field = xr.DataArray(data=data, name='test')

        self.assertEqual(field.name, 'test')
        self.assertEqual(field.dtype, np.dtype(np.int32))
        self.assertEqual(field.cerbere.science_dtype, np.dtype(np.int32))
        self.assertEqual(field.cerbere.fill_value, None)

        field.cerbere.fill_value = np.int32(0)

        self.assertEqual(np.dtype(type(field.cerbere.fill_value)).kind,
                         field.dtype.kind)
        self.assertEqual(field.dims, ('dim_0', 'dim_1'))
        self.assertEqual(field.dtype,
                         field.cerbere.to_masked_array().dtype)


    def test_int_masked_from_ndarray(self):
        # ndarray
        data = np.zeros((100, 200), dtype=np.int32)
        field = xr.DataArray(data=data, name='test')

        # transformed by xarray into -2147483648 !?
        field.loc[0:20, 0:20] = np.nan

        field.cerbere.fill_value = np.int32(-2147483648)
        self.assertEqual(
            field.cerbere.to_masked_array().count(), data.size - 400)
        self.assertEqual(
            field.cerbere.to_masked_array().dtype, np.int32)
        # no way the -2147483648 can be masked with classic xarray
        self.assertEqual(
            field.to_masked_array(copy=False).count(), data.size)
        self.assertEqual(
            field.to_masked_array(copy=True).count(), data.size)
        self.assertEqual(
            field.to_masked_array().dtype, np.int32)

        # the right way to create a cerbere DataArray
        data = np.zeros((100, 200), dtype=np.int32)
        field = cerbere.new_array(data)
        field.cerbere.loc[0:20, 0:20] = np.nan

        # using cerbere loc, fill_value (automatically created by xarray) is
        # properly guessed and set
        self.assertEqual(field.cerbere.fill_value, -2147483648)
        self.assertEqual(field.cerbere.science_dtype, np.int32)
        self.assertEqual(
            field.cerbere.to_masked_array().count(), data.size - 400)
        self.assertEqual(
            field.cerbere.to_masked_array().dtype, np.int32)

    def check_encoding(self, orig_arr, darr, exp_dtype, exp_science_dtype,
                       exp_fill_value, exp_count, keep_dtype):

        self.assertEqual(darr.dtype, exp_dtype)
        self.assertEqual(darr.cerbere.science_dtype, exp_science_dtype)

        # cerbere to_masked_array returns the proper science dtype, fill value
        # and expected number of masked values
        cmdata = darr.cerbere.to_masked_array()
        self.assertEqual(cmdata.dtype, exp_science_dtype)
        self.assertEqual(cmdata.fill_value, exp_fill_value)
        self.assertEqual(cmdata.count(), exp_count)

        xrdata = darr.cerbere.cfarray
        if not keep_dtype:
            self.assertEqual(
                xrdata.to_masked_array(copy=False).count(), exp_count)
            self.assertEqual(
                xrdata.to_masked_array(copy=True).count(), exp_count)
        else:
            self.assertEqual(
                xrdata.to_masked_array(copy=False).count(), darr.size)
            self.assertEqual(
                xrdata.to_masked_array(copy=True).count(), darr.size)

    def test_broadcasting(self):
        # case 1 : 2D broadcast of a coordinate
        dst = cerbere.new_array(
            data=np.ones(shape=(160, 360)),
            dims=('lat', 'lon',),
            coords={
                'lat': (['lat'], np.arange(-80, 80, 1)),
                'lon': (['lon'], np.arange(-180, 180, 1))
            },
            attrs={'gattr1': 'gattr_val'}
        )
        dst.coords['lon'].attrs['standard_name'] = 'longitude'
        new_lon = dst.coords['lon'].cerbere.isel(
            broadcast_coords=[dst.lat, dst.lon]
        )
        self.assertEqual(len(new_lon.dims), 2)
        self.assertSequenceEqual(
            list(new_lon[5, :].values), list(dst.coords['lon'][:].values))

        # case 2: broadcast of a 1D variable to a new coordinate
        lat = xr.DataArray(
            dims=['lat'],
            data=np.arange(-80, 80, 1),
            name='lat')
        lon = xr.DataArray(
            dims='lon',
            data=np.arange(-180, 180, 1),
            name='lon')
        dst = cerbere.new_array(
            data=np.arange(160),
            dims=('lat'),
            coords={
                'lat': (['lat'], np.arange(-80, 80, 1)),
            },
            attrs={'gattr1': 'gattr_val'}
        )
        new_dst = dst.cerbere.isel(
            broadcast_coords=[lat, lon]
        )
        self.assertEqual(len(new_dst.dims), 2)
        self.assertSequenceEqual(
            list(new_dst[:, 5].values), list(dst[:].values))

       # case 3: broadcast of a 2D variable to a new coordinate
        lat = xr.DataArray(
            dims=['lat'],
            data=np.arange(-80, 80, 1),
            name='lat')
        lon = xr.DataArray(
            dims='lon',
            data=np.arange(-180, 180, 1),
            name='lon')
        time = xr.DataArray(
            dims='time',
            data=[datetime(2022, 1, 1)],
            name='time')
        dst = cerbere.new_array(
            data=np.reshape(np.arange(160), (1, 160)),
            dims=('time', 'lat'),
            coords={
                'time': time,
                'lat': lat,
            },
            attrs={'gattr1': 'gattr_val'}
        )
        new_dst = dst.cerbere.isel(
            broadcast_coords=[time, lon, lat]
        )
        self.assertEqual(len(new_dst.dims), 3)
        self.assertSequenceEqual(
            list(new_dst[0, 5, :].values), list(dst[0, :].values))

        # same but omitting existing coordinates (time, lat) in array
        # add the missing dimensions at the end
        new_dst = dst.cerbere.isel(
            broadcast_coords=[lon]
        )
        self.assertEqual(len(new_dst.dims), 3)
        self.assertSequenceEqual(
            list(new_dst[5, 0, :].values), list(dst[0, :].values))

    def test_init_masked_from_maskedarray(self):
        for dtype in [np.int32, np.uint32, np.float32]:

            fdtype = np.float64
            if np.issubdtype(dtype, np.floating):
                fdtype = dtype

            values = np.zeros(shape=(360, 160), dtype=dtype)
            values[0:20, 0:20] = 1
            values = np.ma.masked_equal(values, 1, copy=False)

            # check by forbidding dtype transformation by warray
            cerbere.set_options(keep_dtype=True)
            new_field = cerbere.new_array(
                    data=values,
                    dims=('lon', 'lat',),
                    science_dtype=dtype,
                    name='newvar')
            # cerbere to_masked_array returns the proper science dtype, fill value
            # and expected number of masked values
            self.check_encoding(
                values, new_field, dtype, dtype, 1, 360*160 - 400, True)

            # same but setting another fill value
            new_field = cerbere.new_array(
                    data=values,
                    dims=('lon', 'lat',),
                    science_dtype=dtype,
                    fill_value=np.dtype(dtype).type(99999),
                    name='newvar')
            self.check_encoding(
                values, new_field, dtype, dtype, 99999, 360*160 - 400, True)

            # same but setting the fill value after the array creation
            new_field = cerbere.new_array(
                    data=values,
                    dims=('lon', 'lat',),
                    science_dtype=dtype,
                    name='newvar')
            new_field.cerbere.fill_value = dtype(99999)
            self.check_encoding(
                values, new_field, dtype, dtype, 99999, 360*160 - 400, True)

            # don't prevent dtype transformation by xarray (default)
            # xarray will transform the MaskedArray to float dtype
            cerbere.set_options(keep_dtype=False)
            new_field = cerbere.new_array(
                    data=values,
                    dims=('lon', 'lat',),
                    science_dtype=dtype,
                    name='newvar')
            # cerbere to_masked_array returns the proper science dtype, fill value
            # and expected number of masked values
            self.check_encoding(
                values, new_field, fdtype, dtype, 1, 360*160 - 400, False)

            # same but setting another fill value
            new_field = cerbere.new_array(
                    data=values,
                    dims=('lon', 'lat',),
                    science_dtype=dtype,
                    fill_value=np.dtype(dtype).type(99999),
                    name='newvar')
            self.check_encoding(
                values, new_field, fdtype, dtype, 99999, 360*160 - 400,
                False)

            # test on a extracted subset
            # check by forbidding dtype transformation by xarray
            cerbere.set_options(keep_dtype=True)
            new_field = cerbere.new_array(
                    data=values,
                    dims=('lon', 'lat',),
                    science_dtype=dtype,
                    name='newvar')
            subset_field = new_field.cerbere.isel(
                {'lat': slice(0, 30),
                 'lon': slice(0, 30)})

            # cerbere to_masked_array returns the proper science dtype, fill value
            # and expected number of masked values
            self.check_encoding(
                values, subset_field, dtype, dtype, 1, 900 - 400, True)

            # test on a extracted subset
            # check by allowing dtype transformation by xarray
            cerbere.set_options(keep_dtype=False)
            new_field = cerbere.new_array(
                    data=values,
                    dims=('lon', 'lat',),
                    science_dtype=dtype,
                    name='newvar')
            subset_field = new_field.cerbere.isel(
                {'lat': slice(0, 30),
                 'lon': slice(0, 30)})

            # cerbere to_masked_array returns the proper science dtype, fill value
            # and expected number of masked values
            self.check_encoding(
                values, subset_field, fdtype, dtype, 1, 900 - 400, False)

    def test_int_masked_from_maskedarray(self):
        # masked array
        data = np.ma.zeros((100, 200), dtype=np.int32)
        data[0:20, 0:20] = 1
        data = np.ma.masked_equal(data, 1)
        field = xr.DataArray(data=data, name='test')
        # xarray from MaskedArray will transform dtype to float and use
        # np.nan as _FillValue
        self.assertEqual(field.dtype, np.float64)
        self.assertEqual(field.cerbere.science_dtype, np.float64)
        self.assertEqual(
            field.cerbere.to_masked_array().count(), data.size - 400)
        self.assertEqual(
            field.to_masked_array(copy=False).count(), data.size - 400)
        self.assertEqual(
            field.to_masked_array(copy=True).count(), data.size - 400)

        # set the properties for converting back to original values
        field.cerbere.science_dtype = np.int32
        field.cerbere.fill_value = np.int32(1)

        self.assertEqual(field.dtype, np.float64)
        self.assertEqual(field.cerbere.science_dtype, np.int32)
        self.assertEqual(field.cerbere.fill_value, np.int32(1))
        self.assertEqual(
            field.cerbere.to_masked_array().count(), data.size - 400)
        self.assertEqual(
            field.to_masked_array(copy=False).count(), data.size - 400)
        self.assertEqual(
            field.to_masked_array(copy=True).count(), data.size - 400)

        # the right way to create a cerbere DataArray
        data = np.ma.zeros((100, 200), dtype=np.int32)
        data[0:20, 0:20] = 1
        data = np.ma.masked_equal(data, 1)
        field = cerbere.new_array(data)

        # using cerbere loc, fill_value (automatically created by xarray) is
        # properly guessed and set
        self.assertEqual(field.cerbere.fill_value, 1)
        self.assertEqual(field.cerbere.science_dtype, np.int32)
        self.assertEqual(
            field.cerbere.to_masked_array().count(), data.size - 400)
        self.assertEqual(
            field.cerbere.to_masked_array().dtype, np.int32)

    def test_int_masked_from_dataarray(self):
        # DataArray
        data = np.zeros((100, 200), dtype=np.int32)
        field = xr.DataArray(data=data, name='test')
        field = xr.zeros_like(field, dtype=np.int32)
        field[0:20, 0:20] = np.nan

        # NaN transformed by xarray into -2147483648 !?
        self.assertEqual(data.dtype, np.int32)
        self.assertEqual(field.dtype, np.int32)
        self.assertEqual(field.cerbere.science_dtype, np.int32)
        # inconsistent but can't know at this stage -2147483648 is a fill_value
        self.assertEqual(
            field.cerbere.to_masked_array().count(), data.size)
        self.assertEqual(
            field.to_masked_array(copy=False).count(), data.size)
        self.assertEqual(
            field.to_masked_array(copy=True).count(), data.size)

        # set the properties for converting back to original values
        field.cerbere.fill_value = np.int32(-2147483648)
        field.cerbere.science_dtype = np.int32

        self.assertEqual(field.dtype, np.int32)
        self.assertEqual(field.cerbere.science_dtype, np.int32)
        # using cerbere.to_masked_array provides more consistent result
        self.assertEqual(
            field.cerbere.to_masked_array().count(), data.size - 400)
        # inconsistent but xarray can't know at this stage -2147483648 is a
        # fill_value
        self.assertEqual(
            field.to_masked_array(copy=False).count(), data.size)
        self.assertEqual(
            field.to_masked_array(copy=True).count(), data.size)

        # the right way to create a cerbere DataArray
        data = np.zeros((100, 200), dtype=np.int32)
        field = xr.DataArray(data=data, name='test')
        field = cerbere.new_array(field)
        field.cerbere.loc[0:20, 0:20] = np.nan

        # using cerbere loc, fill_value (automatically created by xarray) is
        # properly guessed and set
        self.assertEqual(field.cerbere.fill_value, -2147483648)
        self.assertEqual(field.cerbere.science_dtype, np.int32)
        self.assertEqual(
            field.cerbere.to_masked_array().count(), data.size - 400)
        self.assertEqual(
            field.cerbere.to_masked_array().dtype, np.int32)

    def test_masked_from_daskarray(self):
        """Test creating a new cerbere array from a Dask array, with masked
        values.

        Verifies also the behaviour of Dask array is as previously known
        """
        for dtype in [np.int32, np.float32, np.float64]:
            print("dtype tested: ", dtype)
            data = np.zeros((100, 200), dtype=dtype)
            data = dask.array.zeros_like(data, dtype=dtype)
            data[0:20, 0:20] = np.nan
            field = xr.DataArray(data=data, name='test')

            self.assertEqual(data.dtype, dtype)
            self.assertEqual(field.dtype, dtype)
            self.assertEqual(field.cerbere.science_dtype, dtype)
            # Not the result we expect (fill values not detected as such)
            # but we can't know at this stage the replaced values are
            # fill_value (except when replaced with NaNs)
            if np.issubdtype(dtype, np.integer):
                expected_size = data.size
            else:
                expected_size = data.size - 400
            self.assertEqual(
                field.cerbere.to_masked_array().count(), expected_size)
            self.assertEqual(
                field.to_masked_array(copy=False).count(), expected_size)
            self.assertEqual(
                field.to_masked_array(copy=True).count(), expected_size)

            field.compute()

            # Dask will transform nan into another value, depending on the dtype
            if np.isnan(AUTO_FILL_VALUE[dtype]):
                self.assertTrue(np.isnan(data[0, 0].compute()))
            else:
                self.assertEqual(data[0, 0].compute(), AUTO_FILL_VALUE[dtype])

            self.assertEqual(data.dtype, dtype)
            self.assertEqual(field.dtype, dtype)
            self.assertEqual(field.cerbere.science_dtype, dtype)
            # Not the result we expect (fill values not detected as such)
            # but we can't know at this stage the replaced values are
            # fill_value (except when replaced with NaNs)
            if np.issubdtype(dtype, np.integer):
                expected_size = data.size
            else:
                expected_size = data.size - 400
            self.assertEqual(
                field.cerbere.to_masked_array().count(), expected_size)
            self.assertEqual(
                field.to_masked_array(copy=False).count(), expected_size)
            self.assertEqual(
                field.to_masked_array(copy=True).count(), expected_size)

            # set the properties for converting back to original values
            field.cerbere.fill_value = DEF_FILL_VALUE[dtype]
            field.cerbere.science_dtype = dtype

            self.assertEqual(field.dtype, dtype)
            self.assertEqual(field.cerbere.science_dtype, dtype)
            # using cerbere.to_masked_array provides the expected result as
            # the fill_value is now known
            self.assertEqual(
                field.cerbere.to_masked_array().count(), data.size - 400)
            # Not the result we expect (fill values not detected as such)
            # but we can't know at this stage the replaced values are
            # fill_value (except when replaced with NaNs)
            if np.issubdtype(dtype, np.integer):
                expected_size = data.size
            else:
                expected_size = data.size - 400
            self.assertEqual(
                field.to_masked_array(copy=False).count(), expected_size)
            self.assertEqual(
                field.to_masked_array(copy=True).count(), expected_size)

            # the right way to create a cerbere DataArray
            data = np.zeros((100, 200), dtype=dtype)
            data = dask.array.zeros_like(data, dtype=dtype)
            field = cerbere.new_array(data)
            field.cerbere.loc[0:20, 0:20] = np.nan
            print(field.cerbere.cfarray.encoding)

            # using cerbere loc, fill_value (automatically created by xarray) is
            # properly guessed and set
            print(type(field.cerbere.fill_value), type(DEF_FILL_VALUE[dtype]))
            self.assertEqual(field.cerbere.fill_value, DEF_FILL_VALUE[dtype])
            self.assertEqual(field.cerbere.science_dtype, dtype)
            self.assertEqual(
                field.cerbere.to_masked_array().count(), data.size - 400)
            self.assertEqual(
                field.cerbere.to_masked_array().dtype, dtype)

    def test_create_int_field_from_numpy_no_missing_value(self):
        """integer array with no fill value"""
        data = np.ma.zeros((100, 200), dtype=np.int32)
        field = xr.DataArray(data=data, name='test')

        self.assertEqual(field.dtype, np.dtype(np.int32))
        self.assertEqual(field.cerbere.science_dtype, np.dtype(np.int32))
        self.assertEqual(field.cerbere.fill_value, None)

        # same from xarray subset
        subset = field[:10, :10]

        self.assertEqual(subset.dtype, np.dtype(np.int32))
        self.assertEqual(subset.cerbere.science_dtype, np.dtype(np.int32))
        self.assertEqual(subset.cerbere.fill_value, None)

        # same from subset with cerbere isel
        subset = field.cerbere.isel(dim_0=slice(0, 10), dim_1=slice(0, 10))

        self.assertEqual(subset.dtype, np.dtype(np.int32))
        self.assertEqual(subset.cerbere.science_dtype, np.dtype(np.int32))
        self.assertEqual(subset.cerbere.fill_value, None)

    def test_masked_int_dtype_mismatch(self):
        """Test creating a DataArray wit mismatched dtype and fillvalue"""
        values = np.zeros(shape=(360, 160), dtype=np.int32)
        values[10:20, 10:20] = 1
        values = np.ma.masked_equal(values, 1, copy=False)
        field = xr.DataArray(
            data=values,
            name='newvar',
            dims=('lon', 'lat',))

        with self.assertRaises(TypeError):
            field.cerbere.fill_value = 0,

    def test_create_float_field_from_numpy_no_fillvalue(self):
        # float array, no fill value
        data = np.ma.zeros((100, 200), dtype=np.float32)
        field = xr.DataArray(data=data, name='test')

        self.assertEqual(field.name, 'test')
        self.assertEqual(field.dims, ('dim_0', 'dim_1'))
        self.assertEqual(field.dtype, np.dtype(np.float32))
        self.assertEqual(field.cerbere.science_dtype, np.dtype(np.float32))
        self.assertEqual(field.cerbere.fill_value, None)

        field.cerbere.fill_value = np.float32(0)

        self.assertEqual(np.dtype(type(field.cerbere.fill_value)).kind,
                         field.dtype.kind)

        # verify properties after subsetting
        subset = field.isel(dim_0=slice(0, 10))
        self.assertEqual(field.dtype, subset.dtype)
        self.assertEqual(subset.name, 'test')
        self.assertEqual(subset.dtype, np.dtype(np.float32))
        self.assertEqual(subset.cerbere.science_dtype, np.dtype(np.float32))
        self.assertEqual(np.dtype(type(subset.cerbere.fill_value)).kind,
                         subset.dtype.kind)
        self.assertEqual(subset.dims, ('dim_0', 'dim_1'))

    def test_create_masked_float_field_from_numpy_no_dims(self):
        data = np.ma.zeros((100, 200), dtype=np.float32)
        data[0:20, 0:20] = 1
        data = np.ma.masked_equal(data, 0)
        self.assertEqual(data.count(), 400)

        field = xr.DataArray(data=data, name='test')

        self.assertEqual(field.name, 'test')
        self.assertEqual(field.dtype, np.dtype(np.float32))
        print(field.to_masked_array())
        print(field.to_masked_array().data)
        print("FILL ", field.to_masked_array().fill_value)
        print(field.cerbere.fill_value,
              np.dtype(type(field.cerbere.fill_value)),
              np.dtype(field.dtype))
        self.assertEqual(
            np.dtype(type(field.cerbere.fill_value)), np.dtype(field.dtype))
        self.assertEqual(field.dims, ('dim_0', 'dim_1'))
        self.assertEqual(field.dtype, field.isel().dtype)
        self.assertEqual(field.dtype, field.cerbere.isel().dtype)
        self.assertEqual(field.count(), 400)

    def test_create_field_from_numpy_masked_array(self):
        # creates from a masked array, verify masked values are propagated
        data = np.ma.zeros((100, 200))
        field = xr.DataArray(data=data, name='test')

        self.assertIsInstance(field, xr.DataArray)
        self.assertEqual(field.cerbere.science_dtype, np.dtype(np.float64))
        self.assertEqual(field.cerbere.fill_value, None)
        self.assertEqual(field.count(), 20000)
        self.assertEqual(field.cerbere.mask, False)

        # same with masked value
        data[:10, :10] = 1
        data = np.ma.masked_equal(data, 1)
        print("FILL ", data.fill_value)

        field = xr.DataArray(data=data, name='test')
        print(field)

        self.assertIsInstance(field, xr.DataArray)
        self.assertEqual(field.cerbere.science_dtype, np.dtype(np.float64))
        # it should have been one oif xarray was properly storing the
        # original fill value of the masked array in some encoding attr
        self.assertEqual(field.cerbere.fill_value, 1e+20)
        self.assertEqual(field.count(), 19900)
        self.assertEqual(field.cerbere.mask.sum(), 100)

        print(field.cerbere.fill_value, data.dtype)

    def test_create_field(self):
        field = self.create_field_from_numpy()
        self.assertIsInstance(field, xr.DataArray)
        print(field)

    def test_get_values(self):
        field = self.create_field_from_numpy()
        self.assertIsInstance(field, xr.DataArray)
        self.assertEqual(field.cerbere.science_dtype, np.dtype(np.float64))
        self.assertEqual(field.dtype, np.dtype(np.float64))

        values = field.isel()
        self.assertIsInstance(values, xr.DataArray)
        self.assertEqual(values.cerbere.science_dtype, np.dtype(np.float64))
        self.assertEqual(values.dtype, np.dtype(np.float64))

        values = field.cerbere.isel()
        self.assertIsInstance(values, xr.DataArray)
        self.assertEqual(values.cerbere.science_dtype, np.dtype(np.float64))
        self.assertEqual(values.dtype, np.dtype(np.float64))

    def test_inline_value_modification(self):
        data = np.ma.zeros((100, 200), dtype=np.float32)
        data[10:20, 10:20] = 1
        data = np.ma.masked_equal(data, 0)
        self.assertEqual(data.count(), 100)

        field = xr.DataArray(data=data, name='test')

        values_original = field.isel()

        # modify the returned array and verify the field was modified too
        values_original[10:15, 10:15] = 3

        self.assertTrue(field[10:15, 10:15].equals(
            values_original[10:15, 10:15]))

        np.testing.assert_equal(
            field[10:15, 10:15].to_masked_array(),
            values_original[10:15, 10:15].to_masked_array())
        np.testing.assert_equal(
            field[10:15, 10:15].cerbere.to_masked_array(),
            values_original[10:15, 10:15].cerbere.to_masked_array())

    def test_set_values(self):
        field = self.create_field_from_numpy()

        # modify a reference (data were not actually copied)
        values = field.isel()
        values[:] = 2

        values = field.isel()
        self.assertTrue(values.min() == 2)
        self.assertTrue(values.max() == 2)

        # @TODO with padding

    def test_set_values_func(self):
        field = self.create_field_from_numpy()
        values = np.full((100, 200,), 2)
        field[:, :] = values

        self.assertTrue(values.min() == 2)
        self.assertTrue(values.max() == 2)

    def test_get_values_subset(self):
        field = self.create_field_from_numpy()

        values = field.isel({'x': slice(10, 20), 'y': slice(0, 5)})
        self.assertIsInstance(values, xr.DataArray)
        self.assertEqual(values.shape, (10, 5))

        # cerbere isel
        values = field.cerbere.isel({'x': slice(10, 20), 'y': slice(0, 5)})
        self.assertIsInstance(values, xr.DataArray)
        self.assertEqual(values.shape, (10, 5))

        # cerbere isel as MaskedArray
        values = field.cerbere.isel(
            {'x': slice(10, 20), 'y': slice(0, 5)},
            as_masked_array=True
        )
        self.assertIsInstance(values, np.ma.MaskedArray)
        self.assertEqual(values.shape, (10, 5))

    def test_set_values_subset(self):
        field = self.create_field_from_numpy()
        subset = np.full((10, 5,), 2)
        field.loc[{'x': slice(10, 20), 'y': slice(0, 5)}] = subset

        values = field.isel()
        self.assertTrue(values.min() == 0)
        self.assertTrue(values.max() == 2)

    def test_subset_out_of_limit(self):
        field = self.create_field_from_numpy()
        values = field.isel({'x': slice(90, 110), 'y': slice(0, 5)})

        self.assertIsInstance(values, xr.DataArray)
        self.assertEqual(values.shape, (10, 5))

        values = field.cerbere.isel({'x': slice(90, 110), 'y': slice(0, 5)})

        self.assertIsInstance(values, xr.DataArray)
        self.assertEqual(values.shape, (10, 5))

    def test_subset_out_of_limit_with_padding(self):
        field = self.create_field_from_numpy()
        values = field.cerbere.isel(
            {'x': slice(90, 110), 'y': slice(0, 5)}, padding=True)

        self.assertIsInstance(values, xr.DataArray)
        self.assertEqual(values.shape, (20, 5))
        self.assertEqual(values.count(), 50)
        self.assertEqual(values[10:, :].count(), 0)

    def test_longitude_180(self):
        data = np.arange(0, 180)
        field = xr.DataArray(data, name='lon', dims=('lon',))
        field.cerbere.standard_name = 'longitude'

        lon = field.cerbere.isel()

        # verify correct longitude range
        self.assertTrue(lon.max() < 180)
        self.assertTrue(lon.min() >= -180)

        # verify properties are preserved by transformation
        self.assertEqual(field.cerbere.standard_name, 'longitude')
