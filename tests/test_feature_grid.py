import os
from datetime import datetime

import numpy as np
import xarray as xr

from cerbere.dataset.field import Field
from cerbere.dataset.ncdataset import NCDataset
from cerbere.feature.grid import CylindricalGrid

from .test_xarray_feature import TEST_FILE, TEST_SAVE, TestFeature


class TestGridFeature(TestFeature):
    """Test class for XArrayDataset"""

    def __init__(self, methodName='runTest'):
        super().__init__(methodName)

    def create_feature(cls):
        return CylindricalGrid(
            dataset={
                'lat': np.arange(-80, 80, 1),
                'lon': np.arange(-180, 180, 1),
                'time': [datetime(2018, 1, 1)],
                'myvar': np.ones(shape=(360, 160))
            }
        )

    def setUp(self):
        """ Setting up for the test """
        # creates a test xarray object
        lon = xr.DataArray(data=np.arange(-180, 180, 1), dims=['lon'])
        lat = xr.DataArray(data=np.arange(-80, 80, 1), dims=['lat'])
        time = xr.DataArray(data=[datetime(2018, 1, 1)], dims=['time'])
        var = xr.DataArray(data=np.ones(shape=(360, 160)), dims=['lon', 'lat'])
        var.attrs['myvar'] = 'test_attr_val'
        self.xrdataset = xr.Dataset(
            coords={'lat': lat, 'lon': lon, 'time': time},
            data_vars={'myvar': var}
        )
        self.xrdataset.attrs['gattr1'] = 'gattr1_val'
        self.xrdataset.attrs['gattr2'] = 'gattr2_val'

        # save it to disk
        self.xrdataset.to_netcdf(TEST_FILE, 'w')

    def tearDown(self):
        """Cleaning up after the test"""
        os.remove(TEST_FILE)

    def test_init_from_file(self):
        ncf = NCDataset(dataset=TEST_FILE)
        grid = CylindricalGrid(ncf)
        self.assertIsInstance(grid, CylindricalGrid)

    def test_init_from_xarraydataset(self):
        grid = CylindricalGrid(content=self.xrdataset)
        self.assertIsInstance(grid, CylindricalGrid)

    def test_init_from_data(self):
        grid = self.create_feature()
        self.assertIsInstance(grid, CylindricalGrid)

    def test_save_grid(self):
        try:
            ncf = NCDataset(dataset=TEST_SAVE, mode='w')
            grid = self.create_feature()
            grid.save(ncf)
            os.remove(TEST_SAVE)
        except:
            os.remove(TEST_SAVE)

    def test_get_lat(self):
        grid = self.create_feature()
        data = grid.get_lat()
        self.assertIsInstance(data, np.ndarray)

    def test_get_lon(self):
        grid = self.create_feature()
        data = grid.get_lon()
        self.assertIsInstance(data, np.ndarray)

    def test_get_times(self):
        grid = self.create_feature()
        data = grid.get_times()
        self.assertIsInstance(data, np.ndarray)
        self.assertTrue(np.issubdtype(data.dtype, np.datetime64))

    def test_get_datetimes(self):
        grid = self.create_feature()
        data = grid.get_datetimes()
        print(data, type(data))
        self.assertIsInstance(data, np.ndarray)
        self.assertIsInstance(data[0], datetime)

    def test_get_values(self):
        grid = self.create_feature()
        data = grid.get_values('myvar')
        self.assertIsInstance(data, np.ma.MaskedArray)

    def test_get_expanded_values(self):
        grid = self.create_feature()
        data = grid.get_values('lon', expanded=True)
        self.assertIsInstance(data, np.ma.MaskedArray)
        # self.assertEquals(data[])
        #
        # data = grid.get_values('lat', expanded=True)
        # self.assertIsInstance(data, np.ma.MaskedArray)
        #
        # data = grid.get_values('time', expanded=True)
        # self.assertIsInstance(data, np.ma.MaskedArray)

    def test_deprecated_options(self):
        grid = self.create_feature()
        self.assertWarns(Warning, grid.get_values, 'myvar', cache=True)

    def test_get_geolocation_field(self):
        grid = self.create_feature()
        data = grid.get_geolocation_field('lat')
        self.assertIsInstance(data, Field)
        print('DIMS ', data.dims)

    def test_add_field(self):
        grid = self.create_feature()
        print(grid.dims)
        da = xr.DataArray(
            dims=('lon', 'lat',),
            data=np.ones(shape=(360, 160))
        )
        newfield = Field(
            name='newfield',
            data=da
        )
        grid.add_field(newfield)
        print('DATASET', grid.dataset, grid.dataset.get_fieldnames())
        self.assertTrue('newfield' in grid.get_fieldnames())
        self.assertIsInstance(grid.get_field('newfield'), Field)

    def test_clone_field(self):
        ncf = NCDataset(dataset=TEST_FILE)
        grid = CylindricalGrid(ncf)

        cloned = grid.get_field('myvar').clone()
        print(cloned)
        self.assertIsInstance(cloned, Field)

        grid2 = self.create_feature()
        cloned2 = grid2.get_field('myvar').clone()
        print(cloned2)
        self.assertIsInstance(cloned2, Field)
        self.assertEqual(cloned.name, cloned2.name)
        #self.assertEqual(cloned.variable, cloned2.variable)
        #self.assertEqual(cloned.get_values(), cloned2.get_values())


    def test_get_bad_field(self):
        ncf = NCDataset(dataset=TEST_FILE)
        grid = CylindricalGrid(ncf)
        self.assertRaises(ValueError, grid.get_field, 'unknown_field')
