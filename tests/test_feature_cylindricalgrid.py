from datetime import datetime

import netCDF4 as netcdf
import numpy as np
import shapely.geometry
import xarray as xr

from cerbere.feature.grid import CylindricalGrid

from .test_feature import TEST_SAVE, TestFeature


class TestCylindricalGridFeature(TestFeature):
    """Test class for CylindricalGridFeature"""

    def get_feature_class(self):
        return CylindricalGrid

    def define_base_feature(self):
        # creates a test xarray object
        lon = xr.DataArray(data=np.arange(-180., 180., 1), dims=['lon'])
        lat = xr.DataArray(data=np.arange(-80., 80., 1), dims=['lat'])
        time = xr.DataArray([datetime(2018, 1, 1)], dims='time')
        var = xr.DataArray(
            data=np.ones(shape=(160, 360)),
            dims=['lat', 'lon'],
            attrs={'myattr': 'test_attr_val'}
        )
        xrdataset = xr.Dataset(
            coords={'lat': lat, 'lon': lon, 'time': time},
            data_vars={'myvar': var},
            attrs={'gattr1': 'gattr1_val', 'gattr2': 'gattr2_val'}
        )

        return CylindricalGrid(xrdataset)

    def get_feature_dimnames(self):
        return ('lat', 'lon',)

    def get_feature_dimsizes(self):
        return (160, 360)

    def test_create_feature_with_incorrect_geodim_order(self):
        basefeat = self.define_base_feature()
        feat = self.get_feature_class()(
            {
                'lat': {'data': basefeat.get_lat(), 'dims': ('lat',)},
                'lon': {'data': basefeat.get_lon(), 'dims': ('lon',)},
                'time': {
                    'data': np.array([datetime(2018, 1, 1)]), 'dims': ('time',)
                },
                'myvar': {
                    'data': np.ones(shape=(360, 160)), 'dims': ('lon', 'lat')
                }
            }
        )
        self.assertEqual(feat.get_field_dims('myvar'), ('lat', 'lon'))
        print('Feature from: test_create_feature_with_incorrect_geodim_order')
        print(feat)

    def test_create_feature_from_dict_datetime_1d(self):
        basefeat = self.define_base_feature()
        feat = self.get_feature_class()(
            {
                'lat': {'data': basefeat.get_lat(), 'dims': ('lat',)},
                'lon': {'data': basefeat.get_lon(), 'dims': ('lon',)},
                'time': {
                    'data': np.array([datetime(2018, 1, 1)]), 'dims': ('time',)
                },
                'myvar': {
                    'data': basefeat.get_values('myvar'), 'dims': ('lat', 'lon')
                }
            }
        )
        self.assertIsInstance(feat, self.get_feature_class())
        self.assertEqual(len(feat.get_field_dims('time')), 1)
        print('Feature from: test_create_feature_from_dict_datetime_1d')
        print(feat)

    def test_create_feature_from_dict_datetime64_1d(self):
        basefeat = self.define_base_feature()
        feat = self.get_feature_class()(
            {
                'lat': {'data': basefeat.get_lat(), 'dims': ('lat',)},
                'lon': {'data': basefeat.get_lon(), 'dims': ('lon',)},
                'time': {
                    'data': np.array([np.datetime64('2018-01-01')]),
                    'dims': ('time',)
                },
                'myvar': {
                    'data': basefeat.get_values('myvar'), 'dims': ('lat', 'lon')
                }
            }
        )

        self.assertIsInstance(feat, self.get_feature_class())
        self.assertEqual(len(feat.get_field_dims('time')), 1)
        print('Feature from: test_create_feature_from_dict_datetime64_1d')
        print(feat)

    def test_create_feature_from_dict_datetime64_2d(self):
        basefeat = self.define_base_feature()
        times = np.full(
            tuple(basefeat.get_field_sizes('myvar').values()),
            np.datetime64('2018-01-01'),
            dtype='datetime64[D]',
        )
        feat = self.get_feature_class()(
            {
                'coords': {
                    'lat': {'data': basefeat.get_lat(), 'dims': ('lat',)},
                    'lon': {'data': basefeat.get_lon(), 'dims': ('lon',)},
                    'time': {'data': times, 'dims': ('lat', 'lon',)}
                },
                'data_vars': {
                    'myvar': {
                        'data': basefeat.get_values('myvar'),
                        'dims': ('lat', 'lon')
                    }
                },
            }
        )

        self.assertIsInstance(feat, self.get_feature_class())
        self.assertEqual(len(feat.get_field_dims('time')), 2)
        print('Feature from: test_create_feature_from_dict_datetime64_2d')
        print(feat)

    def test_create_feature_from_dict_datetime64_2d_v2(self):
        basefeat = self.define_base_feature()
        times = np.full(
            tuple(basefeat.get_field_sizes('myvar').values()),
            np.datetime64('2018-01-01'),
            dtype='datetime64[D]',
        )
        feat = self.get_feature_class()(
            {
                'lat': {'data': basefeat.get_lat(), 'dims': ('lat',)},
                'lon': {'data': basefeat.get_lon(), 'dims': ('lon',)},
                'time': {
                    'data': times,
                    'dims': ('lat', 'lon',)
                },
                'myvar': {
                    'data': basefeat.get_values('myvar'), 'dims': ('lat', 'lon')
                }
            }
        )

        self.assertIsInstance(feat, self.get_feature_class())
        self.assertEqual(len(feat.get_field_dims('time')), 2)
        print('Feature from: test_create_feature_from_dict_datetime64_1d')
        print(feat)

    def test_create_feature_from_xarray_keywords(self):
        feat = self.get_feature_class()(
            {'myvar': (['lat', 'lon'], np.ones(shape=(160, 360)))},
            coords={
                'time': (['time'], [datetime(2018, 1, 1)], {
                             'units': 'seconds since 2001-01-01 00:00:00'}),
                'lat': (['lat'], np.arange(-80, 80, 1)),
                'lon': (['lon'], np.arange(-180, 180, 1))
            },
            attrs={'gattr1': 'gattr_val'}
        )
        self.assertIsInstance(feat, self.get_feature_class())
        self.assertEqual(len(feat.get_field_dims('time')), 1)
        print('Feature from: test_create_feature_from_xarray_keywords')
        print(feat)

    def test_create_feature_from_xarray_args(self):
        feat = self.get_feature_class()({
            'coords': {
                'time': {'dims': ('time'), 'data': [datetime(2018, 1, 1)]},
                'lat': {'dims': ('lat'), 'data': np.arange(-80, 80, 1)},
                'lon': {'dims': ('lon',), 'data': np.arange(-180, 180, 1)}},
            'attrs': {'gattr1': 'gattr_val'},
            'data_vars': {'myvar': {'dims': ('lat', 'lon',), 'data': np.ones(shape=(160, 360))}}
        })
        self.assertIsInstance(feat, self.get_feature_class())
        self.assertEqual(len(feat.get_field_dims('time')), 1)
        print('Feature from: test_create_feature_from_xarray_args')
        print(feat)

    def test_expanded_latlon(self):
        basefeat = self.define_base_feature()
        res = basefeat.get_values(
            'lat',
            index={'lat': slice(10, 15), 'lon': slice(50, 55)},
            expand=False
        )
        self.assertEqual(res.shape, (5,))
        self.assertTrue(np.equal(res, np.arange(-70, -65)).all())

        res = basefeat.get_values(
            'lat',
            index={'lat': 10, 'lon': 50},
            expand=True
        )
        self.assertEqual(res, -70)

        res = basefeat.get_values(
            'lon',
            index={'lat': 10, 'lon': 50},
            expand=True
        )
        self.assertEqual(res, -130)

        res = basefeat.get_values(
            'lat',
            index={'lat': slice(10, 15), 'lon': slice(50, 53)},
            expand=True
        )
        self.assertEqual(res.shape, (5, 3,))
        self.assertTrue(np.equal(res[:, 0], np.arange(-70, -65)).all())
        self.assertTrue(np.equal(res[0, :], np.ones((3,)) * -70).all())

        res = basefeat.get_values(
            'lon',
            index={'lat': slice(10, 15), 'lon': slice(50, 53)},
            expand=True
        )
        self.assertEqual(res.shape, (5, 3,))
        self.assertTrue(np.equal(res[0, :], np.arange(-130, -127)).all())
        self.assertTrue(np.equal(res[:, 0], np.ones((5,)) * -130).all())

        res = basefeat.get_values(
            'lat',
            expand=True
        )
        print('result: test_expanded_latlon ', res)

    def test_expanded_latlon(self):
        basefeat = self.define_base_feature()

        res = basefeat.get_values(
            'time',
            index={'lat': slice(10, 15), 'lon': slice(50, 53)},
            expand=True
        )
        self.assertEqual(res.shape, (5, 3,))

        res = basefeat.get_values(
            'time',
            expand=True
        )
        print('result: test_expanded_time ', res)

    def test_clip(self):
        basefeat = self.define_base_feature()
        subfeature = basefeat.clip(shapely.geometry.box(-180, 0, 0, 70))[0]
        self.assertEqual(
            dict(subfeature.dims),
            dict([('lat', 71), ('lon', 181), ('time', 1)]))

    def test_save_feature(self):
        super(TestCylindricalGridFeature, self).test_save_feature()
        dst = netcdf.Dataset(TEST_SAVE)

        self.assertEqual(dst.variables['lat'].getncattr('_FillValue'),
                         np.float32(1e20))
        self.assertEqual(dst.variables['lat'].dtype, np.dtype(np.float32))
