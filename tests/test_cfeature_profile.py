from cerbere.feature.cprofile import Profile

from cerbere.feature.pytest_cfeature import *


@pytest.fixture(scope='module')
def feature_class():
    return Profile


@pytest.fixture(scope='module')
def feature_instance():
    # creates a test xarray object
    lon = xr.DataArray(data=100., dims=[])
    lat = xr.DataArray(data=50., dims=[])
    z = xr.DataArray(data=np.arange(0, 1000, 10), dims=['z'])
    time = xr.DataArray(np.datetime64('2018-01-01 00:00:00', 'ns'), dims=())
    profile = xr.DataArray(
        data=np.int32(1),
        dims=(),
        attrs={'cf_role': "profile_id"}
    )

    var = xr.DataArray(
        data=np.ones(shape=(100,)),
        dims=['z'],
        attrs={'myattr': 'test_attr_val'}
    )
    xrdataset = xr.Dataset(
        coords={'lat': lat, 'lon': lon, 'time': time, 'z': z,
                'profile': profile},
        data_vars={'my_var': var},
        attrs={'gattr1': 'gattr1_val', 'gattr2': 'gattr2_val'}
    )
    return Profile(xrdataset)


@pytest.fixture(scope='module')
def feature_dimnames():
    return 'z',


@pytest.fixture(scope='module')
def feature_dimsizes():
    return 100,


@pytest.fixture(scope='module')
def content_class():
    return
