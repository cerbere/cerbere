"""
Test base mapper class
"""
import os
import unittest
from collections import OrderedDict
from datetime import datetime

import numpy as np
import xarray as xr
from shapely.geometry import Polygon

from cerbere.dataset.dataset import Dataset
from cerbere.dataset.field import Field


class TestXArrayDataset(unittest.TestCase):
    """Test class for XArrayDataset"""

    def __init__(self, methodName='runTest'):
        super().__init__(methodName)

    def setUp(self):
        """ Setting up for the test """
        # creates a test xarray object
        lon = xr.DataArray(data=np.arange(-180, 180, 1), dims=['lon'])
        lat = xr.DataArray(data=np.arange(-80, 80, 1), dims=['lat'])
        var = xr.DataArray(data=np.ones(shape=(360, 160)), dims=['lon', 'lat'])
        var.attrs['test_attr'] = 'test_attr_val'
        self.xrdataset = xr.Dataset(
            coords={'lat': lat, 'lon': lon, 'time': [datetime(2018, 1, 1, 12)]},
            data_vars={'test_var': var}
        )
        self.xrdataset.attrs['gattr1'] = 'gattr1_val'
        self.xrdataset.attrs['gattr2'] = 'gattr2_val'

        # save it to disk
        self.xrdataset.to_netcdf('test_xarraydataset.nc', 'w')

    def tearDown(self):
        """Cleaning up after the test"""
        os.remove('test_xarraydataset.nc')

    def test_init_from_xarray(self):
        dst = Dataset(self.xrdataset)
        self.assertIsInstance(dst, Dataset)

    def test_init_from_file(self):
        dst = Dataset('test_xarraydataset.nc')
        self.assertIsInstance(dst, Dataset)

    def test_init_from_dict1(self):
        dst = Dataset(
            {'time': {'dims': ('time'), 'data': [datetime(2018, 1, 1)]},
             'lat': {'dims': ('lat'), 'data': np.arange(-80, 80, 1)},
             'lon': {'dims': ('lon',), 'data': np.arange(-180, 180, 1)},
             'myvar': {'dims': ('lat', 'lon',),
                       'data': np.ones(shape=(160, 360))}
             }
        )
        print(dst)
        self.assertIsInstance(dst, Dataset)

    def test_init_from_dict2(self):
        dst = Dataset(
            {'coords': {
                'time': {'dims': ('time'), 'data': [datetime(2018, 1, 1)],
                         'attrs': {'units': 'seconds since 2001-01-01 00:00:00'}},
                'lat': {'dims': ('lat'), 'data': np.arange(-80, 80, 1)},
                'lon': {'dims': ('lon',), 'data': np.arange(-180, 180, 1)},
            },
            'attrs': {'gattr1': 'gattr_val'},
            'dims': ('time', 'lon', 'lat'),
            'data_vars': {'myvar': {'dims': ('lat', 'lon',),
                                    'data': np.ones(shape=(160, 360))}}}
        )
        print(dst)
        self.assertIsInstance(dst, Dataset)

    def test_init_from_dict3(self):
        field = Field(
            np.ones(shape=(160, 360)),
            'myvar',
            dims=('lat', 'lon',),
            attrs={'myattr': 'attr_val'}
        )
        dst = Dataset(
            {'time': {'dims': ('time'), 'data': [datetime(2018, 1, 1)]},
             'lat': {'dims': ('lat'), 'data': np.arange(-80, 80, 1)},
             'lon': {'dims': ('lon',), 'data': np.arange(-180, 180, 1)},
             'myvar': field
             }
        )
        print(dst)
        self.assertIsInstance(dst, Dataset)

    def test_init_from_xarray_keywords(self):
        dst = Dataset(
            {'myvar': (['lat', 'lon'], np.ones(shape=(160, 360)))},
            coords={
                'time': (['time'], [datetime(2018, 1, 1)], {
                             'units': 'seconds since 2001-01-01 00:00:00'}),
                'lat': (['lat'], np.arange(-80, 80, 1)),
                'lon': (['lon'], np.arange(-180, 180, 1))
            },
            attrs={'gattr1': 'gattr_val'}
        )
        print(dst)
        self.assertIsInstance(dst, Dataset)

    def test_dimension_names(self):
        print('...from xarray.Dataset')
        refdims = list(('lat', 'lon', 'time'))
        refdims.sort()

        dst = Dataset(self.xrdataset)
        self.assertIsInstance(dst.dimnames, tuple)
        dstdims = list(dst.dimnames)
        dstdims.sort()
        self.assertSequenceEqual(dstdims, refdims)

        print('...from file')
        dst = Dataset('test_xarraydataset.nc')
        self.assertIsInstance(dst.dimnames, tuple)
        dstdims = list(dst.dimnames)
        dstdims.sort()
        self.assertSequenceEqual(dstdims, refdims)

    def test_full_dimensions(self):
        print('...from xarray.Dataset')
        dst = Dataset(self.xrdataset)
        self.assertIsInstance(dst.dims, OrderedDict)
        dims = OrderedDict([('lat', 160), ('lon', 360), ('time', 1)])
        self.assertDictEqual(dict(dst.dims), dict(dims))

        print('...from file')
        dst = Dataset('test_xarraydataset.nc')
        self.assertIsInstance(dst.dims, OrderedDict)
        dims = OrderedDict([('lat', 160), ('lon', 360), ('time', 1)])
        self.assertDictEqual(dict(dst.dims), dict(dims))

    def test_var_get_dimensions(self):
        print('...from xarray.Dataset')
        dst = Dataset(self.xrdataset)
        self.assertIsInstance(dst.get_field_dims('test_var'), tuple)
        self.assertEqual(dst.get_field_dims('test_var'), ('lon', 'lat'))

        print('...from file')
        dst = Dataset('test_xarraydataset.nc')
        self.assertIsInstance(dst.get_field_dims('test_var'), tuple)
        self.assertEqual(dst.get_field_dims('test_var'), ('lon', 'lat'))

    def test_get_full_dimensions(self):
        print('...from xarray.Dataset')
        dst = Dataset(self.xrdataset)
        self.assertIsInstance(dst.get_field_sizes('test_var'),
                              OrderedDict)
        self.assertDictEqual(
            dst.get_field_sizes('test_var'),
            OrderedDict([('lon', 360), ('lat', 160)]))

        print('...from file')
        dst = Dataset('test_xarraydataset.nc')
        self.assertIsInstance(dst.get_field_sizes('test_var'),
                              OrderedDict)
        self.assertDictEqual(dst.get_field_sizes('test_var'),
                             OrderedDict([('lon', 360), ('lat', 160)]))

    def test_var_get_dimsize(self):
        print('...from xarray.Dataset')
        dst = Dataset(self.xrdataset)
        self.assertEqual(dst.get_dimsize('lon'), 360)

        print('...from file')
        dst = Dataset('test_xarraydataset.nc')
        self.assertEqual(dst.get_dimsize('lon'), 360)

    def test_var_get_field_attributes(self):
        print('...from xarray.Dataset')
        dst = Dataset(self.xrdataset)
        self.assertIsInstance(dst.get_field_attrs('test_var'), dict)
        self.assertEqual(dst.get_field_attrs('test_var'),
                         {'test_attr': 'test_attr_val'})

        print('...from file')
        dst = Dataset('test_xarraydataset.nc')
        self.assertIsInstance(dst.get_field_attrs('test_var'), dict)
        self.assertEqual(dst.get_field_attrs('test_var'),
                         {'test_attr': 'test_attr_val'})

    def test_var_get_global_attributes(self):
        attrs = {
            'gattr1': 'gattr1_val', 'gattr2': 'gattr2_val',
        }

        print('...from xarray.Dataset')
        dst = Dataset(self.xrdataset)

        # verify standard attributes are added
        print(dst.dataset.cerbere.time_coverage_start)
        print(dst.dataset.attrs)
        self.assertTrue('time_coverage_start' in dst.attrs)
        self.assertTrue('time_coverage_end' in dst.attrs)

        self.assertIsInstance(dst.attrs, dict)
        self.assertDictContainsSubset(attrs, dict(dst.attrs))

        print('...from file')
        dst = Dataset('test_xarraydataset.nc')
        self.assertIsInstance(dst.attrs, dict)
        self.assertDictContainsSubset(attrs, dict(dst.attrs))

    def test_var_get_global_attribute(self):
        print('...from xarray.Dataset')
        dst = Dataset(self.xrdataset)
        self.assertEqual(dst.get_attr('gattr1'), 'gattr1_val')

        print('...from file')
        dst = Dataset('test_xarraydataset.nc')
        self.assertEqual(dst.get_attr('gattr1'), 'gattr1_val')

    def test_get_time_coverage(self):
        print('Test time coverage')
        print('...from xarray.Dataset')
        dst = Dataset(self.xrdataset)
        print(dst.dataset.attrs)
        self.assertEqual(dst.time_coverage_start, datetime(2018, 1, 1, 12))
        self.assertEqual(dst.time_coverage_end, datetime(2018, 1, 1, 12))

        print('...from file')
        dst = Dataset('test_xarraydataset.nc')
        print(dst.dataset.attrs)
        self.assertEqual(dst.time_coverage_start, datetime(2018, 1, 1, 12))
        self.assertEqual(dst.time_coverage_end, datetime(2018, 1, 1, 12))

    def test_print(self):
        print('Test print')
        dst = Dataset(self.xrdataset)
        print(dst)
        self.assertIsInstance(str(dst), str)

    def test_get_fieldnames(self):
        print('test get_fieldnames()')
        print('...from xarray.Dataset')
        dst = Dataset(self.xrdataset)
        self.assertEqual(dst.fieldnames, ['test_var'])

        print('...from file')
        dst = Dataset('test_xarraydataset.nc')
        self.assertEqual(dst.fieldnames, ['test_var'])

    def test_save_readonly(self):
        dst = Dataset(self.xrdataset)
        with self.assertRaises(OSError):
            dst.save('test_xarraydataset.nc')

    def test_view(self):
        print('Test view argument')
        with self.assertRaises(DeprecationWarning):
            ds = Dataset(
                self.xrdataset,
                view={'lon': slice(100, 200), 'lat': slice(0, 80)})

    def test_add_field(self):
        dst = Dataset(self.xrdataset)
        new_field = Field(
            xr.DataArray(
                data=np.zeros(shape=(360, 160)),
                dims=('lon', 'lat',)
            ),
            name='newvar'
        )
        dst.add_field(new_field)
        self.assertTrue('newvar' in dst.fieldnames)
        self.assertSequenceEqual(dst.get_values('newvar').shape, (360, 160))

    def test_extract(self):
        dst = Dataset(self.xrdataset)
        subset = dst.extract(deep=True)
        self.assertTrue('test_var' in subset.fieldnames)
        self.assertSequenceEqual(
            subset.get_values('test_var').shape, (360, 160)
        )
        self.assertIsNot(
            subset.get_field('test_var').to_xarray(),
            dst.get_field('test_var').to_xarray()
        )

    def test_extract_as_view(self):
        dst = Dataset(self.xrdataset)
        subset = dst.extract(deep=False)
        self.assertTrue('test_var' in subset.fieldnames)
        self.assertSequenceEqual(
            subset.get_values('test_var').shape, (360, 160)
        )
        self.assertIs(
            subset.get_field('test_var').to_xarray().data,
            dst.get_field('test_var').to_xarray().data
        )

    def test_extract_rename(self):
        dst = Dataset(self.xrdataset)
        subset = dst.extract(prefix='new')
        self.assertTrue('new_test_var' in subset.fieldnames)
        self.assertSequenceEqual(
            subset.get_values('new_test_var').shape, (360, 160)
        )
        self.assertIsNot(
            subset.get_field('new_test_var').to_xarray(),
            dst.get_field('test_var').to_xarray()
        )

    def test_extract_subset(self):
        dst = Dataset(self.xrdataset)
        subset = dst.extract(
            index={'lat': slice(10, 20), 'lon': slice(10, 15)}, deep=True
        )
        self.assertTrue('test_var' in subset.fieldnames)
        self.assertSequenceEqual(
            subset.get_values('test_var').shape, (5, 10)
        )
        self.assertIsNot(
            subset.get_field('test_var').to_xarray(),
            dst.get_field('test_var').to_xarray()
        )

    def test_extract_subset_padding(self):
        dst = Dataset(self.xrdataset)
        subset = dst.extract(
            index={'lat': slice(10, 20), 'lon': slice(355, 365)},
            padding=True,
            deep=True
        )
        self.assertTrue('test_var' in subset.fieldnames)
        self.assertSequenceEqual(
            subset.get_values('test_var').shape, (10, 10)
        )
        self.assertIsNot(
            subset.get_field('test_var').to_xarray(),
            dst.get_field('test_var').to_xarray()
        )
        self.assertEqual(subset.get_values('test_var').count(), 50)
        self.assertEqual(subset.get_values('test_var').size, 100)

    def test_save_with_profile(self):
        dst = Dataset(self.xrdataset)
        if os.path.exists('test_profile.nc'):
            os.remove('test_profile.nc')
        dst.save(
            dest='test_profile.nc',
            profile='default_saving_profile.yaml'
        )

    def test_opening_file_not_existing(self):
        def open_dummy():
            return Dataset('file_not_existing.nc')
        self.assertRaises(IOError, open_dummy)

    def test_clip_values(self):
        dst = Dataset(self.xrdataset)
        subset = dst.clip_values(
            'test_var',
            Polygon([(-80, -40), (80, -40), (80, 20), (-80, 20)])
        )
        self.assertEqual(len(subset), 1)
        self.assertSequenceEqual(subset[0].shape, (161, 61))

    def test_clip(self):
        dst = Dataset(self.xrdataset)
        subset = dst.clip(
            Polygon([(-80, -40), (80, -40), (80, 20), (-80, 20)])
        )
        self.assertEqual(len(subset), 1)
        self.assertDictEqual(
            dict(subset[0].sizes), {'lon': 161, 'lat': 61, 'time': 1}
        )

    def test_xarray_masked_int(self):
        """check consistency of xarray"""
        values = np.zeros(shape=(360, 160), dtype=np.int32)
        values[10:20, 10:20] = 1
        values = np.ma.masked_equal(values, 1, copy=False)

        dr = xr.DataArray(data=values, dims=('lon', 'lat'))
        self.assertEqual(values.dtype, np.dtype(np.int32))
        self.assertEqual(dr.values.dtype, np.dtype(np.float64))

    def test_masked_int(self):
        dst = Dataset(self.xrdataset)
        values = np.zeros(shape=(360, 160), dtype=np.int32)
        values[10:20, 10:20] = 1
        values = np.ma.masked_equal(values, 1, copy=False)

        new_field = Field(
            xr.DataArray(
                data=values,
                dims=('lon', 'lat',)
            ),
            dtype=np.int32,
            fill_value=np.int32(99999),
            name='newvar'
        )
        dst.add_field(new_field)

        self.assertEqual(dst.get_values('newvar').count(), 360*160 - 100)
        self.assertEqual(dst.get_values('newvar').dtype, np.int32)
