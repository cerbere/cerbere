'''
Created on Aug 15, 2018

@author: jfpiolle
'''
import unittest

import dask
import numpy as np
import xarray as xr

import cerbere
from cerbere.dataset.field import Field


class TestField(unittest.TestCase):
    """Test class for AbstractFeature"""

    @classmethod
    def create_field_from_numpy(cls):
        data = np.ma.zeros((100, 200))
        return Field(data, name='test', dims=('x', 'y'))

    def test_create_int_field_from_numpy_no_dims(self):
        data = np.ma.zeros((100, 200), dtype=np.int32)
        field = Field(data=data, name='test')

        self.assertIsInstance(field, Field)
        self.assertEqual(field.to_xarray().name, 'test')
        self.assertEqual(field.dtype, np.dtype(np.int32))
        self.assertEqual(np.dtype(type(field.fill_value)).kind,
                         field.dtype.kind)
        self.assertEqual(field.dims, ('dim_0', 'dim_1'))
        field.get_values()
        self.assertEqual(field.dtype,
                         field.get_values().dtype)

    def test_mask_with_different_init_arrays(self):
        # ndarray
        data = np.zeros((100, 200), dtype=np.int32)
        data[10:20, 10:20] = 1
        field = Field(data=data, fill_value=np.int32(1), name='test')
        self.assertEqual(field.get_values().count(), data.size - 100)

        # masked array
        data = np.ma.zeros((100, 200), dtype=np.int32)
        data[10:20, 10:20] = 1
        data = np.ma.masked_equal(data, 1)
        field = Field(data=data, name='test')
        self.assertEqual(field.get_values().count(), data.size - 100)

        # DataArray
        data = xr.zeros_like(field.to_xarray(), dtype=np.int32)
        data[10:20, 10:20] = 1
        field = Field(data=data, fill_value=np.int32(1), name='test')
        self.assertEqual(field.get_values().count(), data.size - 100)

        # Dask Array
        data = np.zeros((100, 200), dtype=np.int32)
        dask.array.zeros_like(data, dtype=np.int32)
        data[10:20, 10:20] = 1
        data = np.ma.masked_equal(data, 1)
        field = Field(data=data, fill_value=np.int32(1), name='test')
        self.assertEqual(field.get_values().count(), data.size - 100)

    def test_create_masked_int_field_from_numpy_no_dims(self):
        data = np.ma.zeros((100, 200), dtype=np.int32)
        data[0:20, 0:20] = 1
        data = np.ma.masked_equal(data, 0)
        self.assertEqual(data.count(), 400)

        field = Field(data=data, name='test')

        self.assertIsInstance(field, Field)
        self.assertEqual(field.dtype, np.dtype(np.int32))

        self.assertEqual(field.to_xarray().name, 'test')
        self.assertEqual(field.dtype, np.dtype(np.int32))
        self.assertEqual(np.dtype(type(field.fill_value)).kind,
                         field.dtype.kind)
        self.assertEqual(field.dims, ('dim_0', 'dim_1'))
        self.assertEqual(field.to_xarray().dtype, np.dtype(np.float64))
        self.assertEqual(
            field.to_xarray(decoding=True).dtype, np.dtype(np.int32))
        self.assertEqual(field._array.dtype, np.dtype(np.float64))

        self.assertEqual(field.dtype,
                         field.get_values().dtype)
        self.assertEqual(field.get_values().count(), 400)

        self.assertEqual(field.dtype,
                         field.get_values({'dim_0': slice(0, 30),
                                           'dim_1': slice(0, 30)}).dtype)
        self.assertEqual(field.get_values().count(), 400)

    def test_create_int_field_from_numpy_no_missing_value(self):
        # mask array comes with a default fill value (here 999999 for int32)
        # no_missing_value force it to be undefined
        data = np.ma.zeros((100, 200), dtype=np.int32)
        field = Field(data=data, name='test', no_missing_value=True)

        self.assertIsInstance(field, Field)
        self.assertEqual(field.dtype, np.dtype(np.int32))
        self.assertEqual(field.fill_value, None)

    def test_masked_int_dtype_mismatch(self):
        """Test creating a DataArray with mismatched dtype and fillvalue"""
        values = np.zeros(shape=(360, 160), dtype=np.int32)
        values[10:20, 10:20] = 1
        values = np.ma.masked_equal(values, 1, copy=False)
        with self.assertRaises(TypeError):
            field = Field(
                values,
                fill_value=np.float32(0),
                name='newvar'
            )
            print(field.dtype, field._array.dtype)
            print(field.fill_value)

    def test_create_float_field_from_numpy2(self):
        data = np.ma.zeros((100, 200), dtype=np.float32)
        field = Field(data=data, name='test')

        self.assertIsInstance(field, Field)
        self.assertEqual(field.to_xarray().name, 'test')
        self.assertEqual(field.dtype, np.dtype(np.float32))

        self.assertEqual(np.dtype(type(field.fill_value)).kind,
                         field.dtype.kind)
        self.assertEqual(field.dims, ('dim_0', 'dim_1'))
        self.assertEqual(field.dtype,
                         field.get_values().dtype)

    def test_create_masked_float_field_from_numpy_no_dims(self):
        data = np.ma.zeros((100, 200), dtype=np.float32)
        data[10:20, 10:20] = 1
        data = np.ma.masked_equal(data, 0)
        self.assertEqual(data.count(), 100)

        field = Field(data=data, name='test')

        self.assertIsInstance(field, Field)
        self.assertEqual(field.to_xarray().name, 'test')
        self.assertEqual(field.dtype, np.dtype(np.float32))
        self.assertEqual(np.dtype(type(field.fill_value)).kind,
                         field.dtype.kind)
        self.assertEqual(field.dims, ('dim_0', 'dim_1'))
        self.assertEqual(field.dtype,
                         field.get_values().dtype)
        self.assertEqual(field.get_values().count(), 100)

    def test_create_field_from_numpy3(self):
        data = np.ma.zeros((100, 200))
        field = Field(data=xr.DataArray(data), name='test')

        self.assertIsInstance(field, Field)

    def test_create_field(self):
        field = self.create_field_from_numpy()
        self.assertIsInstance(field, Field)
        print(field)

    def test_get_xarray(self):
        field = self.create_field_from_numpy()
        xarr = field.to_xarray()
        self.assertIsInstance(xarr, xr.DataArray)
        print(xarr.dims)
        self.assertTrue('x' in xarr.dims)
        self.assertTrue('y' in xarr.dims)
        print(xarr)

    def test_get_values(self):
        field = self.create_field_from_numpy()
        values = field.get_values()
        self.assertIsInstance(values, np.ma.MaskedArray)

    def test_inline_value_modification(self):
        data = np.ma.zeros((100, 200), dtype=np.float32)
        data[10:20, 10:20] = 1
        data = np.ma.masked_equal(data, 0)
        self.assertEqual(data.count(), 100)

        field = Field(data=data, name='test')

        values_original = field.get_values()

        # modify the returned array and verify the field was modified too
        values_original[10:15, 10:15] = 3

        np.testing.assert_equal(
            field.get_values()[10:15, 10:15], values_original[10:15, 10:15])


    def test_set_values(self):
        field = self.create_field_from_numpy()
        values = field.get_values()
        values[:] = 2
        values = field.get_values()
        self.assertTrue(values.min() == 2)
        self.assertTrue(values.max() == 2)

    def test_set_values_func(self):
        field = self.create_field_from_numpy()
        values = np.full((100, 200,), 2)
        field.set_values(values)
        self.assertTrue(values.min() == 2)
        self.assertTrue(values.max() == 2)

    def test_get_values_subset(self):
        field = self.create_field_from_numpy()
        values = field.get_values({'x': slice(10, 20), 'y': slice(0, 5)})
        self.assertIsInstance(values, np.ma.MaskedArray)
        self.assertEqual(values.shape, (10, 5))

    def test_set_values_subset(self):
        field = self.create_field_from_numpy()
        subset = np.full((10, 5,), 2)
        field.set_values(subset,  {'x': slice(10, 20), 'y': slice(0, 5)})
        values = field.get_values()
        self.assertTrue(values.min() == 0)
        self.assertTrue(values.max() == 2)

    def test_subset_out_of_limit(self):
        field = self.create_field_from_numpy()
        values = field.get_values({'x': slice(90, 110), 'y': slice(0, 5)})
        self.assertIsInstance(values, np.ma.MaskedArray)
        self.assertEqual(values.shape, (10, 5))

    def test_subset_out_of_limit_with_padding(self):
        field = self.create_field_from_numpy()
        values = field.get_values({'x': slice(90, 110), 'y': slice(0, 5)},
                                  padding=True)
        self.assertIsInstance(values, np.ma.MaskedArray)
        self.assertEqual(values.shape, (20, 5))
        self.assertEqual(values.count(), 50)
        self.assertEqual(values[10:, :].count(), 0)

    def test_apply_func(self):
        field1 = self.create_field_from_numpy()
        field2 = self.create_field_from_numpy()
        np.ma.multiply(field1.get_values(), field2.get_values())
        fsum = Field.compute(np.ma.multiply, field1, field2)
        print('SUM ', fsum)
