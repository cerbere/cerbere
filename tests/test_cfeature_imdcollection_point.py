from cerbere.feature.cimdcollection import IMDCollection
from cerbere.feature.cpoint import Point

from cerbere.feature.pytest_cfeature import *


@pytest.fixture(scope='module')
def feature_class():
    return IMDCollection


@pytest.fixture(scope='module')
def feature_instance():
    # creates a test xarray object
    lon = xr.DataArray(data=[100., 100], dims=['obs'])
    lat = xr.DataArray(data=[50., 50], dims=['obs'])

    time = xr.DataArray(
        [np.datetime64('2018-01-01 00:00:00', 'ns'),
         np.datetime64('2018-01-01 00:00:00', 'ns')],
        dims=['obs']
    )
    var = xr.DataArray(
        data=np.ones(shape=(2,)),
        dims=['obs', ],
        attrs={'myattr': 'test_attr_val'}
    )
    xrdataset = xr.Dataset(
        coords={'lat': lat, 'lon': lon, 'time': time},
        data_vars={'my_var': var},
        attrs={'gattr1': 'gattr1_val', 'gattr2': 'gattr2_val'}
    )

    return IMDCollection(xrdataset, instance_class='Point')


@pytest.fixture(scope='module')
def feature_dimnames():
    return 'obs',


@pytest.fixture(scope='module')
def feature_dimsizes():
    return 2,


@pytest.fixture(scope='module')
def instance_class():
    return Point
