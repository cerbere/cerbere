from cerbere.feature.cgrid import Grid

from cerbere.feature.pytest_cfeature import *


@pytest.fixture(scope='module')
def feature_class():
    return Grid


@pytest.fixture(scope='module')
def feature_instance():
    # creates a test xarray object
    lon = xr.DataArray(data=np.arange(-180, 180, 1), dims=['lon'])
    lat = xr.DataArray(data=np.arange(-80, 80, 1), dims=['lat'])
    time = xr.DataArray([datetime(2018, 1, 1)], dims=['time'])
    z = xr.DataArray(data=np.arange(0, 10, 1), dims=['z'])

    var = xr.DataArray(
        data=np.ones(shape=(160, 360, 10)),
        dims=['lat', 'lon', 'z'],
        attrs={'myattr': 'test_attr_val'}
    )
    xrdataset = xr.Dataset(
        coords={'lat': lat, 'lon': lon, 'time': time, 'z': z},
        data_vars={'my_var': var},
        attrs={'gattr1': 'gattr1_val', 'gattr2': 'gattr2_val'}
    )
    return Grid(xrdataset)


@pytest.fixture(scope='module')
def feature_dimnames():
    return 'lat', 'lon', 'time',


@pytest.fixture(scope='module')
def feature_dimsizes():
    return 160, 360, 1,


@pytest.fixture(scope='module')
def content_class():
    return

# TODO Update
# def test_expanded_latlon():
#     basefeat = self.define_base_feature()
#     res = basefeat.get_values(
#         'lat',
#         index={'lat': slice(10, 15), 'lon': slice(50, 55)},
#         expand=False
#     )
#     self.assertEqual(res.shape, (5,))
#     self.assertTrue(np.equal(res, np.arange(-70, -65)).all())
#
#     res = basefeat.get_values(
#         'lat',
#         index={'lat': 10, 'lon': 50},
#         expand=True
#     )
#     self.assertEqual(res, -70)
#
#     res = basefeat.get_values(
#         'lon',
#         index={'lat': 10, 'lon': 50},
#         expand=True
#     )
#     self.assertEqual(res, -130)
#
#     res = basefeat.get_values(
#         'lat',
#         index={'lat': slice(10, 15), 'lon': slice(50, 53)},
#         expand=True
#     )
#     self.assertEqual(res.shape, (5, 3,))
#     self.assertTrue(np.equal(res[:, 0], np.arange(-70, -65)).all())
#     self.assertTrue(np.equal(res[0, :], np.ones((3,)) * -70).all())
#
#     res = basefeat.get_values(
#         'lon',
#         index={'lat': slice(10, 15), 'lon': slice(50, 53)},
#         expand=True
#     )
#     self.assertEqual(res.shape, (5, 3,))
#     self.assertTrue(np.equal(res[0, :], np.arange(-130, -127)).all())
#     self.assertTrue(np.equal(res[:, 0], np.ones((5,)) * -130).all())
#
#     res = basefeat.get_values(
#         'lat',
#         expand=True
#     )
#     print('result: test_expanded_latlon ', res)
#
