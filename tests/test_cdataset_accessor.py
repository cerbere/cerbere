'''
Created on Aug 15, 2018

@author: jfpiolle
'''
from collections import OrderedDict
import os
from datetime import datetime
from pathlib import Path
import unittest

import dask
import numpy as np
from shapely.geometry import Polygon
import xarray as xr

import cerbere.accessor.cdataset


TEST_FILE = '/home/jfpiolle/git/naiad-index/tests/data/avhrr_metop' \
            '/20200330235803-OSISAF-L2P_GHRSST-SSTsubskin-AVHRR_SST_METOP_B' \
            '-sstmgr_metop01_20200330_235803-v02.0-fv01.0.nc'


class TestDataset(unittest.TestCase):
    """Test class for cerbere Dataset accessor"""
    def __init__(self, methodName='runTest'):
        super().__init__(methodName)

    def setUp(self):
        """ Setting up for the test """
        # creates a test xarray object
        lon = xr.DataArray(data=np.arange(-180, 180, 1.), dims=['lon'])
        lat = xr.DataArray(data=np.arange(-80, 80, 1.), dims=['lat'])
        var = xr.DataArray(data=np.ones(shape=(360, 160)), dims=['lon', 'lat'])
        var.attrs['test_attr'] = 'test_attr_val'
        self.xrdataset = xr.Dataset(
            coords={'lat': lat, 'lon': lon, 'time': [datetime(2018, 1, 1, 12)]},
            data_vars={'test_var': var}
        )
        self.xrdataset.attrs['gattr1'] = 'gattr1_val'
        self.xrdataset.attrs['gattr2'] = 'gattr2_val'

        # save it to disk
        self.xrdataset.to_netcdf('test_xarraydataset.nc', 'w')

    def tearDown(self):
        """Cleaning up after the test"""
        os.remove('test_xarraydataset.nc')


    # @TODO test longitude [0, 360]

    def test_from_file(self):
        ds = cerbere.open_dataset(TEST_FILE, reader='GHRSST')

        self.assertTrue('time' in ds.coords)
        self.assertEqual(ds['time'].dims, ('row', 'cell',))

        sds = ds.cb.cfdataset
        print(sds)

        # check coordinates
        self.assertTrue(
            all([_ in sds.coords for _ in ['time', 'lat', 'lon']]))

        # check dims
        self.assertTrue(all([_ in sds.dims for _ in ['row', 'cell']]))
        print(ds)

        # check url
        self.assertEqual(Path(TEST_FILE), ds.cb.url)

    def test_init_from_xarray(self):
        dst = self.xrdataset
        self.assertIsInstance(dst, xr.Dataset)
        print(dst.cb.time_coverage_start)

    def test_init_from_file(self):
        dst = cerbere.open_dataset('test_xarraydataset.nc')
        self.assertIsInstance(dst, xr.Dataset)

    def test_init_from_dict2(self):
        dst = xr.Dataset(
            coords={
                'time': (('time'), [datetime(2018, 1, 1)],
                         {'units': 'seconds since 2001-01-01 00:00:00'}),
                'lat': (('lat'), np.arange(-80, 80, 1)),
                'lon': (('lon',), np.arange(-180, 180, 1))},
            attrs={'gattr1': 'gattr_val'},
            data_vars={
                'myvar': (('lat', 'lon',), np.ones(shape=(160, 360)))}
        )
        print(dst)
        self.assertIsInstance(dst, xr.Dataset)

    def test_cerberization(self):
        # dataset with naming variants
        dst = xr.Dataset(
            {'myvar': (['lat', 'lon'], np.ones(shape=(160, 360)))},
            coords={
                'time': (['time'], [datetime(2018, 1, 1)], {
                             'units': 'seconds since 2001-01-01 00:00:00'}),
                'lat': (['lat'], np.arange(-80, 80, 1)),
                'lon': (['lon'], np.arange(-180, 180, 1))
            },
            attrs={'gattr1': 'gattr_val'}
        )
        print(dst)
        self.assertIsInstance(dst, xr.Dataset)
        self.assertIsInstance(dst.cb.cfdataset, xr.Dataset)
        # original dataset in untouched
        self.assertIn('lat', dst.coords)
        # new dataset has rename coords
        self.assertIn('lat', dst.cb.cfdataset.coords)
        assert 'lat' in dst.cb.cfdataset.coords
        assert 'lon' in dst.cb.cfdataset.coords
        assert 'time' in dst.cb.cfdataset.coords

        print(dst.cb.cfdataset.lat)

    def test_init_from_xarray_keywords(self):
        dst = xr.Dataset(
            {'myvar': (['lat', 'lon'], np.ones(shape=(160, 360)))},
            coords={
                'time': (['time'], [datetime(2018, 1, 1)], {
                             'units': 'seconds since 2001-01-01 00:00:00'}),
                'lat': (['lat'], np.arange(-80, 80, 1)),
                'lon': (['lon'], np.arange(-180, 180, 1))
            },
            attrs={'gattr1': 'gattr_val'}
        )
        print(dst)
        self.assertIsInstance(dst, xr.Dataset)

    def test_dimension_names(self):
        print('...from xarray.Dataset')
        refdims = list(('lat', 'lon', 'time'))
        refdims.sort()

        dst = self.xrdataset
        self.assertIsInstance(dst.cb.dimnames, tuple)
        dstdims = list(dst.cb.dimnames)
        dstdims.sort()
        self.assertSequenceEqual(dstdims, refdims)

        print('...from file')
        dst = cerbere.open_dataset('test_xarraydataset.nc')
        self.assertIsInstance(dst.cb.dimnames, tuple)
        dstdims = list(dst.cb.dimnames)
        dstdims.sort()
        self.assertSequenceEqual(dstdims, refdims)

    def test_full_dimensions(self):
        print('...from xarray.Dataset')
        dst = self.xrdataset
        print(dst.dims, type(dst.dims))
        self.assertIsInstance(dst.dims, xr.core.utils.Frozen)
        dims = OrderedDict([('lat', 160), ('lon', 360), ('time', 1)])
        self.assertDictEqual(dict(dst.dims), dict(dims))

        print('...from file')
        dst = cerbere.open_dataset('test_xarraydataset.nc')
        self.assertIsInstance(dst.dims, xr.core.utils.Frozen)
        dims = OrderedDict([('lat', 160), ('lon', 360), ('time', 1)])
        self.assertDictEqual(dict(dst.dims), dict(dims))

    def test_var_get_dimensions(self):
        print('...from xarray.Dataset')
        dst = self.xrdataset
        self.assertIsInstance(dst['test_var'].dims, tuple)
        self.assertEqual(dst['test_var'].dims, ('lon', 'lat'))

        print('...from file')
        dst = cerbere.open_dataset('test_xarraydataset.nc')
        self.assertIsInstance(dst['test_var'].dims, tuple)
        self.assertEqual(dst['test_var'].dims, ('lon', 'lat'))

    def test_get_full_dimensions(self):
        print('...from xarray.Dataset')
        dst = self.xrdataset
        print(dst['test_var'].sizes, type(dst['test_var'].sizes))
        self.assertIsInstance(
            dst['test_var'].sizes, xr.core.utils.Frozen)
        self.assertDictEqual(
            OrderedDict(dst['test_var'].sizes),
            OrderedDict([('lon', 360), ('lat', 160)]))

        print('...from file')
        dst = cerbere.open_dataset('test_xarraydataset.nc')
        self.assertIsInstance(
            dst['test_var'].sizes, xr.core.utils.Frozen)
        self.assertDictEqual(
            OrderedDict(dst['test_var'].sizes),
            OrderedDict([('lon', 360), ('lat', 160)]))

    def test_var_get_dimsize(self):
        print('...from xarray.Dataset')
        dst = self.xrdataset
        self.assertEqual(dst.dims['lon'], 360)

        print('...from file')
        dst = cerbere.open_dataset('test_xarraydataset.nc')
        self.assertEqual(dst.dims['lon'], 360)

    def test_var_get_field_attributes(self):
        print('...from xarray.Dataset')
        dst = self.xrdataset
        self.assertIsInstance(dst['test_var'].attrs, dict)
        self.assertEqual(
            dict(dst['test_var'].attrs), {'test_attr': 'test_attr_val'})

        print('...from file')
        dst = cerbere.open_dataset('test_xarraydataset.nc')
        self.assertIsInstance(dst['test_var'].attrs, dict)
        self.assertEqual(dst['test_var'].attrs, {'test_attr': 'test_attr_val'})

    def test_var_get_global_attributes(self):
        attrs = {
            'gattr1': 'gattr1_val', 'gattr2': 'gattr2_val',
        }

        print('...from xarray.Dataset')
        dst = self.xrdataset

        # verify standard attributes are added
        self.assertIsInstance(dst.attrs, dict)
        self.assertDictContainsSubset(attrs, dict(dst.attrs))

        print('...from file')
        dst = cerbere.open_dataset('test_xarraydataset.nc')
        self.assertIsInstance(dst.attrs, dict)
        self.assertDictContainsSubset(attrs, dict(dst.attrs))

    def test_var_get_global_attribute(self):
        print('...from xarray.Dataset')
        dst = self.xrdataset
        self.assertEqual(dst.attrs['gattr1'], 'gattr1_val')

        print('...from file')
        dst = cerbere.open_dataset('test_xarraydataset.nc')
        self.assertEqual(dst.attrs['gattr1'], 'gattr1_val')

    def test_get_time_coverage(self):
        print('Test time coverage')
        print('...from xarray.Dataset')
        dst = self.xrdataset
        print(dst.attrs)
        self.assertEqual(
            dst.cb.time_coverage_start, datetime(2018, 1, 1, 12))
        self.assertEqual(
            dst.cb.time_coverage_end, datetime(2018, 1, 1, 12))

        print('...from file')
        dst = cerbere.open_dataset('test_xarraydataset.nc')
        print(dst.attrs)
        self.assertEqual(
            dst.cb.time_coverage_start, datetime(2018, 1, 1, 12))
        self.assertEqual(
            dst.cb.time_coverage_end, datetime(2018, 1, 1, 12))

    def test_print(self):
        print('Test print')
        dst = self.xrdataset
        print(dst.cb)
        self.assertIsInstance(str(dst), str)

    def test_get_fieldnames(self):
        print('test get_fieldnames()')
        print('...from xarray.Dataset')
        dst = self.xrdataset
        self.assertListEqual(list(dst.data_vars.keys()), ['test_var'])

        print('...from file')
        dst = cerbere.open_dataset('test_xarraydataset.nc')
        self.assertListEqual(list(dst.data_vars.keys()), ['test_var'])

    def test_save_readonly(self):
        dst = self.xrdataset
        with self.assertRaises(OSError):
            dst.cb.save('test_xarraydataset.nc')

    def test_add_field(self):
        dst = self.xrdataset
        new_field = cerbere.new_array(
            data=np.zeros(shape=(360, 160)),
            dims=('lon', 'lat',),
            name='newvar')
        dst['newvar'] = new_field
        self.assertTrue('newvar' in dst.data_vars)
        self.assertSequenceEqual(dst['newvar'].shape, (360, 160))

    def test_extract(self):
        dst = self.xrdataset
        subset = dst.copy(deep=True)
        self.assertTrue('test_var' in subset.data_vars)
        self.assertSequenceEqual(
            subset['test_var'].shape, (360, 160)
        )
        self.assertIsNot(
            subset['test_var'],
            dst['test_var']
        )

    def test_extract_as_view(self):
        dst = self.xrdataset
        subset = dst.copy(deep=False)
        self.assertTrue('test_var' in subset.data_vars)
        self.assertSequenceEqual(
            subset['test_var'].shape, (360, 160)
        )
        self.assertIsNot(subset['test_var'], dst['test_var'])
        self.assertIs(subset['test_var'].values, dst['test_var'].values)

    def test_extract_rename(self):
        dst = self.xrdataset
        prefix = 'new'
        subset = dst.copy(deep=False).rename_vars(
            {v: f'{prefix}_{v}' for v in dst.data_vars})

        self.assertTrue('new_test_var' in subset.data_vars)
        self.assertSequenceEqual(
            subset['new_test_var'].shape, (360, 160)
        )
        self.assertIsNot(subset['new_test_var'], dst['test_var'])
        self.assertIs(subset['new_test_var'].values, dst['test_var'].values)

    def test_extract_subset(self):
        dst = self.xrdataset
        subset = dst.cb.isel(
            {'lat': slice(10, 20), 'lon': slice(10, 15)}).copy(deep=True)
        self.assertTrue('test_var' in subset.data_vars)
        self.assertTrue('time' in subset.coords)
        self.assertSequenceEqual(
            subset['test_var'].shape, (5, 10)
        )
        self.assertIsNot(subset['test_var'].values, dst['test_var'].values)

    def test_extract_subset_padding(self):
        dst = self.xrdataset
        subset = dst.cb.isel(
            {'lat': slice(10, 20), 'lon': slice(355, 365)},
            padding=True).copy(deep=True)

        self.assertTrue('test_var' in subset.data_vars)
        self.assertTrue('time' in subset.coords)

        self.assertSequenceEqual(
            subset['test_var'].shape, (10, 10)
        )
        self.assertIsNot(subset['test_var'].values, dst['test_var'].values)

        self.assertEqual(subset['test_var'].to_masked_array().count(), 50)
        self.assertEqual(subset['test_var'].size, 100)

    def test_save_with_profile(self):
        dst = self.xrdataset
        if os.path.exists('test_profile.nc'):
            os.remove('test_profile.nc')
        dst.cb.save(
            path='test_profile.nc',
            profile='default_saving_profile.yaml'
        )

    def test_opening_file_not_existing(self):
        def open_dummy():
            return cerbere.open_dataset('file_not_existing.nc')
        self.assertRaises(IOError, open_dummy)

    def test_clip_values(self):
        dst = self.xrdataset
        subset = dst['test_var'].cb.clip(
            Polygon([(-80, -40), (80, -40), (80, 20), (-80, 20)]))
        self.assertEqual(len(subset), 1)
        self.assertSequenceEqual(subset[0].shape, (161, 61))

    def test_clip(self):
        dst = self.xrdataset
        subset = dst.cb.clip(
            Polygon([(-80, -40), (80, -40), (80, 20), (-80, 20)])
        )
        print(subset)
        self.assertEqual(len(subset), 1)
        self.assertDictEqual(
            dict(subset[0].sizes), {'lon': 161, 'lat': 61, 'time': 1}
        )

    def test_axis(self):
        print('...from xarray.Dataset')
        dst = self.xrdataset
        dst.cb.set_axis('lat', 'Y')
        self.assertEqual(dst.cb.Y.name, 'lat')
        self.assertEqual(dst.cb.X.name, 'lon')

        print('...from file')
        dst = cerbere.open_dataset('test_xarraydataset.nc')
        dst.cb.set_axis('lat', 'Y')
        self.assertEqual(dst.cb.Y.name, 'lat')
