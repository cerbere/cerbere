"""
   dimensions:
      profile = 30 ;
      z = 42 ;
      name_strlen = 23 ;

   variables:
      float lon ;
          lon:standard_name = "longitude";
          lon:long_name = "station longitude";
          lon:units = "degrees_east";
      float lat ;
          lat:standard_name = "latitude";
          lat:long_name = "station latitude" ;
          lat:units = "degrees_north" ;
      char station_name(name_strlen) ;
          station_name:cf_role = "timeseries_id" ;
          station_name:long_name = "station name" ;
      int station_info;
          station_info:long_name = "some kind of station info" ;

      float alt(profile , z) ;
          alt:standard_name = “altitude”;
          alt:long_name = "height above mean sea level" ;
          alt:units = "km" ;
          alt:axis = "Z" ;
          alt:positive = "up" ;

      double time(profile ) ;
          time:standard_name = "time";
          time:long_name = "time of measurement" ;
          time:units = "days since 1970-01-01 00:00:00" ;
          time:missing_value = -999.9;

      float pressure(profile , z) ;
          pressure:standard_name = "air_pressure" ;
          pressure:long_name = "pressure level" ;
          pressure:units = "hPa" ;
          pressure:coordinates = "time lon lat alt station_name" ;

      float temperature(profile , z) ;
          temperature:standard_name = "surface_temperature" ;
          temperature:long_name = "skin temperature" ;
          temperature:units = "Celsius" ;
          temperature:coordinates = "time lon lat alt station_name" ;

      float humidity(profile , z) ;
          humidity:standard_name = "relative_humidity" ;
          humidity:long_name = "relative humidity" ;
          humidity:units = "%" ;
          humidity:coordinates = "time lon lat alt station_name" ;

   attributes:
    :featureType = "timeSeriesProfile";
"""
from cerbere.feature.ctimeseriesprofile import TimeSeriesProfile
from cerbere.feature.pytest_cfeature import *


@pytest.fixture(scope='module')
def feature_class():
    return TimeSeriesProfile


@pytest.fixture(scope='module')
def feature_instance():
    # creates a test xarray object
    lon = xr.DataArray(data=10, dims=[])
    lat = xr.DataArray(data=5, dims=[])
    z_data = np.repeat(np.arange(0, 1000, 10), 15).reshape(100, 15).T
    z = xr.DataArray(
        data=z_data, dims=['profile', 'z'], attrs=dict(
            axis='Z',
            standard_name='altitude',
            long_name="height above mean sea level",
            units="km",
            positive="up"))
    times = np.full((15,), np.datetime64('2018-01-01 00:00:00', 'ns'))
    times += np.array([np.timedelta64(_) for _ in np.arange(0, 15)])
    times = xr.DataArray(times, dims='profile')
    station = xr.DataArray(
        data='station #1', dims=(), attrs=dict(
            cf_role="timeseries_id",
            long_name="station name"))
    var = xr.DataArray(
        data=np.ones(shape=(15, 100)),
        dims=['profile', 'z'],
        attrs={'myattr': 'test_attr_val'}
    )
    xrdataset = xr.Dataset(
        coords={'lat': lat, 'lon': lon, 'time': times, 'alt': z,
                'station': station},
        data_vars={'my_var': var},
        attrs={'gattr1': 'gattr1_val', 'gattr2': 'gattr2_val'}
    )
    print(
        xrdataset
    )
    return TimeSeriesProfile(xrdataset)


@pytest.fixture(scope='module')
def feature_dimnames():
    return 'profile', 'z',


@pytest.fixture(scope='module')
def feature_dimsizes():
    return 15, 100,


@pytest.fixture(scope='module')
def content_class():
    return


def test_axis_coords(feature_instance):
    assert feature_instance.ds.cb.cf_axis_coords == {'Z': 'alt'}


def test_axis_dims(feature_instance):
    assert feature_instance.ds.cb.cf_axis_dims == {'Z': 'z'}


def test_instance_dims(feature_instance):
    assert feature_instance.ds.cb.cf_instance_dims == ('profile',)
