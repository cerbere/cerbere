"""
Abstract testing class for checking a new mapper
"""
from __future__ import absolute_import, division, print_function

import datetime
import os
from pathlib import Path

import numpy

import cerbere
from cerbere.dataset.dataset import Dataset
from cerbere.dataset.field import Field
from cerbere.dataset.ncdataset import NCDataset

TEST_SUBSET = 'test_subset.nc'


class Checker():
    """Checker for dataset classes

    You must override in an inherited class the following class methods:
        * test_file : return the input file to be used in this test
        * download_url : return the url to test data repository
        * mapper : return the mapper class name
        * datamodel : return the related datamodel class name
    """

    def test_file(cls):
        """get the test file to be used in the tests

        This class must be overriden by all inheriting classes.
        """
        raise NotImplementedError

    @classmethod
    def dataset(cls):
        """Return the dataset class name"""
        raise NotImplementedError

    @classmethod
    def feature(cls):
        """Return the related feature class name"""
        raise NotImplementedError

    @classmethod
    def download_url(cls):
        """Return the URL of the data test repository where to get the test
        files
        """
        raise NotImplementedError

    def setUp(self):
        """ Setting up for the test """
        # get tes initialization info from inherited classes
        testfile = self.test_file()
        mydataset = self.dataset()
        myfeature = self.feature()

        # search test file
        if 'CERBERE_TESTDATA_REPOSITORY' not in os.environ:
            raise EnvironmentError(
                'CERBERE_TESTDATA_REPOSITORY environment variable is not set')
        rootpath = os.environ['CERBERE_TESTDATA_REPOSITORY']
        datasetdirname = mydataset.split('.')[-1]
        testfilepath = os.path.join(rootpath,
                                    datasetdirname,
                                    testfile)
        if not os.path.exists(testfilepath):
            print('Test file {} was not found. Please download it from {} and '
                  'place in {} folder'.format(
                      testfilepath,
                      self.download_url(),
                      os.path.join(rootpath, datasetdirname))
                  )
            raise IOError(
                'Test file {} was not found. Please download it from {} and '
                'place in {} folder'.format(
                    testfilepath,
                    self.download_url(),
                    os.path.join(rootpath, datasetdirname))
            )
        self.testfile = testfilepath

        self.datasetclass = self.load_dataset(mydataset)
        self.featureclass = self.load_feature(myfeature)

    def tearDown(self):
        """Cleaning up after the test"""

    def load_dataset(self, dst):
        """Import dataset class"""
        return cerbere.helpers._datasets[dst].load()

    def load_feature(self, feat):
        """Import feature class"""
        return cerbere.helpers._features[feat].load()

    def open_file(self):
        """Open file and create dataset object"""
        print('...instantiate class with {}'.format(self.testfile))
        print(self.datasetclass(self.testfile, mode='r'))
        return self.datasetclass(self.testfile, mode='r')

    def test_load_dataset(self):
        """Checker dataset import"""
        self.assertTrue(
            issubclass(self.datasetclass, Dataset))

    def test_open_file_with_dataset(self):
        """Test dataset instantiation with an input file"""
        datasetobj = self.open_file()
        datasetobj.close()
        self.assertIsInstance(datasetobj, self.datasetclass)

    def test_open_file_with_generic_open(self):
        """Test dataset instantiation with an input file, using the generic
        opening methode
        """
        print('...instantiate class with generic opening of {}'
              .format(self.testfile))
        datasetobj = cerbere.open_dataset(self.testfile, self.dataset())

        self.assertIsInstance(datasetobj, self.datasetclass)
        datasetobj.close()

    def test_open_file_with_path(self):
        """Open file fiven as a Path object and create dataset object"""
        print('...instantiate class with {}'.format(self.testfile))
        datasetobj = self.datasetclass(Path(self.testfile), mode='r')
        self.assertIsInstance(datasetobj, self.datasetclass)

    def test_get_list_of_fields(self):
        """Test reading the list of fields"""
        datasetobj = self.open_file()
        fields = datasetobj.fieldnames
        for fieldname in fields:
            print(fieldname)
            self.assertIsInstance(fieldname, str)
            self.assertNotEqual(fieldname, '')
        datasetobj.close()

    def test_read_fields(self):
        """Test reading and displaying field properties"""
        datasetobj = self.open_file()
        fields = datasetobj.fieldnames
        for fieldname in fields:
            print('...reading ', fieldname)
            field = datasetobj.get_field(fieldname)
            self.assertIsInstance(field, Field)
            print(field)

    def test_read_global_dimensions(self):
        """Test reading and displaying global dimensions"""
        datasetobj = self.open_file()
        print('...reading global dimensions')
        dims = datasetobj.dims
        self.assertNotEqual(dims, None)
        self.assertGreater(len(dims), 0)

        for dim in dims:
            dimsize = datasetobj.get_dimsize(dim)
            self.assertIsInstance(dim, str)
            self.assertIsInstance(dimsize, int)
            print('.....', dim, dimsize)

    def test_read_lat(self):
        """Test reading latitude and checking validity"""
        datasetobj = self.open_file()
        print('fillvalue', datasetobj.get_geocoord('lat').fill_value)
        data = numpy.ma.array(datasetobj.get_values('lat'))
        print('...test there are no fillvalues')
        self.assertEqual(data.size, data.count(),
                         'They are fillvalues in lat field')
        print('...test values are in valid range')
        self.assertTrue(-90 <= data.min() <= 90,
                        'Invalid min latitude %s' % data.min())
        self.assertTrue(-90 <= data.max() <= 90,
                        'Invalid max latitude %s' % data.max())
        datasetobj.close()

    def test_read_lon(self):
        """Test reading longitude and checking validity"""
        datasetobj = self.open_file()
        data = numpy.ma.array(datasetobj.get_lon())
        print('...test there are no fillvalues')
        self.assertEqual(data.size, data.count(),
                         'They are fillvalues in lon field')
        print('...test values are in valid range')
        self.assertTrue(-180 <= data.min() <= 180,
                        'Invalid min longitude %s' % data.min())
        self.assertTrue(-180 <= data.max() <= 180,
                        'Invalid max longitude %s' % data.max())
        datasetobj.close()

    def test_read_time(self):
        """Test reading time and checking validity"""
        def dt64todatetime(dt64):
            unix_epoch = numpy.datetime64(0, 's')
            one_second = numpy.timedelta64(1, 's')
            seconds_since_epoch = (dt64 - unix_epoch) / one_second
            return datetime.datetime.utcfromtimestamp(seconds_since_epoch)

        mapperobj = self.open_file()
        data = numpy.ma.array(mapperobj.get_values('time'))
        print('...test there are no fillvalues')
        self.assertEqual(data.size, data.count(),
                         'They are fillvalues in time field')

        self.assertIsInstance(data.min(), (numpy.datetime64, numpy.timedelta64))

        print('...test values are in valid range')

        time_min = dt64todatetime(data.min())
        print(data.min(), data.min().astype(datetime.datetime))
        print('...min time: ', time_min, type(time_min), data.min(), type(data.min()))
        self.assertGreater(time_min, datetime.datetime(1970, 1, 1))

        time_max = dt64todatetime(data.max())
        print('...max time: ', time_max)
        self.assertLess(time_max, datetime.datetime(2050, 1, 1))

        mapperobj.close()

    def test_read_global_attributes(self):
        """Test reading the global attributes"""
        datasetobj = self.open_file()
        attributes = datasetobj.attrs
        for attr in attributes:
            print(attr, datasetobj.get_attr(attr))
            msg = 'Unkown attribute type {} for {}'.format(
                type(datasetobj.get_attr(attr)),
                attr)
            self.assertIsInstance(
                datasetobj.get_attr(attr),
                (str, int, datetime.datetime, numpy.int32, numpy.uint32,
                 numpy.int16, numpy.float32, numpy.float64, list),
                msg)
        datasetobj.close()

    def test_read_time_coverage(self):
        """Test reading the coverage start end end time"""
        datasetobj = self.open_file()

        start = datasetobj.time_coverage_start
        print('...start time : ', start)
        self.assertIsInstance(start, datetime.datetime)

        end = datasetobj.time_coverage_end
        print('...end time : ', end)
        self.assertIsInstance(end, datetime.datetime)
        datasetobj.close()

    def _create_feature(self, *args, **kwargs):
        return self.featureclass(*args, **kwargs)

    def test_load_dataset_content_into_feature(self):
        """Test loading the content of a file into a feature structure"""
        datasetobj = self.datasetclass(self.testfile)
        print(datasetobj)
        feature = self._create_feature(datasetobj)
        self.assertIsInstance(feature, self.featureclass)
        datasetobj.close()

    def __extract_subset(self):
        """Extract a subset from a datamodel structure loaded with a file"""
        datasetobj = self.datasetclass(self.testfile)
        featureobj = self._create_feature(datasetobj)

        if featureobj.__class__.__name__ in ['Swath', 'Image']:
            cells = featureobj.cf_dims['cell']
            rows = featureobj.cf_dims['row']
            width = min(min(rows // 2, cells // 2), 5)
            r0, r1 = rows // 2 - width, rows // 2 + width
            c0, c1 = cells // 2 - width, cells // 2 + width
            slices = {'row': slice(r0, r1, 1), 'cell': slice(c0, c1, 1)}
            print('Subset: ', slices)
            subset = featureobj.extract(index=slices)
            subset.attrs['slices'] = str(slices)

        elif featureobj.__class__.__name__ in ['Grid', 'GridTimeSeries']:
            ni = featureobj.cf_dims['x']
            nj = featureobj.cf_dims['y']
            width = min(min(nj // 2, ni // 2), 5)
            j0, j1 = nj // 2 - width, nj // 2 + width
            i0, i1 = ni // 2 - width, ni // 2 + width
            subset = featureobj.extract(index={'y': slice(j0, j1, 1),
                                                    'x': slice(i0, i1, 1)})

        elif featureobj.__class__.__name__ in [
                'CylindricalGrid', 'CylindricalGridTimeSeries'
        ]:
            ni = featureobj.cf_dims['lon']
            nj = featureobj.cf_dims['lat']
            width = min(min(nj // 2, ni // 2), 5)
            j0, j1 = nj // 2 - width, nj // 2 + width
            i0, i1 = ni // 2 - width, ni // 2 + width
            subset = featureobj.extract(index={'lat': slice(j0, j1, 1),
                                                'lon': slice(i0, i1, 1)})

        elif featureobj.__class__.__name__ in ['Trajectory']:
            times = featureobj.cf_dims['time']
            width = min(times // 2, 5)
            r0, r1 = times // 2 - width, times // 2 + width
            print('Subset ')
            print('time : ', r0, r1)
            subset = featureobj.extract(index={'time': slice(r0, r1, 1)})

        elif featureobj.__class__.__name__ in ['TrajectoryProfile']:
            times = featureobj.cf_dims['profile']
            n_levels = featureobj.cf_dims['z']
            width = min(times // 2, 5)
            r0, r1 = times // 2 - width, times // 2 + width
            subset = featureobj.extract(
                index={'profile': slice(r0, r1, 1),
                       'z': slice(0, n_levels // 2)})

        else:
            raise NotImplementedError

        self.assertIsInstance(subset, featureobj.__class__)
        return subset

    def test_extract_and_save_a_subset(self):
        """Test extracting a subset from a file with datamodel and saving it"""
        # extract
        subset = self.__extract_subset()

        # test saving the subset in netCDF format
        fname = TEST_SUBSET
        if os.path.exists(fname):
            os.remove(fname)

        subsetfile = NCDataset(fname, mode='w')
        subset.save(subsetfile)
        subsetfile.close()

        # try reading the subset to check it si correctly formatted
        subsetfile = NCDataset(fname)
        featureobj = self._create_feature(subsetfile)
        self.assertIsInstance(featureobj, self.featureclass)
        subsetfile.close()
