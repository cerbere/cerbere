"""
Test class for cerbere GHRSST files mapper
"""
from __future__ import absolute_import, division, print_function

import unittest

from .checker import Checker


class GHRSSTNCDatasetL2PChecker(Checker, unittest.TestCase):
    """Test class for GHRSSTNCFile swath files"""

    def __init__(self, methodName='runTest'):
        super(GHRSSTNCDatasetL2PChecker, self).__init__(methodName)

    @classmethod
    def dataset(cls):
        """Return the mapper class name"""
        return 'GHRSSTNCDataset'

    @classmethod
    def feature(cls):
        """Return the related datamodel class name"""
        return 'Swath'

    @classmethod
    def test_file(cls):
        """Return the name of the test file for this test"""
        return '20190531130355-REMSS-L2P_GHRSST-SSTsubskin-AMSR2-L2B_v08_r37425-v02.0-fv01.0.nc'

    @classmethod
    def download_url(cls):
        """Return the URL of the data test repository where to get the test
        files
        """
        return 'ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/'

    def test_dim_swath(self):
        ncf = self.datasetclass(self.testfile)
        swath = self.featureclass(ncf)
        self.assertEquals(tuple(swath.get_geocoord('lat').dims.keys()), ('row', 'cell',))
        print('DMS SST ', swath.get_field_dims('sea_surface_temperature'))
        self.assertEquals(swath.get_field_dims('sea_surface_temperature'), ('row', 'cell',))
