from cerbere.feature.cswath import Swath

from cerbere.feature.pytest_cfeature import *


@pytest.fixture(scope='module')
def feature_class():
    return Swath


@pytest.fixture(scope='module')
def feature_instance():
    # creates a test xarray object
    lon = np.arange(0, 10)
    lat = np.arange(20, 30)
    lon, lat = np.meshgrid(lon, lat)
    times = np.arange(100).reshape((10, 10,)).astype('timedelta64[s]') + \
            np.datetime64('2018-01-01', 'ns')

    lon = xr.DataArray(lon, dims=['row', 'cell'])
    lat = xr.DataArray(data=lat, dims=['row', 'cell'])
    time = xr.DataArray(data=times, dims=['row', 'cell'])
    var = xr.DataArray(data=np.ones(shape=(10, 10)), dims=['row', 'cell'])
    var.attrs['my_var'] = 'test_attr_val'
    xrdataset = xr.Dataset(
        coords={'lat': lat, 'lon': lon, 'time': time},
        data_vars={'my_var': var},
        attrs={'gattr1': 'gattr1_val', 'gattr2': 'gattr2_val'}
    )

    return Swath(xrdataset)


@pytest.fixture(scope='module')
def feature_dimnames():
    return 'row', 'cell', 'time',


@pytest.fixture(scope='module')
def feature_dimsizes():
    return 10, 10, 1,


@pytest.fixture(scope='module')
def content_class():
    return
