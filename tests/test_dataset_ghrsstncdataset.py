"""
Test class for cerbere GHRSST files mapper
"""
from __future__ import absolute_import, division, print_function

import os
import unittest
from datetime import datetime

import numpy as np

from cerbere.dataset.ghrsstncdataset import GHRSSTNCDataset
from cerbere.feature.grid import CylindricalGrid

from .checker import Checker


class GHRSSTNCDatasetL2PChecker(Checker, unittest.TestCase):
    """Test class for GHRSSTNCFile swath files"""

    def __init__(self, methodName='runTest'):
        super(GHRSSTNCDatasetL2PChecker, self).__init__(methodName)

    @classmethod
    def dataset(cls):
        """Return the mapper class name"""
        return 'GHRSSTNCDataset'

    @classmethod
    def feature(cls):
        """Return the related datamodel class name"""
        return 'Swath'

    @classmethod
    def test_file(cls):
        """Return the name of the test file for this test"""
        return '20190719000110-MAR-L2P_GHRSST-SSTskin-SLSTRA-20190719021531-v02.0-fv01.0.nc'

    @classmethod
    def download_url(cls):
        """Return the URL of the data test repository where to get the test
        files
        """
        return 'ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/'

    def test_dim_swath(self):
        ncf = self.datasetclass(self.testfile)
        swath = self.featureclass(ncf)
        self.assertEquals(swath.get_geocoord('lat').dimnames, ('row', 'cell',))
        self.assertEquals(
            swath.get_field('sea_surface_temperature').dimnames,
            ('row', 'cell',)
        )

    def test_time_padding(self):
        ncf = self.datasetclass(self.testfile)
        subset = ncf.extract(
                index={'cell':slice(1354, 1500, None)},
                padding=True).get_datetimes()
        self.assertTrue(None not in subset)
        self.assertTrue(subset.size != subset.count())

    def test_ghrsst_cast_l4(self):
        grid = CylindricalGrid(
            {'time': {'dims': ('time'), 'data': [datetime(2018, 1, 1)]},
             'lat': {'dims': ('lat'), 'data': np.arange(-80, 80, 1)},
             'lon': {'dims': ('lon',), 'data': np.arange(-180, 180, 1)},
             'sea_surface_temperature': {'dims': ('lat', 'lon',),
                       'data': np.ones(shape=(160, 360))}
             }
        )
        grid.attrs['id'] = 'my_collection_id_L4'
        fname = 'ghrsst_file.nc'
        if os.path.exists(fname):
            os.remove(fname)
        ghrsstf = GHRSSTNCDataset(fname, mode='w')

        grid.save(ghrsstf, profile='ghrsst_saving_profile.yaml')

    def test_ghrsst_cast_l3(self):
        times = np.full(
            (160, 360),
            np.datetime64('2018-01-01'),
            dtype='datetime64[D]',
        )
        grid = CylindricalGrid(
            {'time': {'dims': ('lat', 'lon',), 'data': times},
             'lat': {'dims': ('lat'), 'data': np.arange(-80, 80, 1)},
             'lon': {'dims': ('lon',), 'data': np.arange(-180, 180, 1)},
             'sea_surface_temperature': {'dims': ('lat', 'lon',),
                       'data': np.ones(shape=(160, 360))}
             }
        )
        grid.attrs['id'] = 'my_collection_id_L3C'
        fname = 'ghrsst_file_l3.nc'
        if os.path.exists(fname):
            os.remove(fname)
        ghrsstf = GHRSSTNCDataset(fname, mode='w')

        grid.save(ghrsstf, profile='ghrsst_saving_profile.yaml')

    def test_ghrsst_cast_l3_nc3(self):
        times = np.full(
            (160, 360),
            np.datetime64('2018-01-01'),
            dtype='datetime64[D]',
        )
        grid = CylindricalGrid(
            {'time': {'dims': ('lat', 'lon',), 'data': times},
             'lat': {'dims': ('lat'), 'data': np.arange(-80, 80, 1)},
             'lon': {'dims': ('lon',), 'data': np.arange(-180, 180, 1)},
             'sea_surface_temperature': {'dims': ('lat', 'lon',),
                       'data': np.ones(shape=(160, 360))}
             }
        )
        grid.attrs['id'] = 'my_collection_id_L3C'
        fname = 'ghrsst_file_l3_nc3.nc'
        if os.path.exists(fname):
            os.remove(fname)
        ghrsstf = GHRSSTNCDataset(fname, format='NETCDF3_CLASSIC', mode='w')

        grid.save(ghrsstf, profile='ghrsst_saving_profile.yaml')
