import numpy as np
import xarray as xr

from cerbere.feature.imdcollection import IMDCollection
from cerbere.feature.profile import Profile

from .test_feature import TestFeature


class TestIMDCollectionProfileFeature(TestFeature):
    """Test class for incomplete multidimensional collection of profile
    features"""

    def __init__(self, methodName='runTest'):
        super().__init__(methodName)

    def get_feature_class(self):
        return IMDCollection

    def get_feature(self, *args, **kwargs):
        return self.get_feature_class()(
            *args, feature_class=Profile, **kwargs
        )

    def define_base_feature(self):
        # creates a test xarray object
        lon = xr.DataArray(data=[100., 100], dims=['profile'])
        lat = xr.DataArray(data=[50., 50], dims=['profile'])
        z = xr.DataArray(
            data=np.stack([np.arange(0, 1000, 10), np.arange(0, 100, 1)]),
            dims=['profile', 'z']
        )
        time = xr.DataArray(
            [np.datetime64('2018-01-01 00:00:00'),
             np.datetime64('2018-01-01 00:00:00')],
            dims=['profile']
        )
        var = xr.DataArray(
            data=np.ones(shape=(2, 100,)),
            dims=['profile', 'z'],
            attrs={'myattr': 'test_attr_val'}
        )
        xrdataset = xr.Dataset(
            coords={'lat': lat, 'lon': lon, 'time': time, 'PRES': z},
            data_vars={'myvar': var},
            attrs={'gattr1': 'gattr1_val', 'gattr2': 'gattr2_val'}
        )

        return IMDCollection(xrdataset, feature_class=Profile)

    def get_feature_dimnames(self):
        return 'profile', 'z',

    def get_feature_dimsizes(self):
        return 2, 100,
