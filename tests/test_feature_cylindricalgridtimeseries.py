from datetime import datetime

import numpy as np
import xarray as xr

from cerbere.feature.gridtimeseries import CylindricalGridTimeSeries
from cerbere.feature.trajectory import Trajectory

from .test_feature import TestFeature


class TestCylindricalGridFeature(TestFeature):
    """Test class for CylindricalGridFeature"""

    def get_feature_class(self):
        return CylindricalGridTimeSeries

    def define_base_feature(self):
        # creates a test xarray object
        lon = xr.DataArray(data=np.arange(-180, 180, 1), dims=['lon'])
        lat = xr.DataArray(data=np.arange(-80, 80, 1), dims=['lat'])
        time = xr.DataArray([
            datetime(2018, 1, 1),
            datetime(2018, 1, 2),
            datetime(2018, 1, 3),
            datetime(2018, 1, 4),
            datetime(2018, 1, 5)
        ], dims='time')
        var = xr.DataArray(
            data=np.ones(shape=(5, 160, 360)),
            dims=['time', 'lat', 'lon'],
            attrs={'myattr': 'test_attr_val'}
        )
        xrdataset = xr.Dataset(
            coords={'lat': lat, 'lon': lon, 'time': time},
            data_vars={'myvar': var},
            attrs={'gattr1': 'gattr1_val', 'gattr2': 'gattr2_val'}
        )
        return CylindricalGridTimeSeries(xrdataset)

    def get_feature_dimnames(self):
        return ('time', 'lat', 'lon',)

    def get_feature_dimsizes(self):
        return (5, 160, 360)

    def test_create_feature_with_incorrect_geodim_order(self):
        basefeat = self.define_base_feature()
        feat = self.get_feature_class()(
            {
                'lat': {'data': basefeat.get_lat(), 'dims': ('lat',)},
                'lon': {'data': basefeat.get_lon(), 'dims': ('lon',)},
                'time': {
                    'data': basefeat.get_times(), 'dims': ('time',)
                },
                'myvar': {
                    'data': np.ones(shape=(5, 360, 160)),
                    'dims': ('time', 'lon', 'lat')
                }
            }
        )
        self.assertEqual(
            feat.get_field_dims('myvar'), ('time', 'lat', 'lon')
        )
        print('Feature from: test_create_feature_with_incorrect_geodim_order')
        print(feat)

    def test_create_feature_from_dict_datetime_1d(self):
        basefeat = self.define_base_feature()
        feat = self.get_feature_class()(
            {
                'lat': {'data': basefeat.get_lat(), 'dims': ('lat',)},
                'lon': {'data': basefeat.get_lon(), 'dims': ('lon',)},
                'time': {
                    'data': basefeat.get_times(), 'dims': ('time',)
                },
                'myvar': {
                    'data': basefeat.get_values('myvar'),
                    'dims': ('time', 'lat', 'lon')
                }
            }
        )
        self.assertIsInstance(feat, self.get_feature_class())
        self.assertEqual(len(feat.get_field_dims('time')), 1)
        print('Feature from: test_create_feature_from_dict_datetime_1d')
        print(feat)

    def test_create_feature_from_dict_datetime64_1d(self):
        basefeat = self.define_base_feature()
        feat = self.get_feature_class()(
            {
                'lat': {'data': basefeat.get_lat(), 'dims': ('lat',)},
                'lon': {'data': basefeat.get_lon(), 'dims': ('lon',)},
                'time': {
                    'data': basefeat.get_times(),
                    'dims': ('time',)
                },
                'myvar': {
                    'data': basefeat.get_values('myvar'),
                    'dims': ('time', 'lat', 'lon')
                }
            }
        )

        self.assertIsInstance(feat, self.get_feature_class())
        self.assertEqual(len(feat.get_field_dims('time')), 1)
        print('Feature from: test_create_feature_from_dict_datetime64_1d')
        print(feat)

    def test_expanded_latlon(self):
        basefeat = self.define_base_feature()
        res = basefeat.get_values(
            'lat',
            index={'lat': slice(10, 15), 'lon': slice(50, 55)},
            expand=False
        )
        self.assertEqual(res.shape, (5,))
        self.assertTrue(np.equal(res, np.arange(-70, -65)).all())

        res = basefeat.get_values(
            'lat',
            index={'lat': 10, 'lon': 50},
            expand=True
        )
        self.assertEqual(res, -70)

        res = basefeat.get_values(
            'lon',
            index={'lat': 10, 'lon': 50},
            expand=True
        )
        self.assertEqual(res, -130)

        res = basefeat.get_values(
            'lat',
            index={'lat': slice(10, 15), 'lon': slice(50, 53)},
            expand=True
        )
        self.assertEqual(res.shape, (5, 3,))
        self.assertTrue(np.equal(res[:, 0], np.arange(-70, -65)).all())
        self.assertTrue(np.equal(res[0, :], np.ones((3,)) * -70).all())

        res = basefeat.get_values(
            'lon',
            index={'lat': slice(10, 15), 'lon': slice(50, 53)},
            expand=True
        )
        self.assertEqual(res.shape, (5, 3,))
        self.assertTrue(np.equal(res[0, :], np.arange(-130, -127)).all())
        self.assertTrue(np.equal(res[:, 0], np.ones((5,)) * -130).all())

        res = basefeat.get_values(
            'lat',
            expand=True
        )
        print('result: test_expanded_latlon ', res)

    def test_expanded_latlon(self):
        basefeat = self.define_base_feature()

        res = basefeat.get_values(
            'time',
            index={'lat': slice(10, 15), 'lon': slice(50, 53)},
            expand=True
        )
        self.assertEqual(res.shape, (5, 5, 3,))

        res = basefeat.get_values(
            'time',
            expand=True
        )
        print('result: test_expanded_time ', res)

    def test_trajectory(self):
        basefeat = self.define_base_feature()

        # create a trajectory
        lon = xr.DataArray(data=np.arange(7), dims=['time'])
        lat = xr.DataArray(data=np.arange(7), dims=['time'])
        time = xr.DataArray([
            datetime(2018, 1, 1),
            datetime(2018, 1, 2),
            datetime(2018, 1, 3),
            datetime(2018, 1, 3, 12),
            datetime(2018, 1, 4),
            datetime(2018, 1, 5),
            datetime(2018, 1, 6)
        ], dims='time')
        xrdataset = xr.Dataset(
            coords={'lat': lat, 'lon': lon, 'time': time},
        )
        traj = Trajectory(xrdataset)

        # extract a collocated trajectory from the feature
        extracted_traj = basefeat.trajectory(traj)
        print(extracted_traj)
