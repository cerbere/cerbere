from cerbere.feature.cpoint import Point

from cerbere.feature.pytest_cfeature import *

@pytest.fixture(scope='module')
def feature_class():
    return Point


@pytest.fixture(scope='module')
def feature_instance():
    # creates a test xarray object
    lon = xr.DataArray(data=10, dims=[])
    lat = xr.DataArray(data=20, dims=[])
    z = xr.DataArray(data=5, dims=[])
    time = xr.DataArray(np.datetime64('2018-01-01 00:00:00', 'ns'), dims=[])

    var = xr.DataArray(
        data=1,
        dims=[],
        attrs={'myattr': 'test_attr_val'}
    )
    xrdataset = xr.Dataset(
        coords={'lat': lat, 'lon': lon, 'time': time, 'z': z},
        data_vars={'my_var': var},
        attrs={'gattr1': 'gattr1_val', 'gattr2': 'gattr2_val'}
    )
    return Point(xrdataset)


@pytest.fixture(scope='module')
def feature_dimnames():
    return 'obs',


@pytest.fixture(scope='module')
def feature_dimsizes():
    return 100,


@pytest.fixture(scope='module')
def content_class():
    return
