from cerbere.feature.comdcollection import OMDCollection
from cerbere.feature.cprofile import Profile

from cerbere.feature.pytest_cfeature import *


@pytest.fixture(scope='module')
def feature_class():
    return OMDCollection


@pytest.fixture(scope='module')
def feature_instance():
    # creates a test xarray object
    lon = xr.DataArray(data=[100., 100], dims=['profile'])
    lat = xr.DataArray(data=[50., 50], dims=['profile'])
    z = xr.DataArray(data=[0., 10., 20., 50], dims=['z'], attrs=dict(axis='Z'))

    time = xr.DataArray(
        [np.datetime64('2018-01-01 00:00:00', 'ns'),
         np.datetime64('2018-01-01 00:00:00', 'ns')],
        dims=['profile']
    )
    var = xr.DataArray(
        data=np.ones(shape=(2, 4)),
        dims=['profile', 'z'],
        attrs={'myattr': 'test_attr_val'}
    )
    xrdataset = xr.Dataset(
        coords={'lat': lat, 'lon': lon, 'time': time, 'z': z},
        data_vars={'my_var': var},
        attrs={'gattr1': 'gattr1_val', 'gattr2': 'gattr2_val'}
    )

    return OMDCollection(xrdataset, instance_class='Profile')


@pytest.fixture(scope='module')
def feature_dims():
    return {'profile': 2, 'z': 4}


@pytest.fixture(scope='module')
def feature_dimnames():
    return 'profile', 'z',


@pytest.fixture(scope='module')
def feature_dimsizes():
    return 2, 4,


@pytest.fixture(scope='module')
def instance_class():
    return Profile


def test_iteration(feature_instance):
    for item in feature_instance:
        assert isinstance(item, Profile)
