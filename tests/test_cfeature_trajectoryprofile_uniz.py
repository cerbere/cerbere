from cerbere.feature.ctrajectoryprofile import TrajectoryProfile
from cerbere.feature.comdcollection import OMDCollection

from cerbere.feature.pytest_cfeature import *


@pytest.fixture(scope='module')
def feature_class():
    return TrajectoryProfile


@pytest.fixture(scope='module')
def feature_instance():
    # creates a test xarray object
    lon = xr.DataArray(data=np.arange(10, 11.5, 0.1), dims=['profile'])
    lat = xr.DataArray(data=np.arange(9, 10.5, 0.1), dims=['profile'])
    times = np.full((15,), np.datetime64('2018-01-01 00:00:00', 'ns'))
    times += np.array([np.timedelta64(_) for _ in np.arange(0, 15)])
    times = xr.DataArray(times, dims='profile')
    trajectory = xr.DataArray(data=0, dims=[], attrs={
        'cf_role': 'trajectory_id'})

    z_data = np.arange(0, 1000, 10)
    z = xr.DataArray(data=z_data, dims=['z'], attrs={'axis': 'Z'})

    var = xr.DataArray(
        data=np.ones(shape=(15, 100)),
        dims=['profile', 'z'],
        attrs={'myattr': 'test_attr_val'}
    )
    xrdataset = xr.Dataset(
        coords={'lat': lat, 'lon': lon, 'time': times, 'depth': z},
        data_vars={'my_var': var, 'trajectory': trajectory},
        attrs={'gattr1': 'gattr1_val', 'gattr2': 'gattr2_val'}
    )
    return TrajectoryProfile(xrdataset)


@pytest.fixture(scope='module')
def feature_dimnames():
    return 'profile', 'z',


@pytest.fixture(scope='module')
def feature_dimsizes():
    return 15, 100,


@pytest.fixture(scope='module')
def content_class():
    return

def test_alias(feature_instance):
    assert (TrajectoryProfile.is_alias(feature_instance.ds) ==
            OMDCollection)
