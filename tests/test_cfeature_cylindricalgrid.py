from cerbere.feature.cgrid import Grid

from cerbere.feature.pytest_cfeature import *


@pytest.fixture(scope='module')
def feature_class():
    return Grid


@pytest.fixture(scope='module')
def feature_instance():
    # creates a test xarray object
    lon = xr.DataArray(data=np.arange(-180., 180, 1), dims=['lon'])
    lat = xr.DataArray(data=np.arange(-80., 80, 1), dims=['lat'])
    time = xr.DataArray(data=[datetime(2018, 1, 1)], dims=['time'])
    var = xr.DataArray(data=np.ones(shape=(360, 160)), dims=['lon', 'lat'])
    var.attrs['my_var'] = 'test_attr_val'
    xrdataset = xr.Dataset(
        coords={'lat': lat, 'lon': lon, 'time': time},
        data_vars={'my_var': var},
        attrs={'gattr1': 'gattr1_val', 'gattr2': 'gattr2_val'}
    )

    return Grid(xrdataset)


@pytest.fixture(scope='module')
def feature_dimnames():
    return 'lat', 'lon', 'time',


@pytest.fixture(scope='module')
def feature_dimsizes():
    return 160, 360, 1,


@pytest.fixture(scope='module')
def content_class():
    return


def test_circularity(feature_instance):
    # no circularity, padding activated
    subset = feature_instance.isel(
        **dict(lon=slice(-5, 5)), lat=slice(5, 10),
        circularity=False, padding=True
    )
    assert subset.ds.lon.count() == 5
    assert subset.ds.lat.count() == 5
    assert subset.ds['my_var'].count() == 5 * 5

    # with circularity
    subset = feature_instance.isel(
        **dict(lon=slice(-5, 5)), lat=slice(5, 10), circularity=True
    )
    assert subset.ds.lon.count() == 10
    assert subset.ds.lat.count() == 5
    assert subset.ds['my_var'].count() == 10 * 5

    print(subset.ds)

def test_fake_z_dimension():
    """check that cerbere ignore ambiguous vars such as bathymetry which are
    NOT a Z coordinate"""
    # creates a test xarray object
    lon = xr.DataArray(data=np.arange(-180., 180, 1), dims=['lon'])
    lat = xr.DataArray(data=np.arange(-80., 80, 1), dims=['lat'])
    time = xr.DataArray(
        data=[datetime(2018, 1, 1)], dims=['time'])
    depth = xr.DataArray(data=np.ones(shape=(360, 160)), dims=['lon', 'lat'])
    depth.attrs['standard_name'] = 'depth'
    depth.attrs['long_name'] = 'depth'
    depth.attrs['units'] = 'm'

    var = xr.DataArray(data=np.ones(shape=(360, 160)), dims=['lon', 'lat'])
    var.attrs['my_var'] = 'test_attr_val'
    xrdataset = xr.Dataset(
        coords={'lat': lat, 'lon': lon, 'time': time},
        data_vars={'my_var': var, 'depth': depth},
        attrs={'gattr1': 'gattr1_val', 'gattr2': 'gattr2_val'}
    ).cb.cfdataset
    print(xrdataset)
    assert 'depth' not in xrdataset.coords

