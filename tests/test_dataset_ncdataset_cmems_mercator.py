"""
Test class for cerbere GHRSST files mapper
"""
from __future__ import absolute_import, division, print_function

import unittest

from .checker import Checker


class NCDatasetL2PChecker(Checker, unittest.TestCase):
    """Test class for NCDataset 3D Grid such as CMEMS model files"""

    def __init__(self, methodName='runTest'):
        super(NCDatasetL2PChecker, self).__init__(methodName)

    @classmethod
    def dataset(cls):
        """Return the mapper class name"""
        return 'NCDataset'

    @classmethod
    def feature(cls):
        """Return the related datamodel class name"""
        return 'GridTimeSeries'

    @classmethod
    def test_file(cls):
        """Return the name of the test file for this test"""
        return 'mercatorpsy4v3r1_gl12_hrly_20200219_R20200210.nc'

    @classmethod
    def download_url(cls):
        """Return the URL of the data test repository where to get the test
        files
        """
        return 'ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/'

    def test_dim_swath(self):
        ncf = self.datasetclass(self.testfile)
        feature = self.featureclass(ncf)
        self.assertEquals(feature.get_geocoord('lat').dims, ('row', 'cell'))
        self.assertEquals(
            feature.get_field('thetao').dims,
            ('row', 'cell', 'time')
        )
