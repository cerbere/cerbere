import os
import unittest
from datetime import datetime

import numpy as np
import xarray as xr

from cerbere.dataset.field import Field
from cerbere.dataset.ncdataset import NCDataset


TEST_FILE = 'test_feature_dataset.nc'
TEST_SAVE = 'test_saved_feature.nc'


class TestFeature(unittest.TestCase):
    """Test class for AbstractFeature"""

    def get_feature_class(self):
        self.skipTest('base class')

    def get_feature(self, *args, **kwargs):
        return self.get_feature_class()(*args, **kwargs)

    def define_base_feature(self):
        self.skipTest('base class')

    def setUp(self):
        """ Setting up for the test """

        # creates a test xarray object
        feat = self.define_base_feature()

        # save it to disk
        saved_feat = feat.dataset.copy()
        for att in saved_feat.attrs:
            if saved_feat.attrs[att] is None:
                saved_feat.attrs[att] = ''
            elif isinstance(saved_feat.attrs[att], datetime):
                saved_feat.attrs[att] = saved_feat.attrs[att].strftime(
                    '%Y-%m-%dT%H:%M:%S'
                )
        saved_feat.to_netcdf(TEST_FILE, 'w')

    def tearDown(self):
        """Cleaning up after the test"""
        os.remove(TEST_FILE)
        if os.path.exists(TEST_SAVE):
            os.remove(TEST_SAVE)

    def test_create_feature_from_dict(self):
        basefeat = self.get_feature(self.define_base_feature())
        feat = self.get_feature(
            {
                'lat': {
                    'data': basefeat.get_lat(),
                    'dims': basefeat.get_geocoord('lat').dims
                },
                'lon': {
                    'data': basefeat.get_lon(),
                    'dims': basefeat.get_geocoord('lon').dims
                },
                'time': {
                    'data': basefeat.get_times(),
                    'dims': basefeat.get_geocoord('time').dims
                },
                'myvar': {
                    'data': basefeat.get_values('myvar'),
                    'dims': basefeat.get_field('myvar').dims
                }
            },
            attrs=basefeat.attrs
        )
        self.assertIsInstance(feat, self.get_feature_class())

    def test_create_feature_from_xarray(self):
        feat = self.get_feature(self.define_base_feature())
        self.assertIsInstance(feat, self.get_feature_class())

    def test_init_from_ncfile(self):
        ncf = NCDataset(dataset=TEST_FILE)
        feat = self.get_feature(ncf)
        self.assertIsInstance(feat, self.get_feature_class())

    def test_save_feature(self):
        feat = self.get_feature(self.define_base_feature())
        feat.save(dest=TEST_SAVE)

    def test_get_lat(self):
        feat = self.get_feature(self.define_base_feature())
        data = feat.get_lat()
        self.assertIsInstance(data, np.ndarray)

    def test_get_lon(self):
        feat = self.get_feature(self.define_base_feature())
        data = feat.get_lon()
        self.assertIsInstance(data, np.ndarray)

    def test_get_times(self):
        feat = self.get_feature(self.define_base_feature())
        data = feat.get_times()
        self.assertIsInstance(data, np.ndarray)
        self.assertTrue(np.issubdtype(data.dtype, np.datetime64))

    def test_get_datetimes(self):
        feat = self.get_feature(self.define_base_feature())
        data = feat.get_datetimes()
        self.assertIsInstance(data, np.ndarray)
        print(data, type(data), data.shape)
        if len(data.shape) > 0:
            self.assertIsInstance(data[0], datetime)
        else:
            self.assertIsInstance(data[()], datetime)

    def test_get_values(self):
        feat = self.get_feature(self.define_base_feature())
        data = feat.get_values('myvar')
        self.assertIsInstance(data, np.ma.MaskedArray)

    def test_get_geocoord(self):
        feat = self.get_feature(self.define_base_feature())
        data = feat.get_geocoord('lat')
        self.assertIsInstance(data, Field)

    def get_feature_dimnames(self):
        raise NotImplementedError

    def get_feature_dimsizes(self):
        raise NotImplementedError

    def test_geodimnames(self):
        dims = self.get_feature(self.define_base_feature())\
            .geodimnames
        self.assertIsInstance(dims, tuple)
        self.assertSequenceEqual(dims, self.get_feature_dimnames())

    def test_geodimsizes(self):
        sizes = self.get_feature(self.define_base_feature()).geodimsizes
        self.assertIsInstance(sizes, tuple)
        self.assertSequenceEqual(sizes, self.get_feature_dimsizes())

    def test_add_field(self):
        feat = self.get_feature(self.define_base_feature())
        print(feat.dims, tuple(feat.geodimsizes))
        da = xr.DataArray(
            dims=feat.geodimnames,
            data=np.ones(shape=tuple(feat.geodimsizes))
        )
        newfield = Field(da, name='newfield')
        feat.add_field(newfield)
        self.assertTrue('newfield' in feat.fieldnames)
        self.assertIsInstance(feat.get_field('newfield'), Field)

    def test_clone_field(self):
        ncf = NCDataset(dataset=TEST_FILE)
        feat = self.get_feature(ncf)

        cloned = feat.get_field('myvar').clone()
        print(cloned)
        self.assertIsInstance(cloned, Field)

        feat2 = feat = self.get_feature(self.define_base_feature())
        cloned2 = feat2.get_field('myvar').clone()
        print(cloned2)
        self.assertIsInstance(cloned2, Field)
        self.assertEqual(cloned.name, cloned2.name)
        #self.assertEqual(cloned.variable, cloned2.variable)
        #self.assertEqual(cloned.get_values(), cloned2.get_values())

    def test_get_bad_field(self):
        ncf = NCDataset(dataset=TEST_FILE)
        feat = self.get_feature(ncf)
        self.assertRaises(ValueError, feat.get_field, 'unknown_field')

    def test_append(self):
        feat = self.get_feature(self.define_base_feature())

        # another feature of same dimensions & coordinates
        feat2 = self.get_feature(self.define_base_feature())

        # append with shared dimension
        feat.append(feat2, prefix='v2_')
        for fieldname in feat2.fieldnames:
            self.assertIn('v2_'+fieldname, feat.fieldnames)
            np.allclose(feat.get_values(fieldname), feat2.get_values(fieldname))

        # test append with unshared dimensions
        feat = self.get_feature(self.define_base_feature())
        feat.append(feat2, prefix='v2_', add_coords=True, as_new_dims=True)
        for fieldname in feat2.fieldnames:
            self.assertIn('v2_'+fieldname, feat.fieldnames)
            for d in feat2.get_field(fieldname).dims:
                self.assertIn('v2_'+d, feat.dimnames)
                self.assertIn('v2_'+d, feat.coordnames)

        # test the appended feature can be save
        feat.save(TEST_SAVE)
