from cerbere.feature.ctrajectory import Trajectory

from cerbere.feature.pytest_cfeature import *


@pytest.fixture(scope='module')
def feature_class():
    return Trajectory


@pytest.fixture(scope='module')
def feature_instance():
    # creates a test xarray object
    lon = xr.DataArray(data=np.arange(100, 180, 1), dims=['time'])
    lat = xr.DataArray(data=np.arange(0, 80, 1), dims=['time'])
    times = np.full((80,), np.datetime64('2018-01-01 00:00:00', 'ns'))
    times += np.array([np.timedelta64(_) for _ in np.arange(0, 80)])
    time = xr.DataArray(times, dims='time')
    var = xr.DataArray(
        data=np.ones(shape=(80,)),
        dims=['time'],
        attrs={'myattr': 'test_attr_val'}
    )
    xrdataset = xr.Dataset(
        coords={'lat': lat, 'lon': lon, 'time': time},
        data_vars={'my_var': var},
        attrs={'gattr1': 'gattr1_val', 'gattr2': 'gattr2_val'}
    )
    return Trajectory(xrdataset)


@pytest.fixture(scope='module')
def feature_dimnames():
    return 'time',


@pytest.fixture(scope='module')
def feature_dimsizes():
    return 80,


@pytest.fixture(scope='module')
def content_class():
    return
